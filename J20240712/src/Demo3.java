//罗马数字转整数
/*
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。
C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
String类中的函数利用charAt(index)可以获取index下标位置上的字符
 */

public class Demo3 {
    public static void main(String[] args) {
    String s="LVIII";
        System.out.println(romanToInt(s));
    }
    public static int romanToInt(String s) {
        int len=s.length();
        int slow=0;
        int sum=0;
        int fast=0;
        for( ;slow<len;slow++){
            if(slow!=len-1){
                 fast=slow+1;
            }
            switch (s.charAt(slow)){
                case 'I':
                    if(s.charAt(fast)=='V'|| s.charAt(fast)=='X'){
                        if(s.charAt(fast)=='V'){
                            sum+=4;
                        }else{
                            sum+=9;
                        }
                        slow++;
                    }else{
                        sum+=1;
                    }
                  break;
                case 'V':
                    sum+=5;
                    break;
                case 'X':
                    if(s.charAt(fast)=='L' || s.charAt(fast)=='C'){
                        if(s.charAt(fast)=='L'){
                            sum+=40;
                        }else{
                            sum+=90;
                        }
                        slow++;
                    }else{
                        sum+=10;
                    }
                    break;
                case 'L':
                    sum+=50;
                    break;
                case 'C':
                    if(s.charAt(fast)=='D'||s.charAt(fast)=='M'){
                        if(s.charAt(fast)=='D'){
                            sum+=400;
                        }else{
                            sum+=900;
                        }
                        slow++;
                    }else{
                        sum+=100;
                    }
                    break;
                case 'D':
                    sum+=500;
                    break;
                case 'M':
                    sum+=1000;
                    break;
            }
        }
          return sum;
    }
}
