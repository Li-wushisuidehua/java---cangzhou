public class Demo5 {
    public static void main(String[] args) {
        int n=2;
        System.out.println(isHappy(n));
    }
    public static int countSum(int n){
        int sum=0;
        while(n!=0){
            int k=n%10;
            sum+=k*k;
            n/=10;
        }
        return sum;
    }
    public static boolean isHappy(int n){
        int slow=countSum(n);
        int fast=countSum(slow);
        while(fast!=slow){
            slow=countSum(slow);
            fast=countSum(countSum(fast));
        }
        return fast==1;
    }
}
