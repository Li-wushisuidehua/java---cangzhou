import java.util.Arrays;

public class Test {
    public static String reverseMessage(String message) {
            String []str= message.trim().split(" ");
            StringBuilder sb=new StringBuilder();
            for (int i=str.length-1;i>=0;i--) {
                //如果满足下面这个条件说明为空格，空格不要
                if(str[i].trim().length()==0){
                    continue;
                }
                sb.append(" "+str[i]);
            }
            return sb.toString().trim();



    }

    public static void main(String[] args) {
        System.out.println(reverseMessage(" "));
    }
}
