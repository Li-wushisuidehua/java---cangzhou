import java.util.Arrays;

public class Demo5 {
    public static int lastStoneWeight(int[] stones) {
        //先对数组进行排序
        int len=stones.length;
        while(len!=1){
            Arrays.sort(stones,0,len);
            int right=len-1;
            int left=right-1;
            if(stones[left]==stones[right]){
                left-=2;
                right=left+1;
                len-=2;
                if(len==0){
                    return 0;
                }
            }else{
                left--;
                stones[right-1]=stones[right]-stones[right-1];
                len-=1;
                right--;
            }

        }
        return stones[0];

    }
//方法二的代码和方法一的代码执行时间是一样的，但是代码篇幅短
    public static int lastStoneWeight1(int[] stones) {
        int index=stones.length-1;
        for(int i=0;i<stones.length-1;i++){
            Arrays.sort(stones);
            stones[index]-=stones[index-1];
            stones[index-1]=0;
        }
        return stones[index];

    }
    public static void main(String[] args) {
        int []arr=new int[]{7,6,7,6,9};
        System.out.println(lastStoneWeight(arr));
    }
}
