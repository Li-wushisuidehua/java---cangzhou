//有20个线程需要同时启动，其中主线程需要等所有线程打印完毕后在进行打印
public class Dmeo2 {
    public static void main(String[] args) throws InterruptedException {
        Thread t0=new Thread(()->{
            System.out.println("这是第一个线程"+0);
        });
        Thread t1=new Thread(()->{
            System.out.println("这是第二个线程"+1);
        });
        Thread t2=new Thread(()->{
            System.out.println("这是第三个线程"+2);
        });
        Thread t3=new Thread(()->{
            System.out.println("这是第四个线程"+3);
        });
        Thread t4=new Thread(()->{
            System.out.println("这是第五个线程"+4);
        });
        Thread t5=new Thread(()->{
            System.out.println("这是第六个线程"+5);
        });
        Thread t6=new Thread(()->{
            System.out.println("这是第七个线程"+6);
        });
        Thread t7=new Thread(()->{
            System.out.println("这是第八个线程"+7);
        });
        Thread t8=new Thread(()->{
            System.out.println("这是第九个线程"+8);
        });
        Thread t9=new Thread(()->{
            System.out.println("这是第十个线程"+9);
        });

        Thread t10=new Thread(()->{
            System.out.println("这是第十一个线程"+10);
        });
        Thread t11=new Thread(()->{
            System.out.println("这是第十二个线程"+11);
        });
        Thread t12=new Thread(()->{
            System.out.println("这是第十三个线程"+12);
        });
        Thread t13=new Thread(()->{
            System.out.println("这是第十四个线程"+13);
        });
        Thread t14=new Thread(()->{
            System.out.println("这是第十五个线程"+14);
        });  Thread t15=new Thread(()->{
            System.out.println("这是第十六个线程"+15);
        });  Thread t16=new Thread(()->{
            System.out.println("这是第十七个线程"+16);
        });  Thread t17=new Thread(()->{
            System.out.println("这是第十八个线程"+17);
        });  Thread t18=new Thread(()->{
            System.out.println("这是第十九个线程"+18);
        });
         t0.start();
        t0.join();
         t1.start();
        t1.join();
         t2.start();
        t2.join();
         t3.start();
        t3.join();
         t4.start();
        t4.join();
         t5.start();
        t5.join();
         t6.start();
        t6.join();
        t7.start();
        t7.join();
        t8.start();
        t8.join();
        t9.start();
        t9.join();
        t10.start();
        t10.join();
        t11.start();
        t11.join();
        t12.start();
        t12.join();
        t13.start();
        t13.join();
        t14.start();
        t14.join();
        t15.start();
        t15.join();
        t16.start();
        t16.join();
        t17.start();
        t17.join();
        t18.start();
        t18.join();
        System.out.println("主线程执行完毕: "+"ok");
    }
}
