import javax.naming.PartialResultException;
import java.util.Random;

public class Demo1 {
    public static int[] arr=new int[100000]; // 非静态变量不能被静态方法调用
   public static int sum1=0;//偶数和
    public static int sum2=0;//奇数和
      public static void main(String[] args) throws InterruptedException {
        Random random=new Random();
          for(int i=0;i<arr.length;i++){
              arr[i]=random.nextInt(100); // 随机生成1-100间的数字
          }
          Thread t1=new Thread(()->{ // 使用lambda创建线程的方法
              for(int i=0;i<arr.length;i+=2){
                  sum1+=arr[i];
              }
          });
          Thread t2=new Thread(()->{
              for(int i=1;i<arr.length;i+=2){
                  sum2+=arr[i];
              }
          });
          long Starttime=System.currentTimeMillis();//用来计算时间的函数
      t1.start();
      t2.start();
          t1.join();
      t2.join();
      long Endtime=System.currentTimeMillis();//用来计算时间的函数
          System.out.println("最终结果为："+(sum1+sum2));
          System.out.println("一共耗费的时间为："+(Endtime-Starttime));
    }
}
