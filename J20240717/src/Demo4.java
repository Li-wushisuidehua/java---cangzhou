import java.util.Arrays;
import java.util.Scanner;

public class Demo4 {
    public static boolean CheckPermutation(String ss1, String ss2) {
        //可以先转化为字符数组，后对数组进行排序根据ascii码然后一一进行比对，如果相等便可以，如果不相等就是不可以

        //第一步先转化为字符数组
        char []s1=ss1.toCharArray();
        char []s2=ss2.toCharArray();

        //分别对数组进行排序
        //方法一
//        for(int i=0;i<s1.length;i++){
//            for(int j=i+1;j<s1.length;j++){
//                if(s1[i]>s1[j]){
//                    char c=s1[i];
//                    s1[i]=s1[j];
//                    s1[j]=c;
//                }
//            }
//        }
//        for(int i=0;i<s1.length;i++){
//            for(int j=i+1;j<s1.length;j++){
//                if(s2[i]>s2[j]){
//                    char c=s2[i];
//                    s2[i]=s2[j];
//                    s2[j]=c;
//                }
//            }
//        }
        //对字符数组进行排序方法二
        Arrays.sort(s1);
        Arrays.sort(s2);

        //判断两个字符数组是否相等
        //方法一
        return Arrays.equals(s1,s2);

        //方法二
//        for (int i = 0; i < s1.length; i++) {
//            if(s1[i]!=s2[i]){
//                return false;
//            }
//        }
//        return true;

    }

    public static void main(String[] args) {
        String s="abc";
        String s2="bca";
        System.out.println(CheckPermutation(s,s2));
    }
}
