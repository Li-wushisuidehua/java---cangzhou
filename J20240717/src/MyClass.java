//以下操作可以使得在静态方法中调用非静态方法
    public class MyClass {

        public void instanceMethod() {
            System.out.println("This is an instance method.");
        }

        public static void staticMethod(MyClass myClassInstance) {
            // 通过类的实例调用实例方法
            myClassInstance.instanceMethod();
        }

        public static void main(String[] args) {
            MyClass myInstance = new MyClass();
            staticMethod(myInstance); // 调用静态方法，并传入一个实例作为参数
        }
    }

