import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Demo1_14_014 {

    public static void main(String[] args) {
        String str1="ababab";
        String str2="ab";
        for (int x:findAnagrams(str1,str2)) {
            System.out.print(x+" ");
        }


    }
    public static int[] fun(String str1,String str2){
        int len2=str2.length(),left=0,right=0,len1=str1.length(),count=0,size=0;
        int []arr=new int[len1];
        HashMap<Character,Integer> hash1=new HashMap<>();
        HashMap<Character,Integer> hash2=new HashMap<>();

        for (int i = 0; i < len2; i++) {
            hash2.put(str2.charAt(i),hash2.getOrDefault(str2.charAt(i),0)+1);
        }
        while(right<len1){
            hash1.put(str1.charAt(right),hash1.getOrDefault(str1.charAt(right),0)+1);
            count++;
            if(count==len2){
                if(hash1.equals(hash2)){
                    arr[size++]=left;
                }
                if(hash1.get(str1.charAt(left))-1==0)
                hash1.remove(str1.charAt(left));
                else
                    hash1.put(str1.charAt(right),hash1.getOrDefault(str1.charAt(right),0)-1);
                left++;
                count--;
            }
            right++;
        }
        return arr;
    }
    public static List<Integer> findAnagrams(String s, String p) {
        int lens=s.length(),lenp=p.length(),left=0,right=0,count=0;
        int []hash1=new int[26];
        int []hash2=new int[26];
        List<Integer> list=new ArrayList<>();
        for (int i = 0; i < lenp; i++) {
            hash2[p.charAt(i)-'a']++;
        }
       while(right<lens){
           int out=s.charAt(right)-'a';
           hash1[out]++;
           if(hash1[out]<=hash2[out]) count++;
           if((right-left+1)>lenp){
               hash1[s.charAt(left)-'a']--;
               if(hash1[s.charAt(left)-'a']<hash2[s.charAt(left)-'a']) count--;
               left++;
           }
           //判断检查 hash1 和 hash2 是否相等
           if(count==lenp) list.add(left);
           right++;
       }
           return list;
    }
}
