//判断字符串中的字符是否唯一
public class DemoTest1 {
    public static void main(String[] args) {
      String str="abc";
      boolean bool=isUnique(str);
        System.out.println(bool);
    }
    //方法一：暴力解法
    public static  boolean isUnique(String str) {
         char[] array=str.toCharArray();
         char[] arr=new char[str.length()];
         int length=0;
        for (int i = 0; i < array.length; i++) {
            int j=0;
            while(j<length){
                if(array[i] == arr[j++]){
                    return false;
                }
            }
            arr[length++]=array[i];
        }
       return true;
    }
    //优化方法，使用hash表来判断是否有出现多次
}
