package Test1;

import Test2.Son2;

public class Son1 extends Son2 {
   // public int c=22;
    public void func(){

        System.out.println(this.c);
    }
    public static void main(String[] args) {
        Son2 son2=new Son2();
        son2.a=34;
     //   son2.c=35;
        Son1 son1=new Son1();
        son1.c=33;
        System.out.println(son1.c);

       // System.out.println(son1.func());
        son1.func();
    }

}
//总结：在protected大前提的修饰下 在不同包中子类的访问方法🈶以下俩种:
//第一中:用 子类实例化对象，并通过引用.x
//第二种:就是在子类中创建非静态的方法，用super进行调用
//总结:在public大前提的修饰下 在不同包中子类的访问方法:
//直接实例化对象一个
