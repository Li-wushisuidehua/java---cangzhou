package Test2;
//

class A {//父类
    public  int a=10;
    public int b=20;
}
class B extends A {//子类
    public int b=5;
}
class C extends B {//孙类
    public int c=3;
}
public class Test {

    public static void main(String[] args) {
        A a0 = new A();//1
        a0.a = 10;
        A a1 = new B();//创建一个A的引用，创建一个B的实例，将实例的地址给到a1；
        System.out.println(a1.b);
        A a2 = new C();//3
        a2.a = 30;
    }
}