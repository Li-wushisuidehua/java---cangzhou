import org.w3c.dom.ls.LSOutput;

//父类
class father{
    public String name;
    public int age;

}
//子类
public class Son extends father{
    public String name;
    public String personality;
    public  void show(){
        System.out.println(this.name+"正在玩耍！");
    }
    public void time(){
        System.out.println(this.personality+"是他的性格！");
    }
    public void age(){
        System.out.println("他今年"+this.age+"岁了！");
    }
    public void loc(){
        super.name="lixiangjun";
        System.out.println(super.name+"是"+this.name+"的爸爸");
    }


   public void func(){
        System.out.println("hahhah");
    }

    public static void main(String[] args) {
        Son son=new Son();
        son.personality="暴躁";
        son.name="李明月";
        son.loc();
        father son5=new Son();

        son.func();
    }

}
