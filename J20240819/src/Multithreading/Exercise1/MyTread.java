package Multithreading.Exercise1;

public class MyTread extends Thread{
    public static int ticket=1000;
    @Override
    public void run() {
        while(true){
            synchronized (MyTread.class){
                if(ticket==0){
                    break;
                }else{
                    ticket--;
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println(getName()+"售出一张票，还剩余"+ticket+"张票");
                }

            }
        }

    }
}
