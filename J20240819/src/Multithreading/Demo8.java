package Multithreading;

//线程安全问题
//问题：现在有一部国产大片要上线，此时有多个窗口开始售卖初始的100张票，请使用代码进行实现
public class Demo8 {
    public static void main(String[] args) {
        MyThread6 t1=new MyThread6();
        MyThread6 t2=new MyThread6();
        MyThread6 t3=new MyThread6();

        //设置窗口名字
        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");

        //开启对应的线程
        t1.start();
        t2.start();
        t3.start();

    }
}
//class MyThread6 extends Thread{
//    static int ticket=0;//这里加入static作为修饰符，是为了确保ticket只有一份
//   //同步代码块(代码中＋snychronized)
//    @Override
//    public void run() {
//     while(true) {
//           synchronized (MyThread6.class){//给上锁，注意这里的锁对象必须为唯一一个，否则多个线程对应多个锁对象无意义
//             if(ticket<100){
//                 ticket++;
//                 System.out.println(getName() + "正在售卖" + ticket + "张票！！！");
//             }else{
//                 break;
//             }
//         }
//     }
//    }
//}
class MyThread6 extends Thread{
    static int ticket=1;//这里加入static作为修饰符，是为了确保ticket只有一份
    //同步代码块(代码中＋snychronized)
    @Override
    public void run() {
        while(true) {
           if(method()){
               ticket++;
           }else{
               break;
           }
        }
    }
    private static synchronized boolean method(){//注意这里的同步方法默认锁对象为this，但是不同的继承的Thread类的不同实例代表不同的对象
            if(ticket<=100){
                System.out.println(Thread.currentThread().getName() + "正在售卖" + ticket + "张票！！！");
                return true;
            }else{
                return false;
            }
    }
}
