package Multithreading;

//礼让线程/出让线程
public class Demo6 {
    public static void main(String[] args) {
        MyThread3 t1=new MyThread3();
        MyThread3 t2=new MyThread3();

        t1.start();
        t2.start();


    }
}
class MyThread3 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(getName()+" "+i);
            Thread.yield();//yield方法为静态方法
        }

    }
}
