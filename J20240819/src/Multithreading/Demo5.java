package Multithreading;

//守护线程
public class Demo5 {
    public static void main(String[] args) {
        MyThread1 t1=new MyThread1("女神");
        MyThread2 t2=new MyThread2("备胎");
         //设置备胎线程
        t2.setDaemon(true);

        //启动线程
        t1.start();
        t2.start();
    }
}
class MyThread1 extends Thread{
    public MyThread1() {
    }

    public MyThread1(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i <=10; i++) {
            System.out.println(getName()+" "+i);
        }
    }
}
class MyThread2 extends Thread{
    public MyThread2() {
    }

    public MyThread2(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i <=100; i++) {
            System.out.println(getName()+" "+i);
        }
    }
}