package Multithreading.Exercise3;

public class MyThread extends Thread{
    public static int m=1;
    @Override
    public void run() {
        while(true){
            synchronized (MyThread.class){
                if(m>100){
                    break;
                }else{
                    if(m%2!=0){
                        System.out.println(m);
                    }
                    m++;
                }
            }
        }

    }
}
