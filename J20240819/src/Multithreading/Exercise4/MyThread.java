package Multithreading.Exercise4;

import java.util.Random;

public class MyThread extends Thread{
    //确保一份总的价钱
    public static double money=100;

    //确认最小值,一个红包的最小值都是0.01
    public static final double MIN=0.01;

    //红包的个数
    public static int count=3;

    @Override
    public void run() {
        synchronized (MyThread.class){
            double prize=0;
            if(count==0) {
                System.out.println(getName()+"没抢到红包");
            }else{
                //最后一个包
                if(count==1){
                    prize=money;
                }else{
                    Random r=new Random();
                    double bounds=money-(count-1)*MIN;
                    prize= r.nextDouble(bounds);//生成带小数的随机值
                    if(prize<MIN){ //生成的随机数是在0~bounds之间，但是最小的包都是0.01
                        prize=MIN;
                    }
                }
                count--;
                money=money-prize;
                System.out.println(getName()+"抢到了"+prize);
            }

        }

    }
}
