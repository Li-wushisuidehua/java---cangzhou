package Multithreading.Exercise4;

//抢红包
public class Demo1 {
    public static void main(String[] args) {
        MyThread t1=new MyThread();
        MyThread t2=new MyThread();
        MyThread t3=new MyThread();
        MyThread t4=new MyThread();
        MyThread t5=new MyThread();

        //设置名字
        t1.setName("花花");
        t2.setName("小美");
        t3.setName("君君");
        t4.setName("大宝");
        t5.setName("破皮");

        //启动线程
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

    }
}
