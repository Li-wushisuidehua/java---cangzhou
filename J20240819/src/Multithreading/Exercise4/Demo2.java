package Multithreading.Exercise4;

public class Demo2 {
    public static void main(String[] args) {
        MyThread2 t1=new MyThread2();
        MyThread2 t2=new MyThread2();
        MyThread2 t3=new MyThread2();
        MyThread2 t4=new MyThread2();
        MyThread2 t5=new MyThread2();

        //设置名字
        t1.setName("花花");
        t2.setName("小美");
        t3.setName("君君");
        t4.setName("大宝");
        t5.setName("破皮");

        //启动线程
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

    }
}
