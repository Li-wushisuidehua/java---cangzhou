package Multithreading.Exercise4;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

//public class MyThread2 extends Thread{
//    //总金额
//    static BigDecimal money=BigDecimal.valueOf(100.0);
//
//    //个数
//    static int count=3;
//
//    //最小抽奖金额
//    static final BigDecimal MIN=BigDecimal.valueOf(0.01);
//
//    @Override
//    public void run() {
//        synchronized (MyThread2.class){
//            if(count==0) System.out.println(getName()+"没有抢到红包！");
//            else{
//                //中奖金额
//                BigDecimal prize;
//                if(count==1) prize=money;
//                else{
//                    //获取抽奖范围
//                    double bounds=money.subtract(BigDecimal.valueOf(count-1).multiply(MIN)).doubleValue();//后面的doubleValue()可以将BigDecimal类型转化为double，因为随机数生成
//                    //的函数中需要使用double类型作为参数进行传递
//                    Random r=new Random();
//                    //抽奖金额
//                    prize=BigDecimal.valueOf(r.nextDouble(bounds));
//                }
//                //设置抽中红包，小数点保留两位，四舍五入
//                prize=prize.setScale(2, RoundingMode.HALF_UP);
//                //在总金额中去掉对应的钱
//                money=money.subtract(prize);
//                //红包少了一个
//                count--;
//                //输出红包信息
//                System.out.println(getName()+"抽中了"+prize+"元");
//            }
//        }
//    }
//}
public class MyThread2 extends Thread{
    //设置总金额
    static BigDecimal money=new BigDecimal("100.0");

    //设置红包的最小值
     final BigDecimal MIN=new BigDecimal("0.01");

    //设置红包的数量
    static int count=3;

    @Override
    public void run() {
        synchronized (MyThread2.class){
            if(count==0) System.out.println(getName()+"没有抢到红包");
            else{
                BigDecimal prize;
                if(count==1){
                    prize=money;
                }else{
                    double bounds=money.subtract(BigDecimal.valueOf(count-1).multiply(MIN)).doubleValue();
                    Random r=new Random();
                    prize=BigDecimal.valueOf(r.nextDouble(bounds));
                }
                prize=prize.setScale(2,RoundingMode.HALF_UP);
                count--;
                money=money.subtract(prize);
                System.out.println(getName()+"抢到了"+prize+"元");

            }
        }
    }
}
