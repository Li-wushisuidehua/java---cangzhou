package Multithreading.Exercise4;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Demo3 {
    public static void main(String[] args) {
        BigDecimal num1=new BigDecimal("10.8355");
        System.out.println(num1.setScale(2, RoundingMode.HALF_UP));
        System.out.println(num1.setScale(2,RoundingMode.HALF_DOWN));
        System.out.println(num1.setScale(0,RoundingMode.HALF_EVEN));


    }
}
