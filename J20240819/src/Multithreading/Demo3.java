package Multithreading;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

//创建线程方法三----callable接口
public class Demo3 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MycCallable mc=new MycCallable();//表示多线程要执行的任务
        FutureTask<Integer> ft=new FutureTask<>(mc);//管理多线程运行的结果

        Thread t1=new Thread(ft);//创建线程

        t1.start();//启动线程

        System.out.println(ft.get());
    }


}
class MycCallable implements Callable<Integer>{
    @Override
    public Integer call() throws Exception {
        int sum=0;
        for (int i = 1; i <= 100; i++) {
            sum+=i;
        }
        return sum;
    }
}
