package Multithreading;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Demo10 {
    public static void main(String[] args) {
        MyThread7 t1=new MyThread7();
        MyThread7 t2=new MyThread7();
        MyThread7 t3=new MyThread7();

        //窗口赋名字
        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");

        //启动线程
        t1.start();
        t2.start();
        t3.start();

    }

}
class MyThread7 extends Thread{
    static int ticket=0;
    static Lock lock=new ReentrantLock();
    @Override
    public void run() {
        while(true){
            lock.lock();
            try {
                if(ticket==100){
                    break;//注意当使用Lock这种显示锁的时候需要自己写unlock去释放锁
                }else{
                    Thread.sleep(100);
                    ticket++;
                    System.out.println(getName()+"售卖的第"+ticket+"张票！！！！");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                lock.unlock();
            }
        }
    }
}
