package Multithreading.Exercise5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

//public class MyThread extends Thread{
//    static int[] arr=new int[]{10,5,20,50,100,200,500,800,2,80,300,700};
//    static int count=12;
//    @Override
//    public void run() {
//        while(true){
//        synchronized (MyThread.class){
//                if(count==0){
//                    System.out.println("奖池奖项已被抽完");
//                    break;
//                }else{
//                    //生成随机数
//                    Random r=new Random();
//                    int i=r.nextInt(12);
//                    if(arr[i]>=0){
//                        System.out.println(getName()+"抽到了"+arr[i]);
//                        arr[i]=-1;
//                        count--;
//                    }
//                }
//            }
//
//        }
//
//    }
//}
public class MyThread extends Thread{
    ArrayList<Integer> list;

    public MyThread(ArrayList<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        while(true){
            synchronized (MyThread.class){
                if(list.size()==0) break;
                else{
                    Collections.shuffle(list);
                    int prize=list.remove(0);
                    System.out.println(getName()+"又抽中了"+prize+"元");
                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }
}