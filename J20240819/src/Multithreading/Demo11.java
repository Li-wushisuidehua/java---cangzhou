package Multithreading;

//死锁问题
public class Demo11 {
    public static void main(String[] args) {
        MyRunnale2 mr=new MyRunnale2();
        Thread t1=new Thread(mr);
        Thread t2=new Thread(mr);

        //给t1和t2设置名字
        t1.setName("线程1");
        t2.setName("线程2");

        //开启线程
        t1.start();
        t2.start();
    }
}
class MyRunnale2 implements Runnable{
    Object obj1=new Object();
    Object obj2=new Object();
    @Override
    public void run() {
        if("线程1".equals(Thread.currentThread().getName())){
            synchronized (obj1){
                System.out.println("线程1此时进去锁1，要获取锁2");
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (obj2){
                    System.out.println("锁1后拿到锁2，线程1执行完毕");
                }
            }
        }
        if("线程2".equals(Thread.currentThread().getName())){
            synchronized (obj2){
                System.out.println("线程2此时进去锁2，要获取锁1");
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (obj1){
                    System.out.println("锁2拿到锁1，线程2执行完毕");
                }
            }
        }
    }
}
