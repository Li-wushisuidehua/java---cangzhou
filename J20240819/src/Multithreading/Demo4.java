package Multithreading;

//线程的优先级
public class Demo4 {
    public static void main(String[] args) {
        MyRunnale mr=new MyRunnale();
        Thread t1=new Thread(mr,"飞机");
        Thread t2=new Thread(mr,"坦克");

        //设置优先级
        t1.setPriority(1);
        t2.setPriority(10);

        //启动线程
        t1.start();
        t2.start();

        //获得线程的优先级
        System.out.println(t1.getPriority());
        System.out.println(t2.getPriority());


    }
}
class MyRunnale implements Runnable{
    @Override
    public void run() {
        for (int i = 1; i <=100; i++) {
            System.out.println(Thread.currentThread().getName()+"----"+i);
        }
    }
}
