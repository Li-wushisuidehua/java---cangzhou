package Multithreading;

//插入线程/插队线程
public class Demo7 {
    public static void main(String[] args) throws InterruptedException {
        MyThread5 t1=new MyThread5("憨憨");
        t1.start();
        t1.join();
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName()+" "+i);
        }
    }
}
class MyThread5 extends Thread{
    public MyThread5(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(getName()+" "+i);
        }
    }
}
