package Multithreading;

//创建线程方法二----实现Runnable接口
public class Demo2 {
    public static void main(String[] args) {
        MyRun mr=new MyRun();

        //创建线程
        Thread t1=new Thread(mr);
        Thread t2=new Thread(mr);

        //给线程赋予名字
        t1.setName("线程1");
        t2.setName("线程2");

        //开启线程
        t1.start();
        t2.start();

    }
}
class MyRun implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName()+" Hello World!");
        }
    }
    //Thread.currentThread() 为静态方法获取到了当前的Thread的对象，对象.getName()获取到了当前对象的名字
}
