package Multithreading;

//同步方法
public class Demo9 {
    public static void main(String[] args) {
        MyRunnale1 mr=new MyRunnale1();

        //创建线程
        Thread t1=new Thread(mr);
        Thread t2=new Thread(mr);
        Thread t3=new Thread(mr);//他们都使用mr来创建线程，所以说ticket只需要一份就够了

        //修改线程的名字
        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");

        //启动线程
        t1.start();
        t2.start();
        t3.start();
    }
}
class MyRunnale1 implements Runnable {
    int ticket=1;
//    @Override
//    public void run() {
//        while(true){
//            synchronized (this){
//                if(ticket<100){
//                    ticket++;
//                    System.out.println(Thread.currentThread().getName()+"正在售卖"+ticket+"张票！！！");
//                }else {
//                    break;
//                }
//            }
//
//        }
//    }
@Override
public void run() {
    while(true){
        if(method()){
                ticket++;
        }
    }
}
private synchronized boolean method(){
    if(ticket<=100){
        System.out.println(Thread.currentThread().getName()+"正在售卖"+ticket+"张票！！！");
        return true;
    }else {
        return false;
    }
}
}
