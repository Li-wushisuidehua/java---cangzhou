package Multithreading;
//创建多线程的方法一----thread
public class Demo1 {
    public static void main(String[] args) {
        MyThread t1=new MyThread();//创建线程
        MyThread t2=new MyThread();
        t1.setName("线程1");
        t2.setName("线程2");
        t1.start();//开启线程
        t2.start();
    }
}
class MyThread extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(getName()+"Hello world");
        }
    }
    //getName()方法，在Thread类中，且为public修饰的方法，MyThread类继承Thread类，便可以直接调用getName()方法-----> Returns this thread's name
}

