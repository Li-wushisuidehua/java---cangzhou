package Multithreading.ThreadsPool;

public class MyRunnable implements Runnable{
    @Override
    public void run() {
        synchronized (this){
            for (int i = 1; i <= 100; i++) {
                System.out.println(Thread.currentThread().getName()+"------"+i);
            }
        }
    }
}
