package Multithreading.ThreadsPool;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Demo1 {
    public static void main(String[] args) throws InterruptedException {
     //   ExecutorService pool= Executors.newCachedThreadPool();//线程池中数量无限（其实有上限，上限为int类型的最大值）
         ExecutorService pool=Executors.newFixedThreadPool(3);//指定线程池中线程的数量
        MyRunnable mr=new MyRunnable();
        pool.submit(mr);//提交任务
        pool.submit(mr);//提交任务
        pool.submit(mr);//提交任务
        pool.submit(mr);//提交任务
        pool.submit(mr);//提交任务
        pool.submit(mr);//提交任务

    }


}
