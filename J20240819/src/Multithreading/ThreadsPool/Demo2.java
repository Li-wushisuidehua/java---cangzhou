package Multithreading.ThreadsPool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Demo2 {
    public static void main(String[] args) {
        ThreadPoolExecutor pool=new ThreadPoolExecutor(
                3,//核心线程数量
                6,//总线程数量，由总线程数量可以推算出临时线程的数量，临时=总-核心，总线程的数量>=核心线程的数量
                60,//停60秒会自动销毁临时线程
                TimeUnit.SECONDS,//60秒
                new ArrayBlockingQueue<>(3),//阻塞队列的长度为3
                new ThreadPoolExecutor.AbortPolicy()//拒绝任务线程，并且会抛出异常
        );
        MyRunnable mr=new MyRunnable();
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
        pool.submit(mr);
    }
}
/**
 * 运算中会先占用核心线程的数量，当核心线程的数量被占满的时候，此时会占用阻塞队列，当阻塞队列被占满的时候，此时会开始创建临时线程，总的线程和
 * 阻塞队列均被沾满的时候，此时如果还有任务等待执行的时候此时会触发最后一个参数，也就是拒绝机制
 */
