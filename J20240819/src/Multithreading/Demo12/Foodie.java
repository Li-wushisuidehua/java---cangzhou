package Multithreading.Demo12;

/**
 * 1. 循环
 * 2. 加锁
 * 3. 判断是否到结尾（到了会怎么样）
 * 4. 判断是否到结尾（没到会怎么样）
 */
public class Foodie extends Thread{
    @Override
    public void run() {
        while(true){
            synchronized (Desk.obj){
                if(Desk.count==0){
                    break;
                }
                if(Desk.Foodflag==0){
                    //没有面条会进入等待，一直到厨师做好面条
                    try {
                        Desk.obj.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }else{
                    //一共数量--
                    Desk.count--;
                    //将状态设置为桌子上没有面条
                    System.out.println("还能再吃"+Desk.count+"碗面条！！");
                    Desk.Foodflag=0;
                    //唤醒厨师做面条
                    Desk.obj.notifyAll();
                }
            }
        }
    }
}
