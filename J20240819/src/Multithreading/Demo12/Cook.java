package Multithreading.Demo12;

public class Cook extends Thread{
    int sum=0;
    @Override
    public void run() {
        while(true){
            synchronized (Desk.obj){
                if(Desk.count==0) break;
                if(Desk.Foodflag==0){
                    //没有面条，此时需要做面条
                    Desk.Foodflag=1;
                    System.out.println("还需要做"+(Desk.count-1)+"碗面条");
                    Desk.obj.notifyAll();
                }else{
                    try {
                        Desk.obj.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

        }
    }
}
