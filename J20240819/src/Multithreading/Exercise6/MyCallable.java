package Multithreading.Exercise6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.Callable;

public class MyCallable implements Callable<Integer> {

    ArrayList<Integer> list;
    public MyCallable(ArrayList<Integer> list) {
        this.list = list;
    }

    @Override
    public Integer call() throws Exception {
        ArrayList<Integer> lists=new ArrayList<>();
        while(true){
            synchronized (MyCallable.class){//1   //2
                if(list.size()==0) {
                    System.out.println(Thread.currentThread().getName()+lists);
                    break;
                }else{
                    Collections.shuffle(list);
                    int prize=list.remove(0);
                    if("抽箱一".equals(Thread.currentThread().getName())){
                        lists.add(prize);
                    }else if("抽箱二".equals(Thread.currentThread().getName())){
                        lists.add(prize);
                    }

                }
            }
                Thread.sleep(10);//因为call抛出了
        }
        return Collections.max(lists);
    }
}
