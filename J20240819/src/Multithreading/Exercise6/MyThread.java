package Multithreading.Exercise6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MyThread extends Thread{
    ArrayList<Integer> list;

    public MyThread(ArrayList<Integer> list) {
        this.list = list;
    }
     ArrayList<Integer> lists=new ArrayList<>();
    int []arr=new int[12];
    static int count1=0;
    static int max1=0;
    static int sum1=0;
    static int count2=0;
    static int max2=0;
    static int sum2=0;

    @Override
    public String toString() {
        return "MyThread{" +
                "lists=" + lists +
                '}';
    }

    @Override
    public void run() {
        while(true){
            synchronized (Multithreading.Exercise5.MyThread.class){
                if(list.size()==0) {
//                    if("抽箱一".equals(getName())){
//                            System.out.println("在此次抽奖过程中，抽奖箱1总共完成了"+count1+"个奖项");
//                            System.out.print("分别为：");
//                        System.out.println(lists);
//                            System.out.print("最高奖项为"+max1+"总计"+sum1+"元");
//                            System.out.println();
//                    }
//                    if("抽箱二".equals(getName())){
//                        synchronized (MyThread.class){
//                            System.out.println("在此次抽奖过程中，抽奖箱2总共完成了"+count2+"个奖项");
//                            System.out.print("分别为：");
//                            System.out.println(lists);
//                            System.out.print("最高奖项为"+max2+"总计"+sum2+"元");
//                            System.out.println();
//                        }
//                    }
                    System.out.println(getName()+lists);
                    if(max1>max2 && "抽箱一".equals(getName())){
                        System.out.println("在此次抽奖的过程中"+getName()+"产生了最大奖项，该奖项金额为"+max1);
                    }
                    if(max2>max1 && "抽箱二".equals(getName())){
                        System.out.println("在此次抽奖的过程中"+getName()+"产生了最大奖项，该奖项金额为"+max2);

                    }

                    break;
                }
                else{
                    Collections.shuffle(list);
                    int prize=list.remove(0);
                    if("抽箱一".equals(getName())){
                       lists.add(prize);
                        count1++;
                        sum1+=prize;
                        max1=Math.max(prize,max1);

                    }else if("抽箱二".equals(getName())){
                       lists.add(prize);
                        count2++;
                        sum2+=prize;
                        max2=Math.max(prize,max2);
                    }

                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
//上文中使用数组和集合的不同在于，使用数组需要事先规定好数组的长度，而集合可以在增加的过程中，增加其长度
