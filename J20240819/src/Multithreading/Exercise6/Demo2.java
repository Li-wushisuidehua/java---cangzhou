package Multithreading.Exercise6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Demo2 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ArrayList<Integer> list=new ArrayList<>();
        Collections.addAll(list,10,5,20,50,100,200,500,800,2,80,300,700);
        MyCallable mc=new MyCallable(list);//可以看成要执行的函数体+返回值

        FutureTask<Integer> ft1=new FutureTask<>(mc);//接受mc返回的返回值
        FutureTask<Integer> ft2=new FutureTask<>(mc);//接受mc返回的返回值

        //创建线程
        Thread t1=new Thread(ft1);
        Thread t2=new Thread(ft2);

        t1.setName("抽箱一");
        t2.setName("抽箱二");

        t1.start();
        t2.start();

        //接受返回值
        System.out.println(ft1.get());
        System.out.println(ft2.get());

    }
}
