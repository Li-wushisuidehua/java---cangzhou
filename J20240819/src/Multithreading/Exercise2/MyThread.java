package Multithreading.Exercise2;

public class MyThread extends Thread{
    public static int ticket=100;
    @Override
    public void run() {
        while(true){
            synchronized (MyThread.class){
                if(ticket<10){
                    break;
                }else{
                    ticket--;
                    System.out.println(getName()+"送出一份礼物还剩余"+ticket+"份礼物");
                }
            }
        }
    }
}
