import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestJDBC3 {
    //实现在终端实行的各种命令语句
    public static void main(String[] args) throws SQLException {
        //创建数据源
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/java112?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("050217");
         //数据库与服务器建立网络连接
        Connection connection=dataSource.getConnection();
        //sql语句
        //删除语句
        //String sql="delete from student where id=5";
        //插入语句
       // String sql="insert into student values(5,'田七'),(6,'妞子')";
        //更新语句
        String sql="update student set name='limingyue' where name='wangzhaohan'";
        PreparedStatement statement=connection.prepareStatement(sql);
        int n=statement.executeUpdate();
        System.out.println(n);
    }
}
