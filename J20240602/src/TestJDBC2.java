import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestJDBC2 {
    public static void main(String[] args) throws SQLException {
        //创建数据源
        DataSource dataSource=new MysqlDataSource();
        //数据源具体化
        ((MysqlDataSource) dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java112?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource) dataSource).setUser("root");
        ((MysqlDataSource) dataSource).setPassword("050217");
        //数据库与服务器建立网络链接
        Connection connection= dataSource.getConnection();
        //sql语句
        String sql="select * from student";
        PreparedStatement statement=connection.prepareStatement(sql);
        ResultSet resultSet=statement.executeQuery();
        while(resultSet.next()){
            System.out.print(resultSet.getInt("id")+" ");
            System.out.print(resultSet.getString("name"));
            System.out.println();
        }
        //释放资源
        statement.close();
        connection.close();
    }
}
