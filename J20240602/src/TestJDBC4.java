
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestJDBC4 {
    //使用JDBC实现多种查询，顺带加复习
    public static void main(String[] args) throws SQLException {
        //创建数据源
        DataSource dataSource=new MysqlDataSource();
        //数据源具体化
        (( MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/java112?characterEncoding=utf8&useSSL=false");
        (( MysqlDataSource)dataSource).setUser("root");
        (( MysqlDataSource)dataSource).setPassword("050217");
        //连接数据库
        Connection connection=dataSource.getConnection();
        //书写sql语句
        String sql="select* from student as stu1,student as stu2 ";
        //进行预编译
        PreparedStatement statement=connection.prepareStatement(sql);
        ResultSet result=statement.executeQuery();
        while(result.next()){
            System.out.print(result.getInt("stu1.id")+"  "+result.getString("stu1.name")+" ");
            System.out.println(result.getInt("stu2.id")+" "+result.getString("stu2.name"));

        }
        //使用完资源后需要关闭资源
        statement.close();
        connection.close();




    }
}
