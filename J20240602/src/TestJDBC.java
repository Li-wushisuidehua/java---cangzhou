import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class TestJDBC {
    public static void main(String[] args) throws SQLException {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入id ");
        int id=scanner.nextInt();
        System.out.println("请输入name ");
        String name=scanner.next();
        //创建数据源
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/java112?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("050217");

        //和数据库服务器建立网络连接
        Connection connection=dataSource.getConnection();
        //System.out.println(connection);

        //程序构造sql语句
        /*
        插入语句
       String sql="insert into student values(5,'田七')";
       String sql="insert into student values(?,?)";
         */
        /*
        更新语句
         */
        String sql="insert into student values("+ id +",'"+ name +"')";
        //把sql语句发送到服务器上，让服务器执行
        PreparedStatement statement=connection.prepareStatement(sql);
//        statement.setInt(1,id);
//        statement.setString(2,name);
        //删除，增加，修改等这些操作均算做executeUpdate，查询是使用executeQuery
        int n=statement.executeUpdate();
        System.out.println(n);
         //释放资源
        statement.close();
        connection.close();

    }
}
