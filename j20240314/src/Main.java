import Person.AdmUser;
import Person.OrdinaryUser;
import Person.User;
import java.util.Scanner;


public class Main {
    public static User login(){
        System.out.println("请输入你的名字：");
        Scanner scanner=new Scanner(System.in);
        String name=scanner.nextLine();
        System.out.println("请输入你的身份：1、管理员 2、普通用户");
        int choice =scanner.nextInt();
        if(choice==1){
            return new AdmUser(name);//实例化对象
        }
        else  {
            return new OrdinaryUser(name);
        }
    }
    public static void main(String[] args) {
       BookList bookList=new BookList();
       User user=login();
      int choice= user.menu();
      user.dofunction(choice,bookList);






    }
}
