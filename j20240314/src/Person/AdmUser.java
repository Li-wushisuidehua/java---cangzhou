package Person;

import function.*;

import java.util.Scanner;

public class AdmUser extends User{
    public AdmUser(String name) {
        super(name);
        this.function=new Function[]{
                new exitbook(),
                new findbook(),
                new addbook(),
                new deketbook(),
                new showbook()

        };//接口的向上转型
    }

    @Override
    public int menu() {
        System.out.println("*******管理员菜单*******");
        System.out.println("1、查找图书");
        System.out.println("2、新增图书");
        System.out.println("3、删除图书");
        System.out.println("4、显示图书");
        System.out.println("0、退出系统");
        System.out.println("请输入你的操作：");
        Scanner scanner=new Scanner(System.in);
        int choice =scanner.nextInt();
        return choice;
    }
}
