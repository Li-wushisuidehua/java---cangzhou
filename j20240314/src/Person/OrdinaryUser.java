package Person;

import function.*;

import java.util.Scanner;

public class OrdinaryUser extends User{
    public OrdinaryUser(String name) {
        super(name);
       this.function = new Function[]{
               new exitbook(),
                new findbook(),
                new borrow(),
                new backbook()

        };
    }

    @Override
    public int menu() {
        System.out.println("********用户菜单********");
        System.out.println("1、查找图书");
        System.out.println("2、借阅图书");
        System.out.println("3、归还图书");
        System.out.println("0、退出系统");
        System.out.println("请输入你要选择的操作：");
        Scanner scanner=new Scanner(System.in);
        int choice=scanner.nextInt();
        return choice;
    }
}
