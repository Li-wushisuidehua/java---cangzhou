import java.io.File;

//删除非空文件夹
public class Demo8 {
    public static void main(String[] args) {
        File file=new File("C:/aaa/");

        //创建文件夹
       // System.out.println(file.mkdirs());
       // System.out.println(file.delete());
        deleteFile(file);

    }
    public static void deleteFile(File file){
        if( file==null || !file.isDirectory() || !file.exists()){
            return ;
        }
        File []files=file.listFiles();

        if(files==null || files.length==0) {
            return ;
        }
        for (File f:files) {
            if(f.isFile()){
                f.delete();
            }else{
                deleteFile(f);
                f.delete();
            }
        }
        System.out.println(file.delete());
    }
}
