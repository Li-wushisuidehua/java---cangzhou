import java.io.File;
import java.io.IOException;

public class Demo1 {
    public static void main(String[] args) throws IOException {
        File file=new File("./test.txt");
        System.out.println(file.getParent());//获得父目录
        System.out.println(file.getName());//获得文件名字
        System.out.println(file.getPath());//获得File对象的文件路径
        System.out.println(file.getAbsoluteFile());//获得File对象的绝对路径
        System.out.println(file.getCanonicalFile());//获得File对象的修饰过的绝对路径
    }

}
