import java.io.File;

//遍历java---cangzou下的所有文件
public class Demo6 {
    public static void main(String[] args) {
        File file=new File("C:\\java\\java---cangzhou");
        scan(file);
    }
    public static void scan(File file){
        //判断是否为目录
        if(!file.isDirectory()) return;

        //列出目录的清单
        File[] f=file.listFiles();
        if(f==null || f.length==0){//当file表示文件，或者路径不对的时候会返回null，如果为空文件length会为0;
            // 不存在的路径 / 空目录
            return ;
        }

        //打印出当前目录名称
        System.out.println(file.getAbsoluteFile());

        //依次遍历进行判断、打印
        for (File file1:f) {
            if(file.isFile())
                System.out.println(file1.getAbsoluteFile());

            else{
                scan(file1);

            }
        }


    }
}
