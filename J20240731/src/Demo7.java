import java.io.File;
import java.io.IOException;
import java.util.Random;

//查找到c盘地下的QQ.exe
public class Demo7 {
    public static void main(String[] args) throws IOException {
        File file=new File("C:/");
        scanf(file,"QQ.exe");
    }
    public static void scanf(File file,String name) throws IOException {
        if(file==null || !file.exists() ||file.isFile()) {
            return;
        }

        //获取filed目录底下的所有文件对象
      File []file1=file.listFiles();

        //没有访问权限 / 目录为空目录
      if(file1==null || file1.length==0) return;

      //此时可以确保既不是空目录也有访问权限
       for (File f:file1) {
            if(f.isFile()){
                if(f.getName().equals(name)){
//                    System.out.println("找到了"+ f.getAbsoluteFile());
                    Runtime runtime=Runtime.getRuntime();
                    runtime.exec(f.getAbsolutePath());
                }
            }else{
                //传递过去的为目录
                scanf(f,name);
            }
        }
    }
}
