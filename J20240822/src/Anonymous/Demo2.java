package Anonymous;

public class Demo2 {
    public static void main(String[] args) {
//        String name="丸子";
//        new Pepole(){
//            @Override
//            public void age() {
//                System.out.println(name+"今年刚满18岁");
//            }
//        }.age();

        //匿名内部类还可以作为参数进行传递
        function(new Pepole() {
            @Override
            public void age() {
                System.out.println("刚满18岁");
            }
        });

    }
    public static void function(Pepole student){
        student.age();
    }
}
