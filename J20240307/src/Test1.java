//组合(eg:car)
class Tire{
    //......;
}
class Color{
    //......;
    public String color="黑色";
    public void show(){
        System.out.println("这辆车是黑色的！");
    }
}
class Engine{
    //.......;
}
//一个类(可以看成一个车的模型）;
public class Test1 {
    private Tire tire;//可以复用轮胎中的属性和方法；
    private Color color;//可以复用颜色中的属性和方法；
    private Engine engine;//可以复用引擎中的属性和方法；
    Color getColor(){
        return color;
    }
}

class Test2 extends Test1{
   //一个对象,可以看成是模型的实例化;
    Test2 Benz=new Test2();
    Test1 test1=new Test1();
    Color color=new Color();


}