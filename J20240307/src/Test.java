//继承、多态
class Shape{
    public void draw(){

        System.out.println("打印图形");
    }
}
class Rect extends Shape{
    @Override
    public void draw() {
        System.out.println("打印矩形！");
    }
}
class Cycle extends Shape{
    @Override
    public void draw() {
        System.out.println("打印圆形！");
    }
}
class Flower extends Shape{
    @Override
    public void draw() {
        System.out.println("打印花朵！");
    }
}
class Dog extends Shape{
    @Override
    public void draw() {
        System.out.println("打印小狗！");
    }
}
public class Test {

   public static void  func(){
        Shape shape=new Rect();
        Shape shape1=new Cycle();
        Shape shape2=new Flower();
        Dog dog=new Dog();
        Shape []shape5={shape,shape1,shape2,dog};
        for (Shape sha: shape5) {
            sha.draw();
        }
    }
    public static void main(String[] args) {
         func();

    }
    /*
    //下面这个方法是错误的，由于func没有static修饰，没有static修饰时为实例函数，实例函数在静态函数中不能调用；上面的方法是正确的
    public func(){
        Shape shape=new Rect();
        Shape shape1=new Cycle();
        Shape shape2=new Flower();
        Dog dog=new Dog();
        Shape []shape5={shape,shape1,shape2,dog};
        for (Shape sha: shape5) {//  for each写法（遍历数组）
            sha.draw();
        }
    }
    public static void main(String[] args) {
        func();

    }

     */
    //向上转型的三种写法；
    /*
    class A{
    .......;
    }
    class Bextend A{
    ........;
    }
    //直接进行赋值，直接进行向上转化；
    1.   public static void main(String[] args) {
         A a=new B();
    }
    2.通过返回值；
    public static A hah(){
     return new B();
    }
    3.方法传参进行赋值
    public static void hah(A a){
    .......;
    }
    public static void main(){
    .......;
    B b=new B();
    hah(b);

     */
}
