import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//优选算法精品课
public class Test {
    public static void main(String[] args) {
        int []arr=new int[]{2,2,2,2,2,2,2,2,2,2,2,2,2};

//        int []arr1=new int[]{1,0,2,3,0,4};
//  //      duplicateZeros(arr);
//        duplicateZeros(arr1);
//   //     System.out.println(Arrays.toString(arr));
//        System.out.println(Arrays.toString(arr1));
//        int n=19;
//     boolean flag= isHappy(19);
//        System.out.println(flag);
        List<List<Integer>> list=fourSum(arr,8);
        for (List<Integer> str:list) {
            System.out.println(Arrays.toString(str.toArray()));;

        }

    }
    //LeetCode代码题
    //1.复写零
    /*
        public static void duplicateZeros(int []arr){
        int cur = 0;
        int dest=-1;
        for (; cur < arr.length; cur++) {
            if(dest< arr.length){
                if(arr[cur]==0){
                    dest+=2;
                }else{
                    dest+=1;
                }
                if(dest>= arr.length-1){
                    break;
                }
            }


        }
        for ( ;cur>=0;cur--){
            if(arr[cur]==0){
                if(dest> arr.length-1){
                    dest-=2;
                    arr[dest+1]= arr[cur];
                }else{
                    int k=2;
                   while(k>0){
                       arr[dest]=arr[cur];
                       dest--;
                       k--;
                   }
                }
            }else{
                    arr[dest]= arr[cur];
                    dest--;

            }

        }
    }
     */

    //覆写0优化
    public static void duplicateZeros(int []arr){
        int cur = 0;
        int dest=-1;
        int n=arr.length;
        for (; cur < arr.length; cur++) {
            //这里dest代表的是下标
                if(arr[cur]==0){
                    dest+=2;
                }else{
                    dest+=1;
                }
                if(dest>= arr.length-1){
                    break;
                }
        }
        if(dest==n) {
            dest--;
            arr[dest--]=0;
            cur--;
        }
        for ( ;cur>=0;cur--){
            if(arr[cur]==0){
              arr[dest--]=0;
              arr[dest--]=0;

            }else{
                arr[dest]= arr[cur];
                dest--;

            }

        }
    }
    //快乐数的代码实现
    public static int countSum(int n){
        int sum=0;
        while(n!=0){
            int k=n%10;
            sum+=k*k;
            n/=10;
        }
        return sum;
    }
    public static boolean isHappy(int n){
       int slow=countSum(n);
       int fast=countSum(slow);
       while(fast!=slow){
           slow=countSum(slow);
           fast=countSum(countSum(fast));
       }
           return fast==1;
    }
    //盛水最多的容器
    public int maxArea(int[] height) {
        int left=0;
        int right=height.length-1;
        int max=0;
        int v1=0;
        while (left<=right) {
            if(height[left]>height[right]){
                v1=(right-left)*height[right];
                if(v1>max){
                    max=v1;
                }
                right--;
            }else{
                v1=(right-left)*height[left];
                if(v1>max){
                    max=v1;
                }
                left++;
            }
        }
        return max;
    }
//盛水最多的容器代码优化版本
public int maxArea2(int[] height) {
    int left=0;
    int right=height.length-1;
    int max=0;
    int v1=0;
    while (left<=right) {
        //获取到容积
      v1= Math.min(height[left],height[right])*(right-left);
      //获取到最大的容积量
      max=Math.max(v1,max);
      //移动双指针
        if(height[right]>height[left]){
            left++;
        }else{
            right--;
        }

    }
    return max;
}
//有效三角形的个数
    public int triangleNumber(int[] nums) {
        Arrays.sort(nums);
        int count = 0;
        int max = nums.length - 1;
        for ( ; max > 0; max--) {
            int left = 0;
            int right = max - 1;
            while(left<right) {
                if (nums[left] + nums[right] > nums[max]) {
                    count += right - left;
                    right--;
                } else {
                    left++;
                }
            }
        }
        return count;
    }
    //三数之和且不能有重复的数组
    public List<List<Integer>> threeSum(int[] nums) {
        //排序
        Arrays.sort(nums);
        int target=0;
        List<List<Integer>> list1=new ArrayList<>();
        for(int i=0;i<nums.length;i++){
            if(nums[i]<=0){
                //对每一次的相加的目标值去重
                if(i>0 && nums[i]==nums[i-1]){
                    continue;
                }
                 target=nums[i];
            }else{
                return list1;
            }
            int left=i+1;
            int right=nums.length-1;
            while(left<right){
                int sum=nums[left]+nums[right];
                if(sum==(-target)){
                    list1.add(new ArrayList<>(Arrays.asList(nums[left],nums[right],nums[i])));
                    left++;
                    right--;
                    while(left<right && (nums[left]==nums[left-1] || nums[right]==nums[right-1])){
                        if(nums[left]==nums[left-1]){
                            left++;
                        }else{
                            right--;
                        }
                    }
                }else if(sum>(-target)){
                    right--;
                }else{
                    left++;
                }
            }
        }
        return list1;
    }
//四数之和
    public static List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);// 对数组进行排序
        List<List<Integer>> list1 = new ArrayList<>();
        for(int i=0;i<nums.length;i++){
           if(i>0 && nums[i]==nums[i-1]){
               continue;
           }
           for(int j=i+1;j<nums.length;j++){
               if(j>i+1 && nums[j]==nums[j-1]){
                   continue;
               }
               int left=j+1;
               int right=nums.length-1;
               while (left < right) {
                   if ((long)nums[i]+nums[j]+nums[left]+nums[right]==(long)target) {
                       list1.add(new ArrayList<>(Arrays.asList(nums[i], nums[j], nums[left], nums[right])));
                       left++;
                       right--;
                       // 不漏的第一步
                       while (left < right) {
                           if ((nums[left] == nums[left - 1] || nums[right] == nums[right + 1])) {
                               if(nums[left]==nums[left-1]){
                                   left++;
                               }
                               if(nums[right]==nums[right+1]){
                                   right--;
                               }

                           }else{
                               break;
                           }
                       }
                   }else if((long)nums[i]+nums[j]+nums[left]+nums[right]<(long)target){
                       left++;
                   }else{
                       right--;
                   }
               }
           }

       }
        return list1;
    }

    /*
    四数之和
    class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);
        List<List<Integer>> quadruples = new ArrayList<>(); // 四元组

        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) continue;

            for (int j = i + 1; j < nums.length; j++) {
                if (j > i + 1 && nums[j] == nums[j - 1]) continue;

                int l = j + 1;
                int r = nums.length - 1;

                while (l < r) {
                    if (r < nums.length - 1 && nums[r] == nums[r + 1]) {
                        r--;
                    } else if (l > j + 1 && nums[l] == nums[l - 1]) {
                        l++;
                    } else if ((long) nums[i] + nums[j] + nums[l] + nums[r] > target) {
                        r--;
                    } else if ((long) nums[i] + nums[j] + nums[l] + nums[r] < target) {
                        l++;
                    } else {
                        quadruples.add(Arrays.asList(nums[i], nums[j], nums[l], nums[r]));
                        l++; r--;
                    }
                }
            }
        }

        return quadruples;
    }
}
     */
    /*
    class Solution {
    public static List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);// 对数组进行排序
        long num=0;
        List<List<Integer>> list1 = new ArrayList<>();
       for(int i=0;i<nums.length;i++){
           for(int j=i+1;j<nums.length;j++){
              num=(long)target-nums[i]-nums[j];
               int left=j+1;
               int right=nums.length-1;
               while (left < right) {
                   if (nums[left]+nums[right]==num) {
                       list1.add(new ArrayList<>(Arrays.asList(nums[i], nums[j], nums[left], nums[right])));
                       left++;
                       right--;
                       // 不漏的第一步
                    while(left<nums.length && nums[left]==nums[left-1]) left++;
                       while(right<nums.length && nums[right]==nums[right+1]) right--;
                   }else if(nums[left]+nums[right]<target){
                       left++;
                   }else{
                       right--;
                   }
               }
               while(j+1<nums.length && nums[j+1]==nums[j]){
                   j++;
               }
           }
           while(i+1<nums.length && nums[i+1]==nums[i]){
               i++;
           }
       }
        return list1;
    }
}
     */
}

