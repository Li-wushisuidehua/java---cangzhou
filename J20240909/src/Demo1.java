public class Demo1 {
    public static void main(String[] args) {
        int []arr=new int[]{0,1,2,2};
        System.out.println(totalFruit(arr));
    }
    public static int totalFruit(int[] fruits) {
        int left=0,right=0,ret=0,len=fruits.length,kinds=0;
        int []hash=new int[len+1];
        while(right<len){
            if(hash[fruits[right]]==0) kinds++;
            hash[fruits[right]]++;
                while(kinds>=3){
                    int out=--hash[fruits[left]];
                  if(out==0) kinds--;
                  left++;
                }

            ret=Math.max(ret,right-left+1);
            right++;
        }
        return ret;

    }
}
