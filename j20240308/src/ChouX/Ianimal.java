package ChouX;
//接口
//
//public interface Ianimal {
//    String name="坤坤";
//    void basketball();
//
//}
//class Chicken implements Ianimal{
//    @Override
//    public void basketball() {
//        System.out.println(name+"正在打篮球！");
//    }
//}
//class Dog implements Ianimal{
//    String dag="坤弟";
//    Chicken pig=new Chicken();
//    @Override
//    public void basketball() {
//        System.out.println(pig.pig+dag+name+"他们三个正在一起打篮球！");
//    }
//}
//class Test{
//    public  static void basketball2(Ianimal animal){
//        animal.basketball();
//
//
//    }
//    public static void main(String[] args) {
//        Dog dog=new Dog();
//        Chicken chicken=new Chicken();
//        basketball2(dog);
//        basketball2(chicken);
////        basketball2(new Dog());
////        basketball2(new Chicken());
//    }
//定义接口
public interface Ianimal {
    String name="坤坤";
    void basketball();
}
//实现接口
class Chicken implements Ianimal{
    @Override
    public void basketball() {
        System.out.println(name+"正在打篮球！");
    }
}
    class Test{
        public  static void basketball2(Ianimal animal){
            animal.basketball();
        }
        public static void main(String[] args) {
            Chicken chicken=new Chicken();//实例化对象
            basketball2(chicken);//向上转型
        }
    }
//}