////抽象
//abstract class Animal {
//private  int age;
//public String name;
//public Animal(int age,String name){
//this.age=age;
//this.name=name;
//
//}
//int getAge(){
// return age;
//}
//String getName(){
//    return name;
//}
//public  abstract  void eat();
//public  void show(){
//    System.out.println("好开心");
//};
//}
////================
//  class  dog extends Animal{
//  public dog(){
//  super(18,"美丽");
// }
// @Override
// public void eat() {
//  System.out.println(name+"这只小狗正在吃粑粑！");
// }
//
//    @Override
//    public void show() {
//        System.out.println(name+"好开心");
//    }
//
//    public  static void eatFood(Animal animal){
//     animal.eat();
// }
// public  static void showhappy (Animal animal){
//      animal.show();
// }
//// public static void func(){
////     System.out.println("哈哈哈哈");
//// }
// public static void main(String[] args) {
//   dog dog=new dog();
//    eatFood(dog);
//  showhappy(dog);
//   // func();
// }
//}
//抽象
//abstract class Animal {
//    private  int age;
//    public String name;
//    public  abstract  void eat();
//}
////================
//abstract  class  dog extends Animal{
////    @Override
////    public void eat() {
////        System.out.println(name+"这只小狗正在吃粑粑！");
////    }
//    public  static void eatFood(Animal animal){
//        animal.eat();
//    }
//    public  static void showhappy (Animal animal){
//        animal.show();
//    }
//    // public static void func(){
////     System.out.println("哈哈哈哈");
//// }
//    public static void main(String[] args) {
//        dog dog=new dog();
//        eatFood(dog);
//        showhappy(dog);
//        // func();
//    }
//}
//抽象
abstract class Animal {
    private  int age;
    public String name;
    public Animal(int age,String name){
        this.age=age;
        this.name=name;
    }
    public  abstract  void eat();
}
//================
class  dog extends Animal{
    public dog(){
        super(18,"美丽");
    }
    @Override
    public void eat() {
        System.out.println(name+"这只小狗正在吃粑粑！");
    }

    @Override
    public void show() {
        System.out.println(name+"好开心");
    }

    public  static void eatFood(Animal animal){
        animal.eat();
    }
    public  static void showhappy (Animal animal){
        animal.show();
    }
    // public static void func(){
//     System.out.println("哈哈哈哈");
// }
    public static void main(String[] args) {
        dog dog=new dog();
        eatFood(dog);
        showhappy(dog);
        // func();
    }
}