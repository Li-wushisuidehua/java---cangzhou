public class Demo2 {

    public int[] reverseBookList(ListNode1 head) {
        int [] arr=new int[10000];
        int size=0,left=0,right=0;
        ListNode1 cur=head;
        while(cur!=null){
            arr[size++]=cur.val;
            cur=cur.next;
        }
        right=size-1;
        while(left<right){
            int tmp=arr[left];
            arr[left]=arr[right];
            arr[right]=tmp;
            left++;right--;
        }
        int [] ret=new int[size];
        for(int i=0;i<size;i++){
            ret[i]=arr[i];
        }
        return ret;

    }
}
 class ListNode1 {
     public int val;
     public ListNode1 next;
      ListNode1(int val) {
          this.val = val; }

  }
