//题目中一定一定要注意最终的n的善后
public class Demo5 {
        public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
            ListNode cur1 = l1;
            ListNode cur2 = l2;
            ListNode ret = new ListNode(0);
            ListNode cur = ret;
            int sum = 0, n = 0;
            while (cur1 != null && cur2 != null) {
                sum = cur1.val + cur2.val + n;
                n=0;
                if (sum >= 10) {
                    n = sum / 10;
                    sum %= 10;
                }
                cur.next = new ListNode(sum);
                cur = cur.next;
                sum = 0;
                cur1 = cur1.next;
                cur2 = cur2.next;
            }
            while (cur1 != null) {
                sum += cur1.val + n;
                n=0;
                if (sum >= 10) {
                    n = sum / 10;
                    sum %= 10;
                }
                cur.next = new ListNode(sum);
                cur = cur.next;
                sum = 0;
                cur1 = cur1.next;
            }
            while (cur2 != null) {
                sum += cur2.val + n;
                n=0;
                if (sum >= 10) {
                    n = sum / 10;
                    sum %= 10;
                }
                cur.next = new ListNode(sum);
                cur = cur.next;
                sum = 0;
                cur2 = cur2.next;
            }
            //l1= 9，9，9，9
            //l2=9，9，9
            //最后的结果sum=10，sum%10了最终一定要记得n=sum/10（n=1）的n要加入链表
            if(n!=0){
                cur.next=new ListNode(n);
            }

            return ret.next;
        }

    public static void main(String[] args) {
        ListNode node1=new ListNode(9);
        ListNode node2=new ListNode(9);
        ListNode node3=new ListNode(9);
        ListNode node4=new ListNode(9);
        ListNode node5=new ListNode(9);

        node1.next=node2;
        node2.next=node3;
        node4.next=node5;


        ListNode node= addTwoNumbers(node1,node4);
        while(node!=null){
            System.out.print(node.val);
            node=node.next;
        }
    }
}
