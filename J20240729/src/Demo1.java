public class Demo1 {
    public static int dayOfYear(String date) {
        String[] strings = date.split("-");
        int sumOfday=0,day=28;
        int n=Integer.valueOf(strings[0]);
        if(n%4==0 && n%100!=0 ||n%400==0) day=29;
        int m = Integer.valueOf(strings[1]);
        int [] days=new int[]{31,day,31,30,31,30,31,31,30,31,30,31};
        for (int i = 0; i < m-1; i++) {
            sumOfday+=days[i];
        }
        sumOfday+=Integer.valueOf(strings[2]);
        return sumOfday;
    }

    public static void main(String[] args) {
        String str = "20-03-01";
        System.out.println(dayOfYear(str));
    }
}
