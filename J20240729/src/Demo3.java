import java.util.HashMap;
import java.util.Map;

public class Demo3 {
    public static ListNode removeDuplicateNodes(ListNode head) {
//        HashMap<Integer, Integer> hashMap = new HashMap<>();
//        ListNode cur = head;
//        while (cur != null) {
//
//            if (!hashMap.containsValue(cur.val)) {
//                hashMap.put(cur.val, cur.val);
//            }
//            cur = cur.next;
//        }
//        ListNode ret = new ListNode(0);
//        cur=ret;
//        for (Map.Entry<Integer,Integer> entry:hashMap.entrySet()) {
//            ListNode node=new ListNode(entry.getValue());
//            cur.next=node;
//            cur=cur.next;
//        }
//return ret.next;
        //注意：hashmap和hashset均会重排序这些int类型的数据
        ListNode cur=head;
        ListNode ret=new ListNode(0);
        ListNode curN=ret;
        int [] arr=new int[20000];
        int size=0;
        while(cur!=null){
           if(++arr[cur.val]==1){
               curN.next=new ListNode(cur.val) ;
               curN=curN.next;
           }
            cur=cur.next;
        }
        return ret.next;

    }

    public static void main(String[] args) {
        ListNode node1=new ListNode(1);
        ListNode node2=new ListNode(2);
        ListNode node3=new ListNode(3);
        ListNode node4=new ListNode(3);
        ListNode node5=new ListNode(2);
        ListNode node6=new ListNode(1);
        node1.next=node2;
        node2.next=node3;
        node3.next=node4;
        node4.next=node5;
        node5.next=node6;

      ListNode node=  removeDuplicateNodes(node1);
      while(node!=null){
          System.out.print(node.val);
          node=node.next;
      }
    }
}

class ListNode {
    public int val;
    public ListNode next;

    ListNode(int val) {
        this.val = val;
    }

}
