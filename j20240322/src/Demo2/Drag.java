package Demo2;

import jdk.swing.interop.SwingInterOpUtils;

public class Drag extends Animal implements Leaping,Swimming{
    public Drag(String name) {
        super(name);
    }

    @Override
    public void leap() {
        System.out.println("青蛙正在跳！！！");
    }

    @Override
    public void swim() {
        System.out.println("青蛙正在游泳！！");
    }

    public static void main(String[] args) {
        Drag drag=new Drag("小青蛙");
        drag.leap();
        drag.swim();

    }
}
