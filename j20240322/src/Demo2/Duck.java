package Demo2;

public class Duck extends Animal implements Running,Flying,Swimming{
    public Duck(String name) {
        super(name);
    }
    @Override
    public void fly() {
        System.out.println("鸭子正在飞！！！");
    }
    @Override
    public void run() {
        System.out.println("鸭子正在跑！！！");

    }
    @Override
    public void swim() {
        System.out.println("鸭子正在游泳！！！");
    }
    public static void main(String[] args) {
        Duck duck=new Duck("小黄鸭");
        duck.fly();
        duck.run();
        duck.swim();
    }
}
