package Demo4;

public class Student implements Cloneable{
    public String name;
    public int age;
    public void eat(){
        System.out.println(this.name+"喜欢吃饭");
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args)throws CloneNotSupportedException {
        Student student=new Student("小明",19);
        Student student1=(Student) student.clone();
    }
}
