package Demo3;

import jdk.swing.interop.SwingInterOpUtils;

public class Frog implements IAmphibious{
    public Frog(String name) {
        this.name = name;
    }

    public String name;

    @Override
    public void run() {
        System.out.println(this.name+"正在跑步");
    }

    @Override
    public void swim() {
        System.out.println(this.name+"正在游泳");
    }

    public static void main(String[] args) {
        Frog frog=new Frog("小青蛙");
    }
}
