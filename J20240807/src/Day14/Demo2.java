package Day14;

public class Demo2 {
    public static void main1(String[] args) {
        int [] arr=new int[]{4,1,2};
        int [] arr1=new int[]{1,3,4,2};
        for (int m:nextGreaterElement(arr,arr1)) {
            System.out.println(m);
        }

    }
    //此算法题解时间为5ms，下文的题解算法为0ms
    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int [] ret=new int [nums1.length];
        int flag=0;
        for(int i=0;i<nums1.length;i++){
            flag=0;
            for(int j=0;j<nums2.length;j++){
                if(nums1[i]==nums2[j] &&j+1<nums2.length ){
                    if(nums2[j+1]>nums1[i]){
                        ret[i]=nums2[j+1];
                        flag=1;
                        break;
                    }else{
                       j+=2;
                       while(j<nums2.length){
                           if(nums2[j]>nums1[i]){
                               ret[i]=nums2[j];
                               flag=1;
                               break;
                           }
                           j++;
                       }
                    }
                }
            }
            if(flag==0) ret[i]=-1;
        }
        return ret;

    }

    //算法题解时间复杂度为0ms
    public static int[] nextGreaterElement1(int[] nums1, int[] nums2) {
        int n2 = nums2.length;
        int[] map = new int[10001];
        int[] stack = new int[n2];
        int top = -1;
        for(int i = 0 ; i < n2 ; i ++){
            while(top != -1 && stack[top] < nums2[i]){
                map[stack[top]] = nums2[i];
                top--;
            }
            stack[++top] = nums2[i];
        }
        while(top != -1){
            map[stack[top--]] = -1;
        }
        int[] result = new int[nums1.length];
        for(int i = 0 ; i < nums1.length ; i ++){
            result[i] = map[nums1[i]];
        }
        return result;
    }
    public static void main(String[] args) {
        int [] arr=new int[]{4,1,2};
        int [] arr1=new int[]{4,2,1,7};
        for (int m:nextGreaterElement1(arr,arr1)) {
            System.out.println(m);
        }

    }
}
