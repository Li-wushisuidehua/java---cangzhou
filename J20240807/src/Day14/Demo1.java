package Day14;

import java.util.ArrayList;
import java.util.List;

public class Demo1 {
    public static void main(String[] args) {
        int []target=new int[]{1,2,3};
        int n=3;
       List<String> strings= buildArray(target,n);
        for (String s:strings) {
            System.out.print(s+" ");
        }
    }
    public static List<String> buildArray(int[] target, int n) {
        List<String> list=new ArrayList<>();
        for (int i = 1,j=0;i <= n && j<target.length; ) {
            if(target[j]==i){
                list.add("Push");
                j++;
                i++;
            }else{
                int m=target[j]-i;
                while((m--)>0){
                    list.add("Push");
                    list.add("Pop");
                    i++;
                }
            }
        }
        return list;
    }
}
