package Day13;

public class Demo2 {
    public static void main(String[] args) {

    }

    /*
    环形链表求环形的入口点
     */
    public ListNode2 detectCycle(ListNode2 head) {
        ListNode2 fast = head;
        ListNode2 slow = head;
        int flag=0;
        if (head == null || fast.next == null) {
            return null;
        }
        // 寻找相遇点
        while (fast != null && fast.next!=null) {
            fast = fast.next.next;
            slow = slow.next;
            if (fast ==slow){
                fast=head;
                flag=1;
                break;
            }

        }
        if (flag==1) {
            while (fast != slow) {
                fast = fast.next;
                slow = slow.next;
            }
            return slow;
        }

        return null;

    }
}
class ListNode2{
    int val;
    ListNode2 next;
    public ListNode2(int val) {
        this.val = val;
    }

}
