package Day13;

//反转链表
public class Demo1 {
    public static void main(String[] args) {
        ListNode1 node1=null;
//        ListNode1 node2=new ListNode1(2);
//        ListNode1 node3=new ListNode1(3);
//        ListNode1 node4=new ListNode1(4);
//        ListNode1 node5=new ListNode1(5);
//        ListNode1 node6=new ListNode1(6);
//        ListNode1 node7=new ListNode1(7);
//        node1.next=node2;
//        node2.next=node3;
//        node3.next=node4;
//        node4.next=node5;
//        node5.next=node6;
//        node6.next=node7;
       ListNode1 cur= reverseList(node1);
        while(cur!=null){
            System.out.println(cur.val);
            cur=cur.next;
        }
    }
    public static ListNode1 reverseList(ListNode1 head) {
        ListNode1 cur=head;
        ListNode1 curN=cur.next;
        ListNode1 prev=null;
        cur.next=null;

        //判断防止为null
        if(cur==null || curN==null) return head;
        //循环遍历，倒转链表
        while(curN!=null){
            prev=curN.next;
            curN.next=cur;
            cur=curN;
            curN=prev;
        }
        return cur;
    }
}
class ListNode1{
    int val;
    ListNode1 next;
    public ListNode1(int val) {
        this.val = val;
    }

}
