package Day11;

public class Demo2 {
    public static void main(String[] args) {
        ListNode2 head=new ListNode2(4);
        ListNode2 node1=new ListNode2(2);
        ListNode2 node2=new ListNode2(1);
       // ListNode node3=new ListNode(3);
        head.next=node1;
        node1.next=node2;
       // node2.next=node3;
        oddEvenList(head);
    }
    public static ListNode2 oddEvenList(ListNode2 head) {
        if(head==null || head.next==null || head.next.next==null) return head;
        ListNode2 left=head;
        ListNode2 right=left.next;
        ListNode2 cur=right.next;
        ListNode2 curN=cur;
        while(cur!=null && curN!=null){
            curN=cur.next;
            right.next=cur.next;
            cur.next=left.next;
            left.next=cur;
            left=left.next;
            if(curN!=null)
            cur=curN.next;
            right=right.next;
        }
        return head;
    }

}
class ListNode2{
    public int val;
    public ListNode2 next;

    public ListNode2(int val) {
        this.val = val;
    }
}
