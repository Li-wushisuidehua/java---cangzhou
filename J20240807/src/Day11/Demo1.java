package Day11;
//使用归并排序对链表进行排序
public class Demo1 {
    public static void main(String[] args) {
        ListNode2 head=new ListNode2(4);
        ListNode2 node1=new ListNode2(2);
        ListNode2 node2=new ListNode2(1);
        ListNode2 node3=new ListNode2(3);
        head.next=node1;
        node1.next=node2;
        node2.next=node3;
        sortList(head);

    }
    public static ListNode2 sortList(ListNode2 head) {
      return  toSortList(head,null);
    }

    public static ListNode2 toSortList(ListNode2 head,ListNode2 tail){
        if(head==null) return head;
        if(head.next==tail){
            head.next=null;
            return head;
        }
        ListNode2 slow=head;
        ListNode2 fast=head;
        while(fast!=tail){
            slow=slow.next;
           fast=fast.next;
           if(fast!=tail) fast=fast.next;
        }
        ListNode2 mid=slow;
        ListNode2 node1=toSortList(head,mid);
        ListNode2 node2=toSortList(mid,tail);
        return Sort(node1,node2);
    }
    public static ListNode2 Sort(ListNode2 node1,ListNode2 node2){
        ListNode2 ret=new ListNode2(0);
        ListNode2 cur=ret;
        while(node1!=null && node2!=null){
            if(node1.val>=node2.val){
                cur.next=node2;
                cur=node2;
                node2=node2.next;
            }else{
                cur.next=node1;
                cur=node1;
                node1=node1.next;
            }

        }
        if(node1!=null){
            cur.next=node1;
        }
        if(node2!=null){
            cur.next=node2;
        }
        return ret.next;
    }
}
//class ListNode{
//    public int val;
//    public ListNode next;
//
//    public ListNode(int val) {
//        this.val = val;
//    }
//}
