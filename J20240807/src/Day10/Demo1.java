package Day10;

import java.util.List;

//从链表中删去总和值为 0 的节点,此解法时间复杂度为2ms，接下来写一个时间为1秒的解法
public class Demo1 {
    public ListNode4 removeZeroSumSublists1(ListNode4 head) {
        if(head==null) return head;
        ListNode4 dummyHead=new ListNode4(0);
        dummyHead.next=head;
        ListNode4 pre=dummyHead;
        while(pre!=null){
            ListNode4 p=pre.next;
            int sum=0;
            while(p!=null){
                sum+=p.val;
                if(sum==0){
                    pre.next=p.next;
                    break;
                }else{
                    p=p.next;
                }
            }
           if(p==null) pre=pre.next;
        }
        return dummyHead.next;
    }
    public ListNode4 removeZeroSumSublists(ListNode4 head) {
        if (head == null)
            return head;
        int sum=0;
        for(ListNode4 p=head;p!=null ;p=p.next){
            sum+=p.val;
            if(sum==0){
                return removeZeroSumSublists(p.next);
            }
        }
        head.next=removeZeroSumSublists(head.next);
        return head;
    }
}
class ListNode4{
    int val;
    ListNode4 next;

    public ListNode4(int val) {
        this.val = val;
    }
}
