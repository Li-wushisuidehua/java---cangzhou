package Day16;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

public class Demo1 {

}
//使用队列实现栈
class MyStack {
    public Queue<Integer> queue1;
   public Queue<Integer> queue2;

    public MyStack() {//没有返回值的方法为构造方法
        queue1=new ArrayDeque<>();
        queue2=new ArrayDeque<>();
    }

    public void push(int x) {
        //当两个队列均为空时
        if(empty()){
            queue1.add(x);
            return ;
        }
        //其中一个为空
        if(queue2!=null){
            queue2.add(x);
        }else {
            queue1.add(x);
        }
    }

    public int pop() {
        if(empty()){
            return -1;
        }
       if(!queue1.isEmpty()){
           int size=queue1.size();
           int ret=-1;
           for (int i = 0; i < size-1; i++) {
               queue2.add(queue1.poll());
           }
           return queue1.poll();
       }else{
           int size=queue2.size();
           int ret=-1;
           for (int i = 0; i < size-1; i++) {
               queue1.add(queue2.poll());
           }
           return queue2.poll();
       }
    }

    public int top() {
        if(empty()) return -1;
        if(!queue1.isEmpty()){
           int size=queue1.size();
           int ret=-1;
            for (int i = 0; i < size; i++) {
                ret=queue1.poll();
                queue2.add(ret);
            }
            return ret;
        }else{
            int size=queue2.size();
            int ret=-1;
            for (int i = 0; i < size; i++) {
                ret=queue2.poll();
                queue1.add(ret);
            }
            return ret;
        }
    }

    public boolean empty() {
      return queue2.isEmpty() && queue1.isEmpty();
    }
}
