package Day15;

import java.util.Stack;

public class Demo2 {
    public static void main(String[] args) {
        String s="";
        System.out.println(isValid(s));

    }
    //类似于括号匹配问题使用栈来解决，此解题时间复杂度为2ms，下面会写一种时间复杂度为0的解决
    public static boolean isValid(String s) {
        Stack<Character> stack=new Stack<>();
        int len=s.length();
        for (int i = 0; i < len; i++) {
             char ch=s.charAt(i);
            if(ch=='(' ||ch=='{' ||ch=='[') stack.push(ch);
            else if(ch==')'){
                if(!stack.empty() && stack.peek()=='(') stack.pop();
                else stack.push(ch);
            } else if (ch=='}') {
                if(!stack.empty() && stack.peek()=='{') stack.pop();
                else stack.push(ch);
            } else if (ch==']') {
                if(!stack.empty() && stack.peek()=='[') stack.pop();
                else stack.push(ch);
            }
        }
        if(stack.empty()){
            return true;
        }else return false;

    }
}
