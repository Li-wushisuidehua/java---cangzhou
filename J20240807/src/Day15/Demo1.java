package Day15;

import java.util.Stack;

public class Demo1 {

}
class MinStack {
    Stack<Integer> stack;
    Stack<Integer> Minstack;


    public MinStack() {
        stack=new Stack<>();
        Minstack=new Stack<>();
    }

    public void push(int val) {
        if(stack.empty() && Minstack.empty()){
            stack.push(val);
            Minstack.push(val);
            return ;
        }
        if(Minstack.peek()>=val){
            Minstack.push(val);
        }
        stack.push(val);
    }

    public void pop() {
        if(stack.empty()) return;
        if(stack.pop().equals(Minstack.peek())){
            Minstack.pop();
        }
    }

    public int top() {
        if(stack.empty()) return -1;
        return stack.pop();

    }

    public int getMin() {
        if(Minstack.empty()){
            return -1;
        }
        return Minstack.pop();
    }
}
