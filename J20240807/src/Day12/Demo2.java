package Day12;

public class Demo2 {
    public static void main(String[] args) {
        ListNode head=new ListNode(1);
        ListNode node1=new ListNode(2);
        ListNode node2=new ListNode(2);
        ListNode node3=new ListNode(3);
        ListNode node4=new ListNode(3);
        ListNode node5=new ListNode(4);
        ListNode node6=new ListNode(5);
        ListNode node7=new ListNode(5);

        head.next=node1;
        node1.next=node2;
        node2.next=node3;
        node3.next=node4;
        node4.next=node5;
        node5.next=node6;
        node6.next=node7;
        ListNode node=deleteDuplication(head);
        while(node!=null){
            System.out.println(node.val);
            node=node.next;
        }

    }
    public static ListNode deleteDuplication(ListNode pHead) {
        if(pHead==null || pHead.next==null) return pHead;

        //新创建的节点
       ListNode ret=new ListNode(0);
       ListNode parent=ret;

       //赋值
        ListNode cur=pHead;
        ListNode curN=cur.next;

        //
        while(curN !=null){
            if(cur.val!= curN.val) {
                parent.next=cur;
                cur.next=null;
                parent=parent.next;
                if(curN.next==null) {
                    parent.next=curN;
                }
                cur=curN;
                curN=curN.next;

            }else {
                while(curN!=null && cur.val== curN.val){
                    curN=curN.next;
                }
                cur=curN;
                if(curN !=null && curN.next==null){
                    parent.next=cur;
                }
                if(curN!=null)
                curN=curN.next;
            }


        }
        return ret.next;
    }
}
class ListNode{
    public int val;
    public ListNode next;

    public ListNode(int val) {
        this.val = val;
    }
}

