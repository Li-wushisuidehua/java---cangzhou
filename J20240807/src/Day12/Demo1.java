package Day12;
public class Demo1 {
    public static void main(String[] args) {

    }
    public static ListNode1 mergeTwoLists(ListNode1 list1, ListNode1 list2) {
        if(list1==null && list2!=null) return list2;
        if(list1!=null && list2==null) return list1;
        ListNode1 node1=list1;
        ListNode1 node2=list2;
        ListNode1 retN=new ListNode1(0);
        ListNode1 ret=retN;
        while(node1!=null && node2!=null){
            if(node1.val>=node2.val){
                ret.next=node2;
                node2=node2.next;
            }else{
                ret.next=node1;
                node1=node1.next;
            }
            ret=ret.next;//此时节点指向下一个节点并没有指向null

        }
        if(node1!=null){
            ret.next=node1;
        }
        if(node2!=null){
            ret.next=node2;
        }
        return retN.next;

    }
}
class ListNode1{
    public int val;
    public ListNode1 next;

    public ListNode1(int val) {
        this.val = val;
    }
}

