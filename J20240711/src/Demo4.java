import java.util.Arrays;
//返回前k个单词
public class Demo4 {
    //方法一
    public static void main(String[] args) {
        String str="hello world hello bit";
        String[] strings=str.split(" ");
        //把数组转化为字符串
        //System.out.println(Arrays.toString(strings));
      //使用stringBuild将一个一个的字符串进行拼接
        StringBuilder sb=new StringBuilder();
        for (String s:strings) {
            sb.append(s);
            sb.append(' ');
        }
        //删除前后尾巴上的空格
        String s1=sb.toString();
        System.out.println(s1.trim());

    }
}
//方法二
class Test2{
    public static void main(String[] args) {
        String s=" ";
        System.out.println(truncateSentence(s,1)+"hh");

    }
    public static String truncateSentence(String s,int k){
        int n=0;
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)==' '){
                n++;
            }
            if(n==k){
                return s.substring(0,i);
            }
        }
        return s;
    }
}

