class A {
    public A() {
        System.out.println("A");
    }
    static {
        System.out.println("A static");
    }
}

class B extends A {
    static int a=10;
    public B() {
        System.out.println("B");
    }
    static {
        System.out.println("B static");
    }
}

public class Test {

    static B b = new B();

    public static void main(String[] args) {
        new A();
        new B();
    }
}
