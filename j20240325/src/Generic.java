//泛型类
public class Generic <T extends Comparable<T>>{//泛型的触头，此时是一个泛型类
    public T findmaxElement(T[] integers){
        T max=integers[0];//T就代表是Interger，Interger是一个类，类的变量是一个引用类型；
        for (int i = 0; i < integers.length; i++) {
            if(integers[i].compareTo(max)>0){//引用类型比较大小通过调用compareTo方法；
                /**
                 * 上一行代码中，由于双方都是引用类型，引用类型的大小判断是需要借助于compareTo函数去实现
                 * 由于需要进行判断的integers这个对象所指的元素，而integers的类Integer本身实现了comparable接口，所以
                 */
                max=integers[i];
            }

        }
        return max;
    }

    /**
     * Java中的数据类型项分有基本数据类型和引用数据类型，其中基本数据类型存储在栈中，而引用类型的数据则存储在堆上，在栈上会存有数据的地址
     * 方便找到；
     * @param args
     */
    public static void main(String[] args) {
        Generic<Integer> list=new Generic<Integer>();//初始化泛型类
//        int []m=new int[]{1,2,3,4};
        Integer [] integers={1,2,3,4,5};//装箱处理,装箱就是把基本数据类型存储在堆中，所以要把它变成引用类型
       Integer max=list.findmaxElement(integers);
        System.out.println(max);

    }
}
