import javax.imageio.stream.ImageInputStream;

//泛型方法，顾名思义，方法是泛型的；
//public class Generic1 {
//    public static <T extends Comparable<T>> T findmaxElement(T[] integers) {
//        T max = integers[0];//T就代表是Interger，Interger是一个类，类的变量是一个引用类型；
//        for (int i = 0; i < integers.length; i++) {
//            if (integers[i].compareTo(max) > 0) {//引用类型比较大小通过调用compareTo方法；
//                /**
//                 * 上一行代码中，由于双方都是引用类型，引用类型的大小判断是需要借助于compareTo函数去实现
//                 * 由于需要进行判断的integers这个对象所指的元素，而integers的类Integer本身实现了comparable接口，所以
//                 */
//                max = integers[i];
//            }
//
//        }
//        return max;
//
//    }

//    public static void main(String[] args) {
//
//        Integer []integer={1,2,3,4,5,6,7,8};
//     Integer X=  Generic1.<Integer>findmaxElement(integer);
//
//
//
//    }
public class Generic1 {
    public <T extends Comparable<T>> T findMax(T[] integers) {
        T max = integers[0];//T就代表是Interger，Interger是一个类，类的变量是一个引用类型；
        for (int i = 0; i < integers.length; i++) {
            if (integers[i].compareTo(max) > 0) {//引用类型比较大小通过调用compareTo方法；
                /**
                 * 上一行代码中，由于双方都是引用类型，引用类型的大小判断是需要借助于compareTo函数去实现
                 * 由于需要进行判断的integers这个对象所指的元素，而integers的类Integer本身实现了comparable接口，所以
                 */
                max = integers[i];
            }

        }
        return max;

    }
    public static void main2(String[] args) {
        Generic1 generic=new Generic1();
        Integer []array={1,2,3,4,5,6,7,8};

     Integer x = generic.<Integer>findMax(array);
        //Integer x =generic.<Integer>findMax(array);//博哥
    }
}
