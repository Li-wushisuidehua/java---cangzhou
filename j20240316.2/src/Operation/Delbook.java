package Operation;

import Dmeo2.Book;
import Dmeo2.BookList;
import Operation.Functions;

import java.util.Scanner;

public class Delbook implements Functions {
    @Override
    public void work(BookList bookList) {
        System.out.println("正在删除图书……");
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入你要删除的书名：");
        String bookname=scanner.nextLine();
        int currentsize=bookList.getUesdsize();
        int pos=-1;
        int i = 0;
        for (; i < currentsize; i++) {
            Book book=bookList.getBooks(i);
            if(book.getBookname().equals(bookname)) {
                pos=i;
                break;
            }

        }
        if(i>=currentsize){
            System.out.println("没有找到这本书");
            return;
        }
            //开始删除
            for (int j = pos; j < currentsize-1; j++) {

                Book book=bookList.getBooks(j+1);
                bookList.setBooks(j,book);
            }
            bookList.setBooks(currentsize-1,null);
            bookList.setUesdsize(currentsize-1);
        System.out.println("删除图书成功！！");
    }
}
