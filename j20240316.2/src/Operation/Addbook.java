package Operation;

import Dmeo2.Book;
import Dmeo2.BookList;
import Operation.Functions;

import java.util.Scanner;

public class Addbook implements Functions {
    @Override
    public void work(BookList bookList) {
        System.out.println("正在新增图书……");
        if(bookList.isfull()){
            System.out.println("书满了，不能在新增书籍了！");
            return;
        }
        System.out.println("请输入您想要输入的书名：");
        Scanner scanner=new Scanner(System.in);
        String bookname=scanner.nextLine();

        System.out.println("请输入书名的作者：");
        String author=scanner.nextLine();

        System.out.println("请输入书的价格：");
        int price=scanner.nextInt();

        String type1=scanner.nextLine();
        System.out.println("请输入书的类型：");
        String type=scanner.nextLine();

        Book book=new Book(bookname,author,price,type);
        int currentsize=bookList.getUesdsize();
        bookList.setBooks(currentsize,book);
        bookList.setUesdsize(currentsize+1);
        System.out.println("新增图书成功！！");
    }
}

