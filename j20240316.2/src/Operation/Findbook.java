package Operation;

import Dmeo2.Book;
import Dmeo2.BookList;
import Operation.Functions;

import java.util.Scanner;

public class Findbook implements Functions {
    @Override
    public void work(BookList bookList) {
        System.out.println("正在查找图书……");
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入你要查找的书名：");
        String bookname=scanner.nextLine();//需要查找的书名
        int currentsize=bookList.getUesdsize();//目前书架的长度
        for (int i = 0; i < currentsize; i++) {
            Book book=bookList.getBooks(i);//将书架上的书找出来，一本一本的对照比
            if(book.getBookname().equals(bookname)) {
                System.out.println("找到了这本书！");
                System.out.println(book);
                return;
            }

        }
        System.out.println("没有找得到！");

    }
}
