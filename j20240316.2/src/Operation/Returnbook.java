package Operation;

import Dmeo2.Book;
import Dmeo2.BookList;
import Operation.Functions;

import java.util.Scanner;

public class Returnbook implements Functions {
    @Override
    public void work(BookList bookList) {
        System.out.println("正在归还图书……");
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入您要借阅的图书：");
        String bookname=scanner.nextLine();
        int currentsize=bookList.getUesdsize();
        for (int i = 0; i < currentsize; i++) {
            Book book=bookList.getBooks(i);
            if(book.getBookname().equals(bookname)) {
                book.setLend(false);
                System.out.println("成功被归还！");
                return;
            }

        }
        System.out.println("归还未成功！");
    }
}
