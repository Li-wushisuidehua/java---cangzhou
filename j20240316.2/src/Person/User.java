package Person;

import Dmeo2.BookList;
import Operation.Findbook;
import Operation.Functions;

public abstract class User {//抽象类
    public String name;
    public User(String name) {
        this.name = name;
    }
    protected Functions []functions;
    //BookList bookList=new BookList();
    public void dofunctions(int choice,BookList bookList){
        this.functions[choice].work(bookList);
    }
    public abstract int menu();
}
