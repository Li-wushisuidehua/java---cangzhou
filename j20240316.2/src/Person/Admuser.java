package Person;

import Operation.*;

import java.util.Scanner;

public class Admuser extends User {
    public Admuser(String name) {
        super(name);
        functions=new Functions[]{
                new EixtSystem(),
                new Findbook(),
                new Addbook(),
                new Delbook(),
                new Showbook()
        };
    }


    @Override
    public int menu() {
        System.out.println("****管理员菜单****");
        System.out.println("1、查找图书");
        System.out.println("2、新增图书");
        System.out.println("3、删除图书");
        System.out.println("4、显示图书");
        System.out.println("0、退出系统");
        System.out.println("****************");
        System.out.println("请输入您要进行的操作：");
        Scanner scanner=new Scanner(System.in);
        int choice=scanner.nextInt();
        return choice;
    }
}
