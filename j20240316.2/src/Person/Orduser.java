package Person;

import Operation.*;

import java.util.Scanner;

public class Orduser extends User {
    public Orduser(String name) {
        super(name);
        functions=new Functions[]{
                new EixtSystem(),
                new Showbook(),
                new Findbook(),
                new Brrowbook(),
                new Returnbook()
        };
    }
    @Override
    public int menu() {
        System.out.println("****普通用户菜单****");
        System.out.println("1、显示图书");
        System.out.println("2、查找图书");
        System.out.println("3、借阅图书");
        System.out.println("4、归还图书");
        System.out.println("0、退出系统");
        System.out.println("****************");
        System.out.println("请输入您要进行的操作：");
        Scanner scanner=new Scanner(System.in);
        int choice=scanner.nextInt();
        return choice;
    }

}
