package Dmeo2;
//一个书架的模型;
public class BookList {
   private Book[] books =new Book[10] ;//最大限度是10本书;
    private int uesdsize;
    public BookList(){
        books[0]=new Book("三国演义","罗贯中",10,"名著");
        books[1]=new Book("红楼梦","曹雪芹",15,"名著");
        books[2]=new Book("西游记","吴承恩",18,"名著");
        this.uesdsize=3;//目前书架上已经有了三本书;
    }
    public boolean isfull(){
        if(this.uesdsize==books.length){
            return true;
        }
        return false;
    }
    public Book getBooks(int pos) {
        return books[pos];//代表的是一本书;
    }

    public void setBooks(int pos,Book book) {//新增图书
        this.books[pos]=book;
    }

    public int getUesdsize() {
        return uesdsize;
    }

    public void setUesdsize(int uesdsize) {
        this.uesdsize = uesdsize;
    }
}
