package Dmeo2;
//一本书的模型;
public class Book {
    private String bookname;//书名
    private String  bookauthor;//书的作者
    private int price;//书的价格
    private String type;//书的类型
    private boolean isLend;//书是否被借阅

    public Book(String bookname, String bookauthor, int price, String type) {
        this.bookname = bookname;
        this.bookauthor = bookauthor;
        this.price = price;
        this.type = type;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getBookauthor() {
        return bookauthor;
    }

    public void setBookauthor(String  bookauthor) {
        this.bookauthor = bookauthor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isLend() {
        return isLend;
    }

    public void setLend(boolean lend) {
        isLend = lend;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookname='" + bookname + '\'' +
                ", bookauthor=" + bookauthor +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", isLend='" + (isLend==true?"已借出":"未借出" )+
                '}';
    }
}
