import Dmeo2.BookList;
import Person.Admuser;
import Person.Orduser;
import Person.User;

import java.util.Scanner;

public class Main {
    public static User login(){
        System.out.println("请输入您的姓名：");
        Scanner scanner=new Scanner(System.in);
        String name=scanner.nextLine();
        System.out.println("请选择您的身份： 1、管理员  2、普通用户" );
        int choice=scanner.nextInt();
        if(choice==1){
            Admuser admuser =new Admuser(name);//选择什么后，便实例化好什么对象了
            return admuser;
        }
        else {
            Orduser orduser=new Orduser(name);
            return orduser;
        }
    }
    public static void main(String[] args) {
        User user=login();
        BookList bookList = new BookList();
        while(true) {
            int choice = user.menu();
            user.dofunctions(choice, bookList);
        }
    }
}
