package Demo1;

import java.util.Arrays;

/**
 * 堆是由顺序表建立的
 */
public class MyHeap {
    public int[] elem;
    public int usedSize;

    public MyHeap() {
        elem = new int[10];
    }

    public void CreateElem(int[] array) {
        for (int i = 0; i < array.length; i++) {
            elem[i] = array[i];
            usedSize++;
        }
    }

    //创建大根堆
    public void CreateHeap() {
        for (int i = (usedSize - 1) / 2; i >= 0; i--) {
            shiftDown(i, usedSize);
        }
    }

    public void shiftDown(int parent, int maxSize) {
        int child = 2 * parent + 1;
        while (child < maxSize) {
            if (child+1 < maxSize && elem[child] < elem[child + 1]) {
                child++;
            }
            if (elem[child] > elem[parent]) {
                swap( child, parent);
                parent=child;
                child=2*parent+1;
            }else{
                break;//如果child下标所对应的元素小于parent所对应的下标的元素时，此时便没有必要再往下走
            }

        }
    }

    public void swap( int i, int j) {
        int tmp = elem[i];
        elem[i] = elem[j];
        elem[j] = tmp;
    }
    //有大根堆创建小根堆
    public void CreateElemReallySmall(int parent, int maxSize) {
        int child = 2 * parent + 1;
        while (child < maxSize) {
            if (child+1 < maxSize && elem[child] > elem[child + 1]) {
                child++;
            }
            if (elem[child] < elem[parent]) {
                swap(child, parent);
                parent=child;
                child=2*parent+1;
            }else{
                break;//如果child下标所对应的元素小于parent所对应的下标的元素时，此时便没有必要再往下走
            }

        }
    }
    public boolean isFull(){

        return usedSize==elem.length;
    }
    //插入的时候需要判断堆里面的元素是否已经满了
    public void offer(int key){
        if(isFull()){
            elem= Arrays.copyOf(elem,elem.length*2);//满了之后需要进行扩容
        }
        elem[usedSize++]=key;
        shiftup(usedSize);


    }
    //向上调整
    public void shiftup(int usedSize){
        int child=usedSize-1;
        int parent=(child-1)>>>1;
        while(child>0){
            if(elem[child]<elem[parent]){
                swap(child,parent);
                child=parent;
                parent=(child-1)>>>1;
            }else{
                break;
            }
        }
    }
    //判断堆中是否为空
   public boolean isEmpty(){
        return usedSize==0;

    }
    //由上知通过插入来创建堆
    public void InsertHeap(int key){
        if(isEmpty()){
            elem[usedSize++]=key;
        }else{
            elem[usedSize++]=key;
//            int child=usedSize-1;
//            int parent=(child-1)>>>1;
//            while(child>0){
//                if(elem[child]<elem[parent]){
//                    swap(child,parent);
//                    child=parent;
//                    parent=(child-1)>>>1;
//                }else{
//                    break;
//                }
//            }
            shiftup(usedSize);
        }
    }

    //堆的删除，堆的删除一定是删除的堆顶的元素
    public void poll(){
        if(isEmpty()){
            System.out.println("堆中元素为空！");
            return;
        }
        swap(0,usedSize-1);//将最后一个元素和堆顶的元素进行交换
        usedSize--;
        shiftDown(0,usedSize);
    }
    //查看堆顶的元素
    public int peek(){
        return elem[0];//peek查看的永远是堆顶的元素
    }
    //清空堆
    public void clearHeap(){
        for (int i = 0; i < usedSize; i++) {
            elem[i]=0;
        }
    }
    //堆排序
    public void HeapSort(){
        int endIndex=usedSize-1;
        while(endIndex>0){
            swap(0,endIndex);
            shiftDown(0,endIndex);
            endIndex--;
        }
    }



}
