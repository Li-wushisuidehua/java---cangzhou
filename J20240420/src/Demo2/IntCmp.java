package Demo2;

import java.util.Comparator;

public class IntCmp implements Comparator<Integer> {
    //Comparator中泛型为什么类型，compare中比较的类型便为什么类型
    @Override
    public int compare(Integer o1, Integer o2) {
        return o2-o1;
    }
}
