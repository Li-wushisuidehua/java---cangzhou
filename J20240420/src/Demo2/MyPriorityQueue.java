package Demo2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

public class MyPriorityQueue {
    public static void main(String[] args) {
//        PriorityQueue<Integer> priorityQueue=new PriorityQueue<>();
//        priorityQueue.offer(12);
//        priorityQueue.offer(17);
//        priorityQueue.offer(40);
//        priorityQueue.offer(35);
//        priorityQueue.offer(34);
//        priorityQueue.offer(21);
//        priorityQueue.offer(90);
//        //  priorityQueue.offer(null);//不能插入null在PriorityQueue下会报错
//        ArrayList<Integer> list=new ArrayList<>();
//        list.add(2);
//        list.add(1);
//        list.add(3);
//        PriorityQueue<Integer> priorityQueue1=new PriorityQueue<>(list);
//        System.out.println(priorityQueue1.size());//应该输出为3
//        System.out.println(priorityQueue1.poll());//应该输出为1，会自动将传过来的集合变为小根堆
        //默认情况下，PriorityQueue队列是小根堆，如果想要创建大根堆需要用户提供比较器
        IntCmp intCmp = new IntCmp();
        PriorityQueue<Integer> priorityQueue2 = new PriorityQueue<>(intCmp);//构造方法为其提供了一个比较器，比较器便规划了创建出来的为大根堆还是小根堆
//        priorityQueue2.offer(12);
//        priorityQueue2.offer(17);
//        priorityQueue2.offer(40);
//        priorityQueue2.offer(35);
//        priorityQueue2.offer(34);
//        priorityQueue2.offer(21);
//        priorityQueue2.offer(90);
//        System.out.println(priorityQueue2.peek());//输出的为90，说明大根堆建立成功
        int[] array = {12, 17, 40, 35, 34};
        int[] ret = smallestK(array, 3);
        System.out.println(Arrays.toString(ret));
    }

    //Top-k问题
    public static int[] smallestK(int[] arr, int k) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        int[] ret = new int[k];
        for (int i = 0; i < arr.length; i++) {
            priorityQueue.offer(arr[i]);
        }
        for (int i = 0; i < k; i++) {
            ret[i] = priorityQueue.poll();
        }
        return ret;
    }


    /**
     * 前K个最小值建立大根堆
     * 前k个最大值建立小根堆
     *
     * @param arr
     * @param k
     * @return
     */
    //Top-问题的优化
    public static int[] smallestK1(int[] arr, int k) {
        IntCmp intCmp=new IntCmp();
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(intCmp);//自身为其提供了一个比较器
        int []ret=new int[k];
        for (int i = 0; i < k; i++) {
            priorityQueue.offer(arr[i]);
        }
        for(int i=k;i<arr.length;i++){
            int tmp=priorityQueue.peek();
            if(tmp>arr[i]){
                priorityQueue.poll();
                priorityQueue.offer(arr[i]);
            }
        }
        for (int i = 0; i < k; i++) {
            ret[i]=priorityQueue.poll();

        }
        return ret;

    }


}
