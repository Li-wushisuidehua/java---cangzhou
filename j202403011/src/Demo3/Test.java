package Demo3;

import java.util.Arrays;
import java.util.Comparator;
//比较器
class Student implements Comparator<Student> {
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

//    @Override
//    public int compareTo(Student o) {
//        return this.age-o.age;//
//    }


    @Override
    public int compare(Student o1, Student o2) {
        return o1.age-o2.age;
    }
}
public class Test {
    public static void main(String[] args) {
        Namecomparator namecomparator=new Namecomparator();
        Student [] student=new Student[3];
       student[0]=new Student("zuahua",20);
       student[1]=new Student("li",15);
        student[2]=new Student("wangwu",18);
        Arrays.sort(student,namecomparator);//自定义类型
        System.out.println(Arrays.toString(student));
    }

    public static void main2(String[] args) {
        Student student=new Student("zuahua",20);
        Student student1=new Student("li",15);
        Agecomparator agecomparator=new Agecomparator();
        agecomparator.compare(student,student1);
        System.out.println(agecomparator.compare(student, student1));
    }
    public static void main1(String[] args) {
     Student student=new Student("zuahua",20);
        Student student1=new Student("li",15);
        Namecomparator namecomparator=new Namecomparator();
        namecomparator.compare(student,student1);
        System.out.println(namecomparator.compare(student, student1));

    }

}
