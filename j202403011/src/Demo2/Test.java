package Demo2;

import java.util.Arrays;

class Student implements Comparable<Student>{
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

//    @Override
//    public int compareTo(Student o) {
//        return this.age-o.age;//
//    }

    @Override
    public int compareTo(Student o) {
        return this.name.compareTo(o.name);
    }
}
public class Test {
    //模拟实现 sort方法排序
    public static void mysort(Comparable [] comparable){//这儿传的就是地址
        for (int i = 0; i < comparable.length-1; i++) {
            for (int j = 0; j < comparable.length-1-i; j++) {

                if(comparable[j].compareTo(comparable[j+1])>0){
                    Comparable tmp=comparable[j];
                    comparable[j]=comparable[j+1];
                    comparable[j+1]=tmp;
                }
            }
        }
    }
    public static void main(String[] args) {

        Student []students=new Student[3];
        students[0]=new Student("zuahua",20);
        students[1]=new Student("li",15);
        students[2]=new Student("wangwu",35);
       // Arrays.sort(students);
        mysort(students);
        System.out.println(Arrays.toString(students));
    }


}
