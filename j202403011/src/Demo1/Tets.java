package Demo1;

import Demo3.Agecomparator;
import Demo3.Namecomparator;

import java.util.Comparator;

//代码实现比较大小;
class Student implements Comparator<Student> {
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

//    @Override
//    public int compareTo(Student o) {
//        return this.age-o.age;//
//    }

//    @Override
//    public int compareTo(Student o) {//comparaTo是包含在comparable中的。
//        return this.name.compareTo(o.name);
//    }

    @Override
    public int compare(Student o1, Student o2) {
        return 0;
    }
}
public class Tets {
    public static void main(String[] args) {
        Student student2=new Student("huahua",20);
        Student student3=new Student("meli",15);
        Namecomparator namecomparator=new Namecomparator();
        Agecomparator agecomparator=new Agecomparator();

    }
}
