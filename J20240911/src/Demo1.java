import java.util.HashMap;
import java.util.Map;

public class Demo1 {
    public static void main(String[] args) {
        int[] arr=new int[]{0,1,2};
        System.out.println(totalFruit(arr));

    }
    //方法二使用Map来做题
    public static int totalFruit(int[] f) {
        int ret=0;
        Map<Integer,Integer> hashMap=new HashMap<>();
        for (int left=0,right=0; right <f.length ; right++) {
            int in=f[right];//拿到即将要存进去的元素
            hashMap.put(in,hashMap.getOrDefault(in,0)+1);//给元素加 1
         //   hashMap.put(in,hashMap.get(in)+1);//注意这里可能in对应的value没有值，直接get获取就会抛出异常
            while(hashMap.size()>2){
                int out=f[left];
              //  hashMap.put(out,hashMap.getOrDefault(out,1)-1);简化版如下
                hashMap.put(out,hashMap.get(out)-1);//这里可以确保一定是有值的，所以说可以放心的get元素
                if(hashMap.get(out)==0) hashMap.remove(out);
                left++;
            }
            ret=Math.max(ret,right-left+1);
        }
        return ret;
    }
    public static int totalFruit1(int[] fruits) {
        int left=0,right=0,ret=0,len=fruits.length,kinds=0;
        int []hash=new int[len+1];
        while(right<len){
            if(hash[fruits[right]]==0) kinds++;
            hash[fruits[right]]++;
            while(kinds>=3){
                int out=--hash[fruits[left]];
                if(out==0) kinds--;
                left++;
            }

            ret=Math.max(ret,right-left+1);
            right++;
        }
        return ret;
    }
}
