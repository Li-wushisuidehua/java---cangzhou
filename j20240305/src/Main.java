////父类
//class person{
//    public String name;
//    public int age=5;
//    public String color;
//    public void eat(){
//        System.out.println(this.name+"正在吃饭！");
//    }
//    static{
//       // System.out.println("父类的静态代码块！");
//    }
//    {
//     //   System.out.println("父类的实例化代码块！");
//    }
//
//    public person(int age) {//父类的构造方法
//        this.age=age;
//       // System.out.println("父类的构造方法");
//    }
//}
////子类
//class Li extends person{
//    public int age;
//    static{
//        System.out.println("子类的静态代码块！");
//    }
//    {
//        System.out.println("子类的实例化代码块！");
//    }
//
//    public Li(int age) {//子类的构造方法
//        super(age);
//        this.age = age;
//        System.out.println("子类的构造方法");
//    }
//
//
//    public void play(){
//        System.out.println(this.name+"正在玩耍！");
//    }
//    public void show(){
//        System.out.println(this.name+"今年"+super.age+"岁了!");
//    }
//    public void eat(){
//        super.eat();
//        System.out.println(name+"正在吃东西！");
//    }
//}
////创建对象
//public class Tets {
//    public static void main(String[] args) {
//
//        Li li=new Li(5);
//        //li.name="limingyue";
//       // li.age=18;
//       // li.play();
//     //   li.show();
//        System.out.println("===========");
//        Li li1=new Li(10);
//        li1.show();
//    }
//
//
//}
//class Base {
//    Base() {
//        System.out.print("Base");
//    }
//}
//
//public class Alpha extends Base {
//
//    public static void main( String[] args ) {
//        new Alpha();//1
//        //调用父类无参的构造方法
//        new Base();//2
//    }
//}
//class X{
//    Y y=new Y();//1
//    public X(){//2
//        System.out.print("X");
//    }
//}
//class Y{
//    public Y(){//3
//        System.out.print("Y");
//    }
//}
//public class Z extends X{
//    Y y=new Y();//4
//    public Z(){//5
//        System.out.print("Z");
//    }
//    public static void main(String[] args) {
//        new Z();
//    }
//}
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            int z = scanner.nextInt();
            Sub sub = new Sub(x, y, z);
            System.out.println(sub.calculate());
        }
    }

}

class Base {

    private int x;
    private int y;

    public Base(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}

class Sub extends Base {

    private int z;

    public Sub(int x, int y, int z) {
        //write your code here
        super(x,y);
        this.z=z;
    }

    public int getZ() {
        return z;
    }

    public int calculate() {
        return super.getX() * super.getY() * this.getZ();
    }

}