import java.util.Objects;

public class Person {
    /**
     * Object类中各种方法的演示
     */
    public String name;
    public int age;
    public void eat(){
        System.out.println(this.name+"正在吃午饭");
    }

    /**
     *hashCode和equals一般不需要自己写，用编译器自己生成的便欧克；
     * @param
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {//重写后表示现在根据name和age来判断hashCode的值
        return Objects.hash(name, age);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {
        Person person=new Person();
        person.name="meili";
        person.age=18;
        Person person1=new Person();
        person1.name="meili";
        person1.age=18;
        System.out.println("===判断两个字符串是否相等===");
       boolean result=person.equals(person1);
        System.out.println(result);
        System.out.println("===hashCode===");
        System.out.println(person.hashCode());
        System.out.println(person1.hashCode());

    }

}
