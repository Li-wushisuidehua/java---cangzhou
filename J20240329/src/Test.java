import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * isEmpty()方法是用于判断一个字符串是否为空的方法
 */
public class Test {
    /**
     * 1.使用addAll方法会顺序表进行插入
     * 2.addAll构成了方法的重载，构成重载的方法中有一个方法，有两个参数列表，其中一个有插入的位置，一个没有插入的位置，没有插入的位置的时候表示插入的是在末尾；
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<Integer> arrayList1=new ArrayList<>();
        ArrayList<Integer> arrayList2=new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        arrayList2.add(6);
        arrayList1.add(7);
//       boolean result= arrayList1.addAll(5,arrayList2);
//        System.out.println(result);
//        System.out.println(arrayList1);
        System.out.println("==========");
        arrayList1.addAll(arrayList2);
        System.out.println(arrayList1);

    }
    //清空顺序表的元素，使用clear方法
    public static void main9(String[] args) {
        ArrayList<Integer> arrayList1=new ArrayList<>();
//        ArrayList<Integer> arrayList2=new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        arrayList1.add(6);
        arrayList1.add(7);
        System.out.println(arrayList1);
        arrayList1.clear();//此时已对元素进行了删除，对表中的所有元素已经进行了删除
        System.out.println(arrayList1);
    }
    /**
     * 使用remove删除顺序表中的元素
     * @param args
     */
    public static void main8(String[] args) {
        ArrayList<Integer> arrayList1=new ArrayList<>();
        ArrayList<Integer> arrayList2=new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        arrayList1.add(6);
        arrayList1.add(7);
        //System.out.println(arrayList1.isEmpty());
     //实现remove
        arrayList1.remove(5);//删除下标为5的元素
        System.out.println(arrayList1);
        arrayList1.remove(Integer.valueOf(3));//删除顺序表中值为3的元素
        System.out.println(arrayList1);

    }
    public static void main7(String[] args) {
        //第三种遍历方法，使用迭代器
        ArrayList<Integer> arrayList1=new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        arrayList1.add(6);
        arrayList1.add(7);
        Iterator<Integer> it=arrayList1.iterator();
        while(it.hasNext()){//迭代器中的下一个元素
            System.out.print(it.next()+" ");
        }
        System.out.println();
    }
    //第二种遍历方法
    public static void main6(String[] args) {
        ArrayList<Integer> arrayList1=new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        arrayList1.add(6);
        arrayList1.add(7);
        for (Integer x:arrayList1){
            System.out.print(x+" ");
        }
        System.out.println( );
    }
    /**
     * 三种遍历方法
     * @param args
     */
    //第一种使用循环进行拆箱
       public  static void main5(String[] args) {
            ArrayList<Integer> arrayList1=new ArrayList<>();
            arrayList1.add(1);
            arrayList1.add(2);
            arrayList1.add(3);
            arrayList1.add(4);
            arrayList1.add(5);
            arrayList1.add(6);
            arrayList1.add(7);

            for (int i = 0; i < arrayList1.size(); i++) {
                System.out.print(arrayList1.get(i)+" ");
            }
            System.out.println();


    }
    public static void main4(String[] args) {
        ArrayList<Integer> arrayList1=new ArrayList<>();//创建了一个对象
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        arrayList1.add(6);
        arrayList1.add(7);
        /**
         * Collection<? extends E> c
         * 其中c为变量，前面<>为c的类型
         */
        ArrayList<Integer> arrayList3=new ArrayList<>(arrayList1);//创建了一个对象
        System.out.println(arrayList3);

    }
    public static void main3(String[] args) {
        /**
         * ArrayList<E>
         */
        //第二种方法
        ArrayList<Integer> arrayList1=new ArrayList<>(5);
        arrayList1.add(3);//当3传过去时进行了装箱
        arrayList1.add(5);
        arrayList1.add(9);
        arrayList1.add(6);
        arrayList1.add(0);
        //输出时进行了拆箱
        for (Integer x:arrayList1){//数组为arrayList1，数组的每个类型为Integer
            System.out.print(x+" ");
        }
        System.out.println();

    }
    public static void main2(String[] args) {
        /**
         * 实例ArrayList有两种方法
         *    List<Integer> list=new ArrayList<>();
         *     ArrayList<Integer> arrayList=new ArrayList<>();
         */
        //第一种方法
        ArrayList<Integer> arrayList=new ArrayList<>();
        arrayList.add(2);
        arrayList.add(5);
        arrayList.add(8);
        arrayList.add(29);
        System.out.println(arrayList);//这里重写了toString方法
    }
}
