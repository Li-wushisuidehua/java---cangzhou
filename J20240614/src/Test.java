import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

public class Test {

    public static void main(String[] args) {
        // 创建一个JFrame实例
        JFrame frame = new JFrame("中缀逆波兰值表达式");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//表示程序在窗口关闭时退出
        frame.setSize(300, 200);//设置窗口的大小

        // 创建一个JPanel(容器)来容纳我们的组件
        JPanel panel = new JPanel();//最终这些组件会存放在GUI的界面中

        // 创建一个JLabel来提示用户输入
        JLabel inputLabel = new JLabel("请输入值:");

        // 创建一个JTextField让用户输入值
        final JTextField inputTextField = new JTextField(20);//这里的20设置的为文本框的长度,当这里的20被替换为字符串的时候，表名其文本框最初的值

        // 创建一个JTextField来显示用户输入的值（作为输出）
        final JTextField outputTextField = new JTextField(20);
        outputTextField.setEditable(false); // 设置输出文本框为不可编辑

        // 创建一个JButton来获取用户输入的值
        JButton button = new JButton("显示输出");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 获取输入文本框中的值
                String input = inputTextField.getText();//值是在这里被输入进去的
                //进行逆波兰值的转化
                String s=transfer(input);
                // 在输出文本框中显示输入的值
                outputTextField.setText(s);//只有点击显示输出之后才会有输出值真正的输出
            }
        });

        // 将组件添加到JPanel中,组件的顺序也会为最终出现在GUI的顺序
        panel.add(inputLabel);
        panel.add(inputTextField);
        panel.add(button);
        panel.add(outputTextField);

        // 将JPanel添加到JFrame中
        frame.getContentPane().add(panel);

        // 显示窗口
        frame.setVisible(true);
    }
    //中缀转后缀的表达式
    public static String transfer(String str){
        Stack<Character> stack=new Stack<>();
        StringBuilder stringBuilder=new StringBuilder();
        int i=0;
        while (i <= str.length()-1){
          char c=str.charAt(i);

            //如果为空格直接跳过
            if(Character.isWhitespace(c)){
                i++;
            }

            //如果为数字直接输出
            if(Character.isDigit(c)){
                StringBuilder sb=new StringBuilder();
                while(Character.isDigit(str.charAt(i))){
                    sb.append(str.charAt(i++));
                    if(i>str.length()-1){
                        break;
                    }
                }
                sb.append(" ");
                stringBuilder.append(sb);
                stringBuilder.append(" ");
            }

            //如果为左括号'('直接压栈
            else if (c=='(') {
                stack.push(c);
                i++;
            }

            //如果为右括号，则弹出栈中的元素
            else if ( c==')'){
                while(!stack.empty() && stack.peek()!='('){
                    stringBuilder.append(stack.pop());
                    stringBuilder.append(" ");
                }
                //将左括号也弹出
                stack.pop();
                i++;
            }

            //如果为运算符，弹出栈中优先级高于此运算符的运算符
            else if(Operate(c)>=0){
                while(!stack.empty() && Compare(stack.peek())>=Compare(c)){
                    stringBuilder.append(stack.pop());
                    stringBuilder.append(" ");
                }
                    stack.push(c);
                i++;
            }else{
                i++;
            }
        }
        //此时如果栈不为空依次进行拼接
        while(!stack.empty()){
            stringBuilder.append(stack.pop());
            stringBuilder.append(" ");
        }
        //将stringBuilder拼接的字符串转换成真的字符串
        return stringBuilder.toString();
    }
    //判断是否为运算符
    public static int Operate(char c){
        return "+-*/".indexOf(c);
    }
    public static int Compare(char c){
        if(c=='*' || c=='/'){
            return 2;
        }
        if(c=='+' || c=='-'){
            return 1;
        }else{
            return 0;
        }

    }
}
