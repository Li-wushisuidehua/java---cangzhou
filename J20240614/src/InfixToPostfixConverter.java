import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

public class InfixToPostfixConverter extends JFrame {
    private JTextField inputField;
    private JButton convertButton;
    private JTextArea outputArea;

    public InfixToPostfixConverter() {
        createUI();
    }

    private void createUI() {
        setTitle("中缀到逆波兰式转换器");
        setSize(400, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());

        inputField = new JTextField(20);
        convertButton = new JButton("转换");
        outputArea = new JTextArea(5, 30);
        outputArea.setEditable(false);

        convertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = inputField.getText();
                if (!input.isEmpty()) {
                    try {
                        String postfix = infixToPostfix(input);
                        outputArea.setText(postfix);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(InfixToPostfixConverter.this,
                                "发生错误: " + ex.getMessage(), "错误", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(InfixToPostfixConverter.this,
                            "请输入一个表达式", "警告", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        add(new JLabel("输入表达式:"));
        add(inputField);
        add(convertButton);
        add(new JScrollPane(outputArea));

        setVisible(true);
    }
    private static Stack<Character> symbolStack = new Stack<>();
    public static int priority(char a) {
        if (a == '(') {
            return 0;
        } else if (a == '+' || a == '-') {
            return 1;
        } else {
            return 2;
        }
    }

    public static boolean compare(char a, char b) {
        return priority(a) > priority(b);
    }

    public static boolean isSymbol(char cur) {
        return (cur == '+' || cur == '-' || cur == '*' || cur == '/' || cur == '(');
    }
    private String infixToPostfix(String expression) {
        StringBuffer res = new StringBuffer();
        int idx = 0;
        while (idx < expression.length()) {
            char cur = expression.charAt(idx);
            if (isSymbol(cur)) {
                if (symbolStack.isEmpty() || cur == '(') {
                    symbolStack.push(cur);
                    idx++;
                } else if (compare(cur, symbolStack.peek())) {
                    symbolStack.push(cur);
                    idx++;
                } else {
                    while (!symbolStack.isEmpty() && !compare(cur, symbolStack.peek())) {
                        append(res, symbolStack.pop());
                    }
                    symbolStack.push(cur);
                    idx++;
                }
            } else if (Character.isDigit(cur)) {
                StringBuffer ret = new StringBuffer();
                while (Character.isDigit(expression.charAt(idx))) {
                    ret.append(expression.charAt(idx++));
                    if (idx >= expression.length()) {
                        break;
                    }
                }
                res.append(ret);
                res.append(' ');
            } else {
                while (!symbolStack.isEmpty() && symbolStack.peek() != '(')
                {
                    append(res, symbolStack.pop());
                }
                if (!symbolStack.isEmpty()) {
                    symbolStack.pop();
                }
                idx++;
            }
        }
        while (!symbolStack.isEmpty()) {
            append(res, symbolStack.pop());
        }
        return res.toString();
    }
    public static void append(StringBuffer str, char ch) {
        str.append(ch);
        str.append(' ');
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new InfixToPostfixConverter();
            }
        });
    }
}

