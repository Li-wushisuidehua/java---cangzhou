public class Student implements Comparable{
    public String name;
    public int age;

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
