package Dmeo2;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class MystackuesQueue {
    Deque<Integer> s1;
    Deque<Integer> s2;


    public MystackuesQueue() {
        s1 = new LinkedList<>();
        s2 = new LinkedList<>();
    }
//入队列
    public void push(int x) {
        if(empty()){
            s1.offer(x);
        }
        if(s1.isEmpty()){
            s2.offer(x);
        }
       else{
            s1.offer(x);
        }
    }
//出队列
    public int pop() {
        /**
         * 如果两个队列都为空不能进行pop
         */
        if(empty()){
            return -1;
        }
        if(!s1.isEmpty()){
            int size=s1.size()-1;
            for (int i = 0; i <size; i++) {
                s2.offer(s1.poll());
            }
            return s1.poll();
        }else{
            int size=s2.size()-1;
            for (int i = 0; i < size; i++) {
                s1.offer(s2.poll());
            }
            return s2.poll();
        }
        /**
         * if-else语句中，return可以写在函数大括号的内部，不一定需要将，return写在外部。
         */
    }
//查看栈元素
    public int top() {
        if(empty()){
            return -1;
        }
        if(!s1.isEmpty()){
            int ret=-1;
            int size=s1.size();
            for (int i = 0; i <size; i++) {
                ret=s1.poll();
                s2.offer(ret);
            }
            return ret;
        }else{
            int ret=-1;
            int size=s2.size();
            for (int i = 0; i <size; i++) {
                ret=s2.poll();
                s1.offer(ret);
            }
            return ret;
        }

    }
//判断是否为空
    public boolean empty() {
return s1.isEmpty() && s2.isEmpty();
    }

}
