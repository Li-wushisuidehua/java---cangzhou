package Dmeo2;

import java.util.ArrayDeque;

//使用栈实现队列，栈与队列的方法是相反的，所以说使用一个栈肯定是无法解决问题的
public class MyQueueusestall {
   ArrayDeque<Integer> stack1 = new ArrayDeque<>();//元素入栈
    ArrayDeque<Integer> stack2 = new ArrayDeque<>();//元素出栈

    public void push(int x) {
        stack1.push(x);

    }

    public int pop() {
        if (empty()) {
            return -1;
        }
        /**
         * 这里不能写下面这一段，因为在第一次push之后，在要pop时，stack1中的元素都去了stack2中，如果连续pop两次就会出错。
         * 因为pop第二次时，stack1当中的元素已经为0了，但是pop是看stack2中的元素。
         */
//        if(stack1.isEmpty()){
//            return -1;
//        }
            if (stack2.isEmpty()) {
                while (!stack1.isEmpty()) {
                    stack2.push(stack1.pop());
                }
//            for (int i = 0; i < stack1.size(); i++) {
//                stack2.push(stack1.pop());
//            }
            }
            return stack2.pop();
        }

        public int peek () {
//        if (empty()) {
//            return -1;
//        }
//        if(stack1.isEmpty()){
//            return -1;
//        }
//        if(stack2.isEmpty()) {
//            for (int i = 0; i < stack1.size(); i++) {
//                stack2.push(stack1.pop());
//            }
//        }
//        return stack2.peek();
            if (empty()) {
                return -1;
            }
            if (stack2.isEmpty()) {
                while (!stack1.isEmpty()) {
                    stack2.push(stack1.pop());
                }
//            for (int i = 0; i < stack1.size(); i++) {
//                stack2.push(stack1.pop());
//            }
            }
            return stack2.peek();
        }

        public boolean empty () {
            return stack2.isEmpty() && stack1.isEmpty();
        }
    }

