import java.util.Stack;

class MinStack {
    Stack<Integer> stack;
    Stack<Integer> MinStack;

    public MinStack() {
        stack=new Stack<>();
        MinStack=new Stack<>();
    }

    public void push(int val) {
        stack.push(val);
        if(MinStack.empty()){
            MinStack.push(val);
        }else{
            if(MinStack.peek()>=val){
                MinStack.push(val);
            }
        }
    }

    public void pop() {
        if(stack.empty()){
            return ;
        }
        stack.pop();
        if(MinStack.peek().equals(stack.pop())){
            MinStack.pop();
        }

    }

    public int top() {
        if(stack.empty()) {
            return -1;
        }
       return stack.peek();
    }

    public int getMin() {
        if(MinStack.empty()) {
            return -1;
        }
        return MinStack.pop();
    }
}

