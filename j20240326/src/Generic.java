//泛型
public class Generic<T extends Comparable> {
    void mySort(T[] array) {
        for (int i = 0; i < array.length-1; i++) {
            for (int j = 0; j <array.length-1-i; j++) {
                if(array[j].compareTo(array[j+1])>0){
                    T tmp=array[j];
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        for (int j = 0; j < array.length; j++) {
            System.out.print(array[j]+" ");

        }
        System.out.println();
    }

    public static void main(String[] args) {
        Generic<Integer> generic = new Generic();
        Integer[] integers = {1, 9, 3, 4, 7, 2, 8, 4, 6};
        generic.mySort(integers);


    }
}
