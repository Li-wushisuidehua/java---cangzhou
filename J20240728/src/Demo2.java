public class Demo2 {
    public static int countCharacters2(String[] words, String chars) {
        int len = chars.length();
        int[] ch = new int[26];
        int tot = 0, sum = 0;
        for (int i = 0; i < words.length; i++) {
            tot = 0;
            int lenth = words[i].length();
            if(lenth>len) continue;
            for (int j = 0; j < lenth; j++) {
                ch[words[i].charAt(j)-'a']++;
                tot++;
            }
            for (int j = 0; j < chars.length(); j++) {
                if (ch[chars.charAt(j)-'a'] > 0) {
                    ch[chars.charAt(j)-'a']--;
                    tot--;
                }
            }
            if (tot == 0) {
                sum += words[i].length();
            }
        }
        return sum;
    }
}
