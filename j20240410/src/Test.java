import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Queue;

public class Test {
    //双端队列实现队列
    public static void main2(String[] args) {
        Deque<Integer> deque=new ArrayDeque<>();
        deque.offer(1);
        deque.offer(2);
        deque.offer(3);
        deque.offer(4);
        deque.offer(5);
        System.out.println(deque.poll());
        System.out.println(deque.poll());
        System.out.println(deque.poll());
        System.out.println(deque.poll());
        System.out.println(deque.poll());
        //输出的结果为1，2，3，4，5，正序输出了,说明使用双端队列可以实现队列，其实本身也就是可以的
    }
    //双端队列实现栈
    public static void main1(String[] args) {
        Deque<Integer> queue=new ArrayDeque<>(); //双端队列的线性实现
       queue.push(1);//压栈，在内存中存储的应该是1，2，3，4，5，6，7
       queue.push(2);
       queue.push(3);
       queue.push(4);
       queue.push(5);
       queue.push(6);
       queue.push(7);
       queue.add(8);
       queue.offer(9);//offer和add均是从队尾进入
        System.out.println("=======");
        System.out.println(queue.remove());
        queue.addLast(10);
        queue.addFirst(11);
        System.out.println("===========");
        System.out.println(queue.poll());
        System.out.println("=========");
        System.out.println(queue.pop());
        System.out.println(queue.pop());
        System.out.println(queue.pop());
        System.out.println(queue.pop());
        System.out.println(queue.pop());


    }

    public static void main(String[] args) {
        MyArraylist myArraylist=new MyArraylist();
        myArraylist.add(1);
        myArraylist.add(2);
        myArraylist.add(3);
        myArraylist.add(4);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);
        myArraylist.add(5);

       myArraylist.display();
       myArraylist.add(3,7);
//        myArraylist.display();
//   //     myArraylist.set(4,7);
//   //     myArraylist.display();
//        myArraylist.remove(5);
//        myArraylist.display();
//        myArraylist.clear();
//        myArraylist.display();
        myArraylist.remove(5);//删除第一个出现的五
        myArraylist.display();
        myArraylist.removeall(5);//删除所有的五
        myArraylist.display();
    }


}
 class MyArraylist {

    public int[] elem;
    public int usedSize;//0
    //默认容量
    private static final int DEFAULT_SIZE = 10;

    public MyArraylist() {
        this.elem = new int[DEFAULT_SIZE];
    }//这里定义规定好了，线性表的长度为10个

    /**
     * 打印顺序表:
     * 根据usedSize判断即可
     */
    public void display() {
        if(isEmpty()){
            return ;
        }
        for (int i = 0; i < size(); i++) {
            System.out.print(elem[i]+" ");
        }
        System.out.println();
    }

    // 新增元素,默认在数组最后新增
    public void add(int data) {
        if(isFull()){//如果满了需要进行扩容
            elem= Arrays.copyOf(elem,(elem.length*2));
        }
        elem[usedSize++]=data;
      //  elem[usedSize++]=data;

    }

    /**
     * 判断当前的顺序表是不是满的！
     *
     * @return true:满   false代表空
     */
    public boolean isFull() {
      if(elem.length==usedSize) {
         // elem= Arrays.copyOf(elem,(elem.length*2));//第一种扩容方法
            return true;
        }
        //第二种扩容方法
//            int newsize=elem.length*2;
//            int []elem=new int[newsize];
//            System.arraycopy(elem,0,elem,0,elem.length);
      return false;
    }


    private boolean checkPosInAdd(int pos) {
        if(pos<0 || pos>=elem.length){
            return false;
        }

        return true;//合法
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if(isFull()) {//如果满了需要进行扩容
            elem= Arrays.copyOf(elem,(elem.length*2));
        }
            if (checkPosInAdd(pos)) {
                for (int i = size(); i > pos; i--) {
                    elem[i] = elem[i - 1];
                }
                elem[pos] = data;
                usedSize++;
            }


    }

    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int x:elem) {
            if(x==toFind){
                return true;
            }
        }
        return false;
    }

    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i < elem.length; i++) {
            if(toFind==elem[i]){
                return i;
            }
        }
        return -1;//return -1代表不存在这个元素；
    }

    // 获取 pos 位置的元素
    public int get(int pos) throws IndexOutOfBoundsException {
      if(!checkPosInAdd(pos)) {
          throw new MyIndexOutOfBoundsException("数组下标错误");
      }
      return elem[pos];
    }

    private boolean isEmpty() {
        if(elem.length==0)
         return true;
        else
            return false;
    }

    // 给 pos 位置的元素设为【更新为】 value
    public void set(int pos, int value) {
        if(checkPosInAdd(pos)){
            elem[pos]=value;//elem[pos]pos就直接代表了下标为4
        }

    }

    /**
     * 删除第一次出现的关键字key
     *
     * @param key
     */
    public void remove(int key) {
        if(isEmpty()){
            System.out.println("线性表为空,无法进行删除操作！");
            return ;
        }
        for (int i = 0; i < size(); i++) {
            if(elem[i]==key){
                for (int j = i; j < elem.length-1; j++) {
                    elem[j]=elem[j+1];
                }
                usedSize--;
                return;//当进行第一次删除后，便返回了，所以不会进行连续的删除了。
            }
        }


    }
    //删除所有出现的关键字
      public void removeall(int key){
        //方法1
//        if(isEmpty()){
//            System.out.println("单链表为空，无法进行删除操作！");
//        }
//          for (int i = 0; i < elem.length-1; i++) {
//              if(elem[i]==key){
//                  for (int j = i; j < elem.length-1; j++) {
//                      elem[j]=elem[j+1];
//                  }
//                 usedSize--;
//                  i--;
//              }
//          }
          //方法2
          if(isEmpty()){
              System.out.println("单链表为空，无法进行删除操作！");
              return ;
          }
          int i=0;
          while(elem[i]!=0){
              if(elem[i]!=key){//如果第一次当i=5时，elem[i]此时的值是等于key的，如果下一个但i=6时，虽然值还是等于五但是不会在进去了
                               //但是此时下面的当前一个元素等于关键字时，后一个也等于的时候，下面的代码已经将后一个元素移动值上一个了，所以
                               //不用担心没有进去，但是相同的元素还是可以被消除掉的；
                  i++;
              }
              else{
                  for (int j = i; j < elem.length-1; j++) {
                      elem[j]=elem[j+1];
                  }
                  usedSize--;
              }
          }

      }

    // 获取顺序表长度
    public int size() {
     return usedSize;
    }

    // 清空顺序表
    public void clear() {
        //清空可以直接让elem.length=0;
        for (int i = 0; i < size(); i++) {
            elem[i]=0;
        }
    }

 }
