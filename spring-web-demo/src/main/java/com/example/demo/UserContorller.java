package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController //是一个标签作用，给spring打了一个标签
@RequestMapping("/v2")//类路径
public class UserContorller {
    @RequestMapping(value = "/v1",method = RequestMethod.POST)// 这里已经建立连接，表示项目的资源 ，这是一个方法路径
   public String print(){
        return "hello v1";
   }

}
