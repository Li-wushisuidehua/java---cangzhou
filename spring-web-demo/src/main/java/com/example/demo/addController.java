package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//导入了一个新的包
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cul")
public class addController {
    @RequestMapping(value = "add",method = RequestMethod.POST)
    public String add(int x,int y){
        return "计算的sum结果为："+(x+y);
    }
    @RequestMapping("/sum")
    public String sum(@RequestParam("num1") Integer num1,
                      @RequestParam("num2") Integer num2){
        Integer sum = num1 + num2;
        return "计算机计算结果:" + sum;
    }
}
