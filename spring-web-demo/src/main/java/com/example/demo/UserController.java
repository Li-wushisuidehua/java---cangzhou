package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/User")
@RestController//标记作用，告诉Spring这个需要被管理
public class UserController {
    @RequestMapping("/p1")
    public String p1(String name){
        return "接收到了参数:"+name;
    }
    @RequestMapping("/p2")
    public String p2(int age){
        return "接收到了参数:"+age;
    }
    @RequestMapping("/p3")
        public String p3(Integer age){
            return "接收到了参数:"+age;
        }
        @RequestMapping("/p4")
    public String p4(boolean ret){ //boolean 类型是唯一一个可以接收空值的普通类型变量，接收到的空值会显示false
        return "接收到了参数:"+ret;
        }

}
