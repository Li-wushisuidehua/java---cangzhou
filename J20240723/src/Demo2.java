import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Demo2 {
    public static void main(String[] args) {
        int []arr=new int[]{1,1,1};
        System.out.println(MLS(arr));
    }
    public static int MLS (int[] arr) {
        Arrays.sort(arr);
        int left=0;
        int right=0;
        int len=arr.length;
        int maxLen=0;
        for(;right<len;){
            if(right+1<len && (arr[right]+1)==arr[right+1]){
                left=right;
                while(right+1<len &&(arr[right]+1)==arr[right+1]){
                    maxLen=Math.max(maxLen,right-left+1);
                    right++;
                }
            }else  right++;

        }
        return maxLen+1;
    }
}
class Read{
    StringTokenizer st=new StringTokenizer(" ");//剪裁
    BufferedReader bf=new BufferedReader(new InputStreamReader(System.in));
    String next() throws IOException {
        while(!st.hasMoreTokens()){ //查看后面的是否还有需要裁剪的
            st=new StringTokenizer(bf.readLine()); //开始裁剪
        }
        return st.nextToken();//一个一个通过“ ”空格分离的子字符串，next是在返回时会检查该子字符串是否为最后一个被原字符串依据空格裁剪的子字符串
    }
    String nextLine() throws Exception{
        return bf.readLine();
    }
    int nextInt() throws Exception{
        return Integer.parseInt(next());
    }
    long nextLong() throws Exception{
        return Long.parseLong(next());
    }
    double nextDuble() throws Exception{
        return Double.parseDouble(next());
    }

}

