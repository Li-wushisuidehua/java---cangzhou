//求两个正整数的最小公倍数
public class Demo1 {
    public static void main(String[] args) {
        System.out.println(lcm(4 * 6, gcd(4, 6)));
    }
    //求最大公约数
    public static int gcd(int a,int b){
        if(b==0){
            return a;
        }
        return gcd(b,a%b);

    }
    //求最小公倍数 = 两整数数总和/ 最大的公因数
    public static int lcm(int y,int x){
           return y/x;
    }
}
