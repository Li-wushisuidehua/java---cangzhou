package Day20;

import java.io.*;
import java.util.StringTokenizer;

public class Demo2 {
    public static Read in=new Read();
    public static PrintWriter out=new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
    public static void main(String[] args) throws Exception{
        int m=in.nextInt();
        long [] arr=new long[m];
        long max=Integer.MIN_VALUE,sum=0,ret=0;
        for (int i = 0; i < m; i++) {
            arr[i]=in.nextLong();
            sum+=arr[i];
            max=Math.max(max,sum);
            if(sum<0){
                sum=0;
            }
        }
        System.out.println(max);
    }
}
class Read{
    StringTokenizer st=new StringTokenizer(" ");//剪裁
    BufferedReader bf=new BufferedReader(new InputStreamReader(System.in));
    String next() throws IOException{
        while(!st.hasMoreTokens()){
            st=new StringTokenizer(bf.readLine());
        }
        return st.nextToken();
    }
    String nextLine() throws Exception{
        return bf.readLine();
    }
    double nextDouble() throws Exception{
        return Double.parseDouble(next());
    }
    int nextInt() throws Exception{
        return Integer.parseInt(next());
    }
    long nextLong() throws Exception{
        return Long.parseLong(next());
    }
}
