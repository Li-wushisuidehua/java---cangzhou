package Day20;

import java.util.Scanner;

//输入四个整数
public class Demo1 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int a=in.nextInt();//攻击力
        int h=in.nextInt();//血量
        int b=in.nextInt();
        int k=in.nextInt();
        int sum=0;
        while(h>=0 && k>=0){
            sum+=a;
            sum+=b;
            h-=b;
            k-=a;
        }
        if(h>=0){
            sum+=(long)a*10;//注意这里会溢出，当a无限接近于int的最大值的时候，此时给a*10，会致使负溢出
        }
        if(k>=0){
            sum+=(long)b*10;
        }
        System.out.println(sum);

    }
}
