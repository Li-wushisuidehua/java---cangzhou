package Day20;

import java.util.Scanner;

public class Demo3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        char[] arr = str.toCharArray();
        int left = 0, right = str.length() - 1, len = str.length(), ret = 0;
        //遍历该字符串是否是aaaaaaaa这种类型的回文字符串，这样的字符串非回文字符串为0
        for (int i = 1; i < len; i++) {
            if (arr[i] != arr[i - 1]) ret = 1;
        }
        //遍历该字符串查看该字符串是否是回文字符串 类似于这样“ababa”这样的字符串最长的非回文字符串为len-1
        while (left <= right && arr[left]==arr[right]) {
                left++;
                right--;
        }

        if (ret == 0) {
            //对应情况为aaaaaaa
            System.out.println(0);
        } else if (left >= right) {
            //表示此时的字符串为回文字符串，此时去掉字符串两边的任意字符，此时的字符串便不再是回文字符串
            System.out.println(len-1);

        }else System.out.println(len); //此时表示这个字符串不是回文字符串，如果这个字符串整体便不是回文字符串，便没有在需要考虑的情况
    }
}
