package Day22;

import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s1=in.nextLine();
        String s2=in.nextLine();
        int l1=s1.length();
        int l2=s2.length();
        int sum=0,min=l1;//最差的情况下也就是匹配的时候所有的字符串均不相等，所以说min初始化为l1
        int len=s2.length()-s1.length();
        for (int i = 0; i <=l2-l1; i++) {//i代表为长的字符串
            for (int j = 0; j < l1; j++) {//j代表的为短的字符串
                if(s1.charAt(j)!=s2.charAt(i+j)){
                    sum++;
                }
            }
            min=Math.min(sum,min);
            sum=0;
        }
        System.out.println(min);
    }
}
