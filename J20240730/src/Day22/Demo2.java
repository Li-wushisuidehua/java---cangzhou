package Day22;

import java.util.PriorityQueue;
import java.util.Scanner;

public class Demo2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n=in.nextInt(),max=0,m=0;
        if(n==0) return ;
        PriorityQueue<Integer> heap=new PriorityQueue<>();
        for (int i = 0; i < n; i++) {
            m=in.nextInt();
            max=Math.max(max,m);
            heap.offer(m);
        }
        while(heap.peek()!=max){
            m=heap.poll()*2;
            if(m>max) {
                System.out.println("NO");
                return ;
            }
            heap.offer(m);
        }
        System.out.println("YES");
    }
}
