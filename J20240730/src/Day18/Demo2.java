package Day18;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.StringTokenizer;
import java.io.*;

class Orange{
    public int a;
    public int b;

    public Orange() {
        this.a = a;
        this.b = b;
    }
}
public class Demo2 {
   public static Read in=new Read();
    public static PrintWriter out=new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
    public static void main(String[] args) throws Exception{
       int m=in.nextInt();//m为总个数
       int k=in.nextInt();
       //这里一个蜜柑有它的甜度与酸度，这里我们可以将一个Orange当作为一个类，并且强度和酸度为这个类的属性
        Orange []o=new Orange[m];
        for (int i = 0; i < m; i++) {
            o[i]=new Orange();
            o[i].a=in.nextInt();//酸度
        }
        for (int i = 0; i < m; i++) {
            o[i].b=in.nextInt();//甜度
        }
        Arrays.sort(o,(x,y)->{
           if(x.b==y.b) return x.a-y.a;
           return y.b-x.b;
        });
        long sum=0,sum1=0;
        for (int i = 0; i <k ; i++) {
           sum+=o[i].a;//酸度之和
           sum1+=o[i].b;//甜度之和
        }
        System.out.println(sum+" "+sum1);
    }
}
class Read{
    StringTokenizer st=new StringTokenizer(" ");//剪裁
    BufferedReader bf=new BufferedReader(new InputStreamReader(System.in));//InputstreamReader
    String next() throws IOException{
        while(!st.hasMoreTokens()){
            st=new StringTokenizer(bf.readLine());
        }
        return st.nextToken();
    }
    String nextLine() throws Exception{
        return bf.readLine();
    }
    int nextInt() throws Exception{
        return Integer.parseInt(next());
    }
    long nextLong() throws Exception{
        return Long.parseLong(next());
    }
    double nextDouble() throws Exception{
        return Double.parseDouble(next());
    }
}
//已改