package Day19;

import java.util.Scanner;

public class Demo3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        StringBuffer sbu=new StringBuffer();
        // 注意 hasNext 和 hasNextLine 的区别
         int n=in.nextInt(),m=0;
        for (int i = 0; i < n; i++) {
               m=in.nextInt();
            for (int j = 0; j < m; j++) {
                String ch=in.next();
                sbu.append(ch);//字符的输入
            }
            if(fun(sbu.toString())) System.out.println("Yes");
            else System.out.println("No");
            sbu=new StringBuffer();
        }

    }
    public static Boolean fun(String s){
        int left=0;
        int right=s.length()-1;
        while(left<right){
            if(s.charAt(left)!=s.charAt(right)){
               return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
