package Day19;

import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        int m=in.nextInt();
        long sum=in.nextLong();
        long x=0;
        for (int i = 0; i < m; i++) {
            x=in.nextLong();
            if(sum>=x) {
                sum += x;
            }else {
                sum+=gcd(sum,x);
             }

        }
        System.out.println(sum);

    }
    public static long gcd(long x,long y){
        long n=0;
        while(y%x!=0){
            n=y%x;
            y=x;
            x=n;
        }
        return x;
    }
}
