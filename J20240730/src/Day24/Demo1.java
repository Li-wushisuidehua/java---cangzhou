package Day24;

public class Demo1 {
    public static void main(String[] args) {

    }
//判断是否为平衡二叉树---方法一
    public boolean IsBalanced_Solution(TreeNode pRoot) {
        if (pRoot == null) {
            return true;
        }
        int suml = IsBalance(pRoot.left);
        int sumr = IsBalance(pRoot.right);
        if(suml<0 || sumr<0) return false;
        return Math.abs(suml - sumr) <= 1 ? true : false;
    }

    public int IsBalance(TreeNode root) {
        if (root == null) return 0;
        int SumofL = IsBalance(root.left);
        int SumofR = IsBalance(root.right);
        if(SumofL<0 || SumofR<0) return -1;
        if(Math.abs(SumofL-SumofR)<=1)
        return Math.max(SumofL, SumofR) + 1;

        return -1;
    }
    //判断是否为平衡二叉树---方法二
    public boolean isBalanced(TreeNode root) {
        if(root == null) return true;

        int leftH = maxDepth(root.left);
        int rightH = maxDepth(root.right);

        return Math.abs(leftH - rightH) <= 1 //这里也可以看到如果 左右两边的差距大了会直接返回
                && isBalanced(root.left)
                && isBalanced(root.right);
    }
    public int maxDepth(TreeNode root) {
        if(root == null) {
            return 0;
        }
        int leftHeight = maxDepth(root.left);
        int rightHeight = maxDepth(root.right);

        return leftHeight > rightHeight ?
                leftHeight+1:rightHeight+1;
    }
}

class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;
    }
}
