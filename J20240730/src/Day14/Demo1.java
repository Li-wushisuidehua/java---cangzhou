package Day14;

import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str1=in.nextLine();
        String str2=in.nextLine();
        int len1=str1.length();
        int len2=str2.length();
        int flag=0;
        int []ch=new int[26];
        for (int i = 0; i < len1; i++) {
            ch[str1.charAt(i)-'A']++;
        }
        for (int i = 0; i < len2; i++) {
            if(--ch[str2.charAt(i)-'A']<0) {
                flag=1;
            }
        }
        System.out.println(flag==0?"Yes":"No");

    }
}
