package Day2;

import java.util.Scanner;

public class Demo3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m=in.nextInt(),ret=Integer.MAX_VALUE;//数组长度
        String s1=in.next();
        String s2=in.next();
        int pre1=-1,pre2=-1;
        if (s1.length() == 0 || s2.length() == 0){
            System.out.println(-1);
            return ;
        }

        String [] s=new String[m];
        for(int i=0;i<m;i++){
            s[i]=in.next();
            if(m<2) {
                System.out.println(-1);
                return;
            }
            if(s[i].equals(s1)) pre1=i;
            if(s[i].equals(s2)) pre2=i;
            if(pre1!=-1 && pre2!=-1)
                ret=Math.min(Math.abs(pre1-pre2),ret);
        }
        System.out.println(ret==Integer.MAX_VALUE?-1:ret);
    }
}
