package Day2;

import java.util.Scanner;

/**
 * 动态规划类型的题
 *  1. dp表创建
 *  2. dp表初始化
 *  3. dp表赋值
 *  4. 返回值
 */

public class Demo2 {
    //最小花费爬楼梯--解法一
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        int m=in.nextInt();
        int []arr=new int[m];

        //dp表的创建
        int[] dp=new int[m+1];

        //dp表的初始化
        dp[0]=dp[1]=0;//此时dp[0]和dp[1]均为0了

        for (int i = 0; i < m; i++) {
            arr[i]=in.nextInt();
        }

        //dp表各项赋初始值
        for (int i = 2; i <=m ; i++) {
            dp[i]=Math.min(dp[i-1]+arr[i-1],dp[i-2]+arr[i-2]);
        }

        //输出
        System.out.println(dp[m]);

    }
    //最小花费爬楼梯--解法二
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m=in.nextInt();
        int []arr=new int[m];

        //dp表的创建
        int[] dp=new int[m];//dp表，记录每次上楼梯需要花费的值
        for (int i = 0; i < m; i++) {
            arr[i]=in.nextInt();
        }

        //dp表的初始化
        dp[m-1]=arr[m-1];
        dp[m-2]=arr[m-2];

        //dp表中各项值赋值
        for (int i = m-3; i >=0 ; i--) {
            dp[i]=Math.min(dp[i+1]+arr[i],dp[i+2]+arr[i]);
        }

        //输出
        System.out.println(Math.min(dp[0],dp[1]));

    }

}
