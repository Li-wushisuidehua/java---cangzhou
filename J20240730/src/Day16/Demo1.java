package Day16;
//注意题目中未提及转化未大写字母
public class Demo1 {
    public static void main(String[] args) {
        String str="%s%s%sD%s";
        char[] ch=new char[]{'a','C','h','m'};
        formatString(str,ch);
        System.out.println(formatString(str,ch));
    }
    public static String formatString (String str, char[] arg) {
        String[] strings=str.split("%s");
        int i=0;
        StringBuffer stb=new StringBuffer();
        for (String s:strings) {
            stb.append(s);
            if(i<arg.length)
            stb.append(arg[i++]);
        }
        while(i<arg.length){
            stb.append(arg[i++]);
        }
        return stb.toString();
    }
    //已改
}
