package Day16;

import java.util.Scanner;

//质数的求解！！！！
public class Demo2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int ret=0;

        //遍历数组区间
        for (int i = Math.max(a,10); i <=b ; i++) {
            ret+=check(i);//ret记录满足神奇数的个数
        }
        System.out.println(ret);
    }

    public static int check(int x){
        int [] arr=new int[10];
        int n=0;
        while(x!=0){
            arr[n++]=x%10;
            x/=10;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(arr[i]!=0 && i!=j){
                    if(Isprim(arr[i]*10 +arr[j])){
                        return 1;//但凡里面有一个满足条件，就可直接返回
                    }
                }
            }
        }
        return 0;//当所有可能会满足的都不满足时，这时返回0；
    }
    //判断是否为素数
    public static Boolean Isprim(int x){
        if(x<2) return false;
        for (int i = 2; i <=Math.sqrt(x) ; i++) {
            if(x%i==0) return false;
        }
        return true;
    }
//已改
}
