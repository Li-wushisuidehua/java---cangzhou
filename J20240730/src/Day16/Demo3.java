package Day16;

import java.util.Scanner;

public class Demo3 {
    //滑动窗口解法一
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        String str=in.nextLine();
        char [] chs=str.toCharArray();
        int n=in.nextInt(),left=0,right=n-1,len=str.length(),cnt=0,maxlen=0,i=0,begin=0;
        while(right<len){
            i=left;
            while(i<=right){
                if(chs[i]=='C'||chs[i]=='G'){
                    cnt++;
                }
                i++;
            }
                if(cnt>maxlen){
                    begin=left;
                    maxlen=cnt;
                }
            right++; left++; cnt=0;
        }
        for (int j = begin; j <begin+n ; j++) {
            System.out.print(chs[j]);
        }
    }
//滑动窗口解法二
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        char [] s=in.next().toCharArray();
        int x=in.nextInt();
        int begin=0,left=0,right=0,n=s.length,count=0,maxCount=0;
        while(right<n){
            if(s[right]=='C' || s[right]=='G') count++;
            if(right-left+1 >x) {
                if(s[left]=='C' || s[left]=='G') count--;
            }
            left++;
            if(right-left+1 ==x){
                begin=left;
                count=maxCount;
            }
            right++;
        }

        for (int i = begin; i <begin+x ; i++) {
            System.out.print(s[i]);
        }
//已改
    }
}
