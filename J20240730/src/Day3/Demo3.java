package Day3;

import java.util.PriorityQueue;
import java.util.Scanner;

// 除2！
//原理：贪心+堆+模拟
public class Demo3 {
    public static void main(String [] args){
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        int k=in.nextInt();
        long sum=0,x=0;
        PriorityQueue<Integer> heap=new PriorityQueue<>((a,b)->b-a);
        for(int i=0;i<n;i++){
            x=in.nextInt();
            sum+=x;
            if(x%2==0){
                heap.add((int)x);//因为x为long类型，而优先级队列中的规定类型为Integer
            }
        }
        while(!heap.isEmpty() && k--!=0){
            long t=heap.poll()/2;
            sum-=t;
            if(t%2==0){
                heap.add((int) t);
            }
        }
        System.out.println(sum);
    }
}
