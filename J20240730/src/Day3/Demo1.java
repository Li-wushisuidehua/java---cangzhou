package Day3;

import java.util.Scanner;

//简写单词
public class Demo1 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        while(in.hasNext()){
            char ch=in.next().charAt(0);
            if(ch>='a' && ch<='z') System.out.print((char)(ch-32));
            else System.out.print(ch);
        }

    }

}

