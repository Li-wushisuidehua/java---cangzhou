package Day21;

import java.util.Arrays;
import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m=in.nextInt(),left = m-2, right = m-1;
        int []arr=new int[m];
        for (int i = 0; i < m; i++) {
            arr[i]=i+1;
        }
          while(left>=0){
            swap(arr,left,right);
              left -= 2;
              right -= 2;
          }
        for (int i = 0; i < m; i++) {
            System.out.print(arr[i]+" ");
        }
    }

public static void swap(int []arr,int left,int right){
        int n=arr[left];
        arr[left]=arr[right];
        arr[right]=n;
}
}
