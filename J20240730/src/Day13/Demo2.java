package Day13;

import java.util.HashMap;

public class Demo2 {
    public static int maxLength (int[] arr) {
        int count=0,maxlen=0,left=0,right=1;
        HashMap<Integer,Integer> hashMap=new HashMap<>();
        hashMap.put(arr[0],arr[0]); count=1;maxlen=1;
        for (; right < arr.length; right++) {
            if(!hashMap.containsValue(arr[right])){
                hashMap.put(arr[right],arr[right]);
                maxlen=Math.max(maxlen,right-left+1);
            }else{
                if(arr[left++]!=arr[right]){
                    left++;
                }
                hashMap.remove(arr[left-1]);
                hashMap.put(arr[right],arr[right]);

            }
        }
        return maxlen;
    }

    public static void main(String[] args) {
        int[] arr=new int[]{1,2,3,1,2,3,2,2};
        maxLength(arr);
        System.out.println(maxLength(arr));
    }
}
