package Day17;

import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        long x=in.nextLong();
        StringBuffer sbu=new StringBuffer();
        while(x!=0){
            if((x%10)%2==0){
                sbu.append(0);
            }else{
                sbu.append(1);
            }
            x/=10;
        }
        System.out.println(Long.valueOf(sbu.reverse().toString()));
//已改
    }
}
