package Day17;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Demo2 {
   public static Read in=new Read();
    public  static PrintWriter out=new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
    //下面这种解法太暴力--超时
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int a=in.nextInt();
//        int b=in.nextInt();
//        int sum=0;
//        long [][] arr=new long[a][b];
//        long[][] ret=new long[a][b];
//        for (int i = 0; i < a; i++) {
//            for (int j = 0; j < b; j++) {
//                arr[i][j]=in.nextInt();
//            }
//        }
//        for (int i = 0; i < a; i++) {
//            for (int j = 0; j < b; j++) {
//                for (int n = 0; n < b; n++) {
//                        sum+=arr[i][n];
//                }
//                for (int k = 0; k < a; k++) {
//                        sum+=arr[k][j];
//                }
//                ret[i][j]=sum-arr[i][j];
//                sum=0;
//            }
//        }
//        for (int i = 0; i < a; i++) {
//            for (int j = 0; j < b; j++) {
//                System.out.print(ret[i][j]+" ");
//            }
//            System.out.println();
//
//        }
//
//    }
    public static void main(String[] args) throws Exception {
        int a=in.nextInt();
        int b=in.nextInt();
        long []a1=new long[a];//记录每一行的总和
        long []b1=new long[b];//记录每一列的总和
        long [][] arr=new long[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                arr[i][j]=in.nextInt();
                a1[i]+=arr[i][j];
                b1[j]+=arr[i][j];
            }
        }
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                out.print((a1[i]+b1[j]-arr[i][j])+" ");//快速输出
            }
            out.println();//由于数据量大，输出的时候也需要快速输出
        }
        out.close();//使用完后注意关闭
    }
}
class Read{
    StringTokenizer st=new StringTokenizer(" ");
    BufferedReader bf=new BufferedReader(new InputStreamReader(System.in));
    String next() throws IOException {
        while(!st.hasMoreTokens()){
            st=new StringTokenizer(bf.readLine());
        }
        return st.nextToken();
    }
    String nextLine() throws Exception{
        return bf.readLine();
    }
    int nextInt() throws Exception{
        return Integer.parseInt(next());
    }
    long nextLong() throws Exception{
        return Long.parseLong(next());
    }
    double nextDouble() throws Exception{
        return Double.parseDouble(next());
    }

}
//已改