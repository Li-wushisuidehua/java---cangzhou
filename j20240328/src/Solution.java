public class Solution {
    public static  int removeElement(int[] nums, int val) {
        int count=0;
        for(int i=0;i<nums.length;i++){
            if((val^nums[i])!=0){
                nums[count++]=nums[i];

            }
        }
        return count;

    }

    public static void main(String[] args) {
        int []nums={3,2,2,3};
       int count= removeElement(nums,3);
        for (int i = 0; i < count; i++) {
            System.out.print(nums[i]+" ");
        }
    }
}
