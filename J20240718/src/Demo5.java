public class Demo5 {
//笔试强训 day4
    //单词检索  算法 dfs

    public  static int m,n;
   public static int []dx={0,0,1,-1};
   public static int []dy={1,-1,0,0};
   public static boolean [][] vis;
   public static char[] word;
    public static boolean exist (String[] board, String _word) {
        m=board.length;
        n=board[0].length();
        vis=new boolean[m][n];
        word=_word.toCharArray();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if(board[i].charAt(j)==word[0]){
                    if(dfs(board,i,j,0)==true) return true;
                }
            }
        }
        return false;
    }
    //查看上下左右的函数
    public  static boolean dfs(String[] board,int i,int j,int pos){
        if(pos== word.length-1){
            return true;
        }
        vis[i][j]=true;
        for (int k = 0; k < 4; k++) {
            int x=i+dx[k],y=j+dy[k];
            if(x>=0 && x<m && y>=0 && y<n && !vis[x][y] && board[x].charAt(y)==word[pos+1]){
                if(dfs(board,x,y,pos+1)) return true;
            }

        }
        vis[i][j]=false;
        return false;

    }

    public static void main(String[] args) {
        String [] strings=new String[]{"XYZE","SFZS","XDEE"};
        String s="XYZZED";
         boolean bool=exist(strings,s);
        System.out.println(bool);
    }
}
