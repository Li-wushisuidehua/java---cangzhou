import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * 编码：
 * ascii码，其中数字和英文均占一个字节
 * GBK，其中容纳了ascii也就是说，GBK中的数字和英文占一个字节，其中GBK中的汉字占两个字节
 * GBK中汉字以1开头，英文和数字以0开头
 * UTF-8，其中容纳了ascii，其中的汉字占三个字节
 * 形式：随时可以上网查
 */

public class Test {
    //如何在Java中使用编码和解码
    public static void main(String[] args) throws Exception {
        //编码
        String s1="a哈bc";
        //使用utf-8来进行编码
        byte[] bytes=s1.getBytes();//注意：此时如果括号内没有写字符集，此时会按照编译器默认的字符集，右下角明显的utf-8
        //使用gbk来进行编码
        byte[] bytes1=s1.getBytes("GBK");
        //输出，使用utf-8编码的
        System.out.println(Arrays.toString(bytes));
        //输出使用GBK编码的
        System.out.println(Arrays.toString(bytes1));
        //解码
        //使用utf-8来进行解码
        String s2=new String(bytes);
        System.out.println(s2);
        //使用GBK来对其进行解码
        String s=new String(bytes1);
        System.out.println(s);//输出为a？？b,因为编码时使用的时GBK的编码，而这里的解码如果没有提供解码的字符集，会默认使用平台的字符集也就是utf-8(右下角）
        String s3=new String(bytes1,"GBK");//此时输出为a哈bc
        System.out.println(s);
    }
}
