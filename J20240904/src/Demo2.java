import java.util.Arrays;

public class Demo2 {
    public static void main(String[] args) {
        int []arr=new int[]{1,1};
        System.out.println(minOperations(arr, 3));
    }
    public static int minOperations(int[] nums, int x) {
//        Arrays.sort(nums);
        int sum1=0,target=0,len=nums.length,left=0,right=0,sum2=0,ret=-1;
        for (int i = 0; i < len; i++) {
            sum1+=nums[i];
        }
        target=sum1-x;
        while(right<len){
            sum2+=nums[right];
            while(sum2>target && left<len){
                sum2-=nums[left];
                left++;
            }
            if(sum2==target) {
                ret=Math.max(ret,right-left+1);
            }
            right++;
        }
        return ret==-1?-1:len-ret;

    }
}
