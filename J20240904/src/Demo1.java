public class Demo1 {
    public static void main(String[] args) {
        int [] arr=new int[]{1,1,0,0,0,1,1,1,0,0,1};
        System.out.println(longestOnes1(arr,3));

    }

    public static int longestOnes(int[] nums, int k) {
        int ret=0;
        for (int right= 0,left=0,zero=0; right< nums.length; right++) {
            if(nums[right]==0) zero++;
            while(zero>k){
                if(nums[left++]==0) zero--;//每次只往后走一步
            }
            ret=Math.max(ret,right-left+1);
        }
        return ret;
    }
    public static int longestOnes1(int[] nums, int k) {
        int l = 0,r=0;
        for (; r < nums.length; r++) {
            k-=1-nums[r];
            if(k<0){
                k+=1-nums[l];
                l++;
            }
        }
        return r-l;
    }
}
