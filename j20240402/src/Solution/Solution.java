package Solution;

import java.util.Arrays;

public class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int p=nums1.length-1;
        while(m>0 && n>0){
            if(nums1[m-1]>nums2[n-1]){
                nums1[p--]=nums1[m-1];
                m--;
            }
            else{
                nums1[p--]=nums2[n-1];
                n--;
            }

        }
        if(m<0){
            while(n>=0){
                nums1[p--]=nums2[n];
                n--;
            }
        }
        System.out.println("应该输出122356");
        System.out.println("===============");
        System.out.println(Arrays.toString(nums1));
//
    }
}
