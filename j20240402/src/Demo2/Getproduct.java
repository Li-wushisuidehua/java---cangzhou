package Demo2;

import java.util.ArrayList;
import java.util.Random;

//奖金可以是钱，也可以是礼品
public class Getproduct<T> {
    Random random=new Random();
    private T product;
    //获奖商品存储在顺序表中
    ArrayList<T> list=new ArrayList<>();//顺序表最初自动开辟10个内存存储，当满了之后会已1.5倍扩容；
   public void Setproducts(T t){
        list.add(t);//由于Arraylist类中显示的存储的为T类型，所以此时的T类型是被允许的;
    }
   public T Getproducts() {
       return list.get(random.nextInt(list.size()));//生成 0~list.size()范围类的随机值，随机值表示下标，随机数的生成遵循左闭右开的原则
   }
}
