package Demo2;

/**
 * 实现抽奖池，现实生活中抽奖池中会有许多的类型的奖项，但是要将这些奖项用代码来实现时，
 * 就不得不使用到的时泛型，泛型可以容纳各种类型的类类型；
 */
public class Test {
    public static void main(String[] args) {
        Getproduct<String> getproduct=new Getproduct<>();
        //这里也可以将奖项用字符串数组进行表示，跟下面的整形数组一样，进行传递
        getproduct.Setproducts("苹果手机");
        getproduct.Setproducts("华为手机");
        getproduct.Setproducts("折叠手机");
        getproduct.Setproducts("笔记本电脑");
        String product=getproduct.Getproducts();
        System.out.println("恭喜您，您获得了"+product);
        System.out.println("============");
        Getproduct<Integer> getproduct1=new Getproduct<>();
        Integer []array=new Integer[]{10000,20000,3000,500,50,10};
        for (int i = 0; i < array.length; i++) {
            getproduct1.Setproducts(array[i]);
        }
        int money=getproduct1.Getproducts();
        System.out.println("恭喜您，您获得了"+money);

    }
}
