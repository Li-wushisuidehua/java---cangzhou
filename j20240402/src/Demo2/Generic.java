package Demo2;

import java.util.ArrayList;
import java.util.Random;

public class Generic <T1,T2>{
    private T1 name;
    Random random=new Random();
    public void eat(T1 name){
        System.out.println(this.name+"正在吃饭");
    }
    public void play(){

    }
    ArrayList<T1> list=new ArrayList<>();//顺序表最初自动开辟10个内存存储，当满了之后会已1.5倍扩容；
    public void Setproducts(T1 t){

        list.add(t);//由于Arraylist类中显示的存储的为T类型，所以此时的T类型是被允许的;
    }
    public T1 Getproducts() {
        return list.get(random.nextInt(list.size()));//生成 0~list.size()范围类的随机值，随机值表示下标，随机数的生成遵循左闭右开的原则
    }
    ArrayList<T2> list2=new ArrayList<>();
    public void Setproducts1(T2 t2){
        list2.add(t2);
    }
    public T2 Getproducts2() {
        return list2.get(random.nextInt(list2.size()));//生成 0~list.size()范围类的随机值，随机值表示下标，随机数的生成遵循左闭右开的原则
    }
}
