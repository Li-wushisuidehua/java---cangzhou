public class Studnet {
    public String name;
    public int age;

    public Studnet(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //重写toString方法，方便以后的打印也为toSting方法
     @Override
    public String toString() {
        return "Studnet{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
