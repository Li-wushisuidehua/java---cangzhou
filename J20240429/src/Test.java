import java.util.Scanner;
import java.util.Stack;

public class Test {
    //寻找字符串的子串是否存在
    public static boolean isSubsequence(String S, String T) {
        int lengthS=S.length();
        int lengthT=T.length();
        int i=0;
        int j=0;
        while(i<S.length() && j<T.length()){
            if(S.charAt(i)==T.charAt(j)){
                i++;
                j++;
            }else{
                j++;
            }
        }
        return i==S.length();
    }

        public static void main2(String[] args) {
            System.out.println(isSubsequence("nowcoder","nowcoder"));

        }
    public static void main(String[] args) {
        //匹配问题使用栈会很好解决问题
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        int i=0;
        Stack<Character> stack=new Stack<>();
        while(i<str.length()){
            char n=str.charAt(i);
            if(stack.empty()){
                stack.push(str.charAt(i));
            }else{
                if(stack.peek()==n){//1.当此时的元素与栈中已有的元素相等时，此时会将元素弹出栈
                    stack.pop();
                }else{//不满足第一个时，必定会满足第二个，此时需要将元素放入栈中
                    stack.push(str.charAt(i));
                }
            }
            i++;
        }
        if(stack.empty()){
            System.out.println("0");
        }else{
            Stack<Character> stack2=new Stack<>();
            while(!stack.empty()){
               stack2.push(stack.pop());
            }
            while(!stack2.empty()){
                System.out.print(stack2.pop());
            }
        }
    }
    
}
