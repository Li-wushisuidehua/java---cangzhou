package Demo2;

public class Test {
    public  void turn(){
        System.out.println("打开电脑！");
    }
    public  void turnon1(Icomputer icomputer){//向上转型

        icomputer.turnon();
        if(icomputer instanceof Keyboard){
            Keyboard keyboard=(Keyboard) icomputer;//向下转型
            keyboard.click();
        }
        else if(icomputer instanceof Draw){
            Draw draw=(Draw) icomputer;//向下转型
            draw.draw();
        }
        icomputer.turnoff();
    }
    public  void off(){
        System.out.println("关闭电脑");
    }
    public static void main(String[] args) {
        Test test=new Test();
        test.turn();
        test.turnon1(new Keyboard());
        test.turnon1(new Draw());
        test. off();
    }
}
