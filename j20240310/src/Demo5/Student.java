package Demo5;

import java.awt.datatransfer.ClipboardOwner;

 public class Student implements Comparable<Student> {
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

//    @Override
//    public int compareTo(Student o) {
//        return this.age-o.age;
//    }

     @Override
     public int compareTo(Student o) {
         if(this.name.compareTo(o.name)>0){
             return 1;
         } else if (this.name.compareTo(o.name)<0) {
             return -1;
         }
         else {
             return 0;
         }
     }
 }
 class Test {
    public static void main(String[] args) {
        Student student=new Student("zhangsan",17);
        Student student2=new Student("lisi",6);
        //进行比较大小
        if(student.compareTo(student2)>0){
            System.out.println("student>studen2");
        }
    }





}
