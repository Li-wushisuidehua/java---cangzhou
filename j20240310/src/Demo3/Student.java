package Demo3;

import java.sql.Array;
import java.util.Arrays;


public class Student implements Comparable<Student>{
    public  String name;
    public int age;

    @Override
    public int compareTo(Student o) {
        return this.age-o.age;//this.xxx-o.xx得出的结果按从小到大进行排序;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }


    public static void main(String[] args) {
        Student[] students=new Student[3];
        students[0]=new Student("zhangsan",1);
        students[1]=new Student("lisi",8);
        students[2]=new Student("wangwu",5);
        Arrays.sort(students);
        System.out.println(Arrays.toString(students));
    }
    public static void main1(String[] args) {
        Student student1=new Student("zhangsan",18);
        Student student2=new Student("lisi",15);

        if(student1.compareTo(student2)>0){
            System.out.println("stu1>stu2");
        }

    }
}
