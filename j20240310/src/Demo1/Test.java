package Demo1;
//用抽象类实现电脑的开关及部分功能
//调用部分
public class Test {

    public static void turncom(computer computer){
        computer.turn();
        computer.turnon();
        if(computer instanceof Keyboard){
            Keyboard keyboard=(Keyboard) computer;
            keyboard.click();
        }
        else if(computer instanceof draw) {
            draw draw=(draw)computer;
            draw.draw();
        }
        computer.turnoff();
        computer.off();
    }
    public static void main(String[] args) {
        turncom(new Keyboard());
        turncom(new draw());

    }

}
