package Demo4;

import java.util.Arrays;

//Test这个类的默认父类时object
class Money implements Cloneable{
    public double money=12.5;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
class Student implements Cloneable{//有Cloneable是使其具备可以被克隆的条件
    public int age=10;

    public String name;
    public Student(int age,String name) {
        this.age = age;
        this.name=name;
    }

    Money m=new Money();

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Student tmp=(Student) super.clone();//tmp想为交换值
        //现在上面已经克隆完了
        tmp.m=(Money)this.m.clone();
        return tmp;

    }
}
public class Test  {
    public static void main(String[] args)throws CloneNotSupportedException {
        Student student1=new Student(18,"huahua");
        Student student2=(Student) student1.clone();//实现克隆.
        //浅拷贝
        System.out.println(student1.m.money);
        System.out.println(student2.m.money);
        System.out.println("============");
       // System.out.println(student2);
        student2.m.money=100;
        System.out.println(student1.m.money);
        System.out.println(student2.m.money);
        System.out.println("============");
        //深拷贝
        Student student3=(Student) student1.clone();//实现克隆.
        System.out.println(student1.m.money);
        System.out.println(student3.m.money);
        System.out.println("===========");
        student3.m.money=25;
        System.out.println(student1.m.money);
        System.out.println(student3.m.money);

    }

}
