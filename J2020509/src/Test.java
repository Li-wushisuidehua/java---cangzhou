import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int []nums={0,2,3,0,0,12};
        Sort(nums);
        System.out.println(Arrays.toString(nums));
    }
    public static void Sort(int []nums){
        int cur=0;
        int dest;
        if(nums[cur]==0){
            dest=-1;
        }else{
            dest=0;
        }
        for (int i = cur+1; i < nums.length; i++) {
            if(nums[i]!=0){
                Swap(nums,dest+1,i);
                dest++;
            }
        }
    }
    public static void Swap(int [] nums,int i,int j){
        int tmp=nums[i];
        nums[i]= nums[j];
        nums[j]=tmp;
    }
}
