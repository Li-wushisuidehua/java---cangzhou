package Day03;

/**
 * 1.创建dp表
 * 2.初始化
 * 3. 填表
 * 4. 返回值
 */
public class Demo2 {
    //此代码运行时间为4ms
    public static int minPathSum1(int[][] grid) {
        //获取行数和列数
        int m=grid.length;
        int n=grid[0].length;

        //创建dp表
        int [][]dp=new int[m+1][n+1];

        //初始化
        for (int i = 0; i <=n; i++) {
            dp[0][i]=Integer.MAX_VALUE;
        }
        for (int i = 1; i <=m; i++) {
           dp[i][0]=dp[i][n]=Integer.MAX_VALUE;
        }
         dp[0][1]=0;
        //填表
        for (int i = 1; i <=m ; i++) {
            for (int j = 1; j <=n ; j++) {
                dp[i][j]=Math.min(dp[i-1][j],dp[i][j-1])+grid[i-1][j-1];
            }
        }

        //返回值
        return dp[m][n];

    }
    //优化代码的运行时间为2ms
    public static int minPathSum(int[][] grid) {
        // 获取行数和列数
        int m = grid.length;
        int n = grid[0].length;

        // 创建dp表
        int[][] dp = new int[m + 1][n + 1];

        // 初始化
        for (int i = 0; i <= n; i++)  dp[0][i] = Integer.MAX_VALUE;

        for (int i = 0; i <= m; i++)  dp[i][0] = Integer.MAX_VALUE;//将这里优化了一下

        dp[0][1] =dp[1][0]= 0;//将这里优化了一下
        // 填表
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
            }
        }

        // 返回值
        return dp[m][n];
    }

    public static void main(String[] args) {
        int [][]grid=new int[][]{{1,2,3},{4,5,6}};
        System.out.println(minPathSum(grid));
    }
}
