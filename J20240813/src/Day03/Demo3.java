package Day03;

//地下城游戏
public class Demo3 {
    public static void main(String[] args) {
        int [][]arr=new int[][]{{0,0}};
        System.out.println(calculateMinimumHP(arr));

    }
    public static int calculateMinimumHP(int[][] dungeon) {
        int m=dungeon.length;
        int n=dungeon[0].length;
        //创建dp表
        int [][]dp=new int[m+1][n+1];

        //初始化dp表
        for (int i = 0; i <=n; i++) dp[m][i]=Integer.MAX_VALUE;
        for (int j=0;j<=m;j++) dp[j][n]=Integer.MAX_VALUE;
        dp[m][n-1]=dp[m-1][n]=1;

        //填表
        for (int i = m-1; i >=0 ; i--) {
            for (int j = n-1; j >=0 ; j--) {
                dp[i][j]=Math.min(dp[i][j+1],dp[i+1][j])-dungeon[i][j];
                dp[i][j]=Math.max(1,dp[i][j]);

            }
        }
         return dp[0][0];
    }
}
