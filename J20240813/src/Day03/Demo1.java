package Day03;

/**
 * 1. 创建dp表
 * 2. 初始化
 * 3. 填表
 * 4. 返回值
 */
public class Demo1 {
    public static void main(String[] args) {
        int [][]arr=new int[][]{{-19}};
        minFallingPathSum(arr);
        System.out.println(minFallingPathSum(arr));
    }
    //原始代码
    public static int minFallingPathSum1(int[][] matrix) {//为方阵
        int min=Integer.MAX_VALUE;
        int m=matrix.length;
        int n=matrix[0].length;
        //创建dp表+初始化
        int [][] dp=new int[m+1][n+2];

        //填表
        for (int i = 1; i <=m ; i++) {
            for(int j=1;j<=n;j++){
                int tmp=0;
                if(j==1){
                    tmp=Math.min(dp[i-1][j],dp[i-1][j+1]);
                }else if(j==n) {
                    tmp=Math.min(dp[i-1][j],dp[i-1][j-1]);
                }else{
                        //获取可能来源的三个有效值中的最小值
                        tmp=Math.min(dp[i-1][j],dp[i-1][j-1]);
                        tmp=Math.min(tmp,dp[i-1][j+1]);
                }
                dp[i][j]=tmp+matrix[i-1][j-1];

                if(i==m){
                    min=Math.min(dp[i][j],min);
                }
            }
        }
         return min;
    }
    //优化修改后的代码
    public static int minFallingPathSum2(int[][] matrix) {//为方阵
        int min=Integer.MAX_VALUE;
        int m=matrix.length;

        //创建dp表
        int [][] dp=new int[m+1][m+2];

        //初始化
        for (int i = 1; i <=m; i++) dp[i][0]=dp[i][m+1]=Integer.MAX_VALUE;

        //填表
        for (int i = 1; i <=m ; i++) {
            for(int j=1;j<=m;j++){
                //获取可能来源的三个有效值中的最小值
                dp[i][j]=Math.min(dp[i-1][j],Math.min(dp[i-1][j-1],dp[i-1][j+1]))+matrix[i-1][j-1];

                if(i==m){
                    min=Math.min(dp[i][j],min);
                }
            }
        }
        return min;
    }

    //再次优化后的代码
    public static int minFallingPathSum(int[][] matrix) {//为方阵
        int min=Integer.MAX_VALUE;
        int m=matrix.length;

        //创建dp表
        int [][] dp=new int[m+1][m+2];

        //初始化
        for (int i = 1; i <=m; i++) dp[i][0]=dp[i][m+1]=Integer.MAX_VALUE;//将两边的值设置为最大的整数值

        //填表
        for (int i = 1; i <=m ; i++) {
            for(int j=1;j<=m;j++){
                //获取可能来源的三个有效值中的最小值
                dp[i][j]=Math.min(dp[i-1][j],Math.min(dp[i-1][j-1],dp[i-1][j+1]))+matrix[i-1][j-1];//求三个数中的最小值
            }
        }
        for(int i=1;i<=m;i++){
            min=Math.min(min,dp[m][i]);
        }
        return min;
    }
    //由此得出判断语句真的很耗时
}
