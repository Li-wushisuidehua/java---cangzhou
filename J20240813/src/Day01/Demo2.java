package Day01;
/**
 * 1. 创建dp表
 * 2. 初始化
 * 3. 填表
 * 4. 返回值
 */

import java.util.Scanner;

public class Demo2 {
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        String str=in.nextLine();
        char[] ch=str.toCharArray();
        int len=str.length();

        //创建dp表
        int []dp=new int[len];

        //dp表的初始化
        if(ch[0]-'0'!=0 && ch[0]-'0'>=1 && ch[0]-'0'<=9) dp[0]=1;
        else {
            System.out.println(0);
            return ;
        }

        if(ch[1]-'0'>=1 && ch[1]-'0'<=9 && (ch[0]-'0')*10+(ch[1]-'0')<=26) dp[1]=2;
        else dp[1]=1;


        //给dp表的各项赋值
        for (int i = 2; i < len; i++) {
            int p=ch[i]-'0';
            int q=ch[i-1]-'0';
            if(p>=1 && p<=9) dp[i]+=dp[i-1];
            if(q*10+p>=10 && q*10+p<=26) dp[i]+=dp[i-2];

        }
        System.out.println(dp[len-1]);
    }

    //将上述代码优化后的代码为
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        char[] ch = str.toCharArray();
        int len = str.length();

        //创建dp表
        int []dp = new int[len+1];

        //dp表的初始化
        dp[0]=1;//保证后续填表是正确的，一般这里为 0，但是此处这里必须为 1
        if (ch[0]!='0') dp[1] = 1;
        else {
            System.out.println(0);
            return ;
        }

        //给dp表的各项赋值
        for (int i = 2; i <=len; i++) {
            int p = ch[i-1] - '0';
            int q = ch[i-2] - '0';
            if (p >= 1 && p <= 9) dp[i] += dp[i - 1];
            if (q*10+p>=10 && q*10+p<=26) dp[i] += dp[i - 2];

        }
        System.out.println(dp[len]);
    }
}
