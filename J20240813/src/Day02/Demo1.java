package Day02;

/**
 * 1. 创建dp表
 * 2. 初始化值
 * 3. 填表
 * 4. 返回值
 */

//返回从左上角到达右下角共总走的路径
public class Demo1 {
    public static int uniquePaths(int m, int n) {

        //创建dp表
        int [][] dp=new int[m+1][n+1];//这里创建的为二维的dp表所以空间复杂度也是m*n的

        //初始化dp表
        dp[0][1]=1;

        //填表
        for(int i=1;i<=m;i++){//两层for循环，时间复杂度是m*n的
            for (int j = 1; j <=n; j++) {
                dp[i][j]=dp[i-1][j]+dp[i][j-1];
            }
        }

        return dp[m][n];
    }

    public static void main(String[] args) {
        System.out.println(uniquePaths(3, 2));
    }
}
