package Day02;

/**
 * 1. 创建dp表
 * 2. dp表的初始化
 * 3. 填表
 * 4. 返回值
 */
public class Demo3 {
    public static int maxValue (int[][] grid) {
        int m=grid.length;//获取行数
        int n=grid[0].length;//获取列数
        //创建dp表＋初始化
        int [][]dp=new int[m+1][n+1];

        //填表
        for (int i = 1; i <=m; i++)
            for (int j = 1; j <=n; j++)
                dp[i][j]=Math.max(dp[i-1][j],dp[i][j-1])+grid[i-1][j-1];
        return dp[m][n];
    }

    public static void main(String[] args) {
        int [][]arr=new int[][]{{1,3,1},{1,5,1},{4,2,1}};
        System.out.println(maxValue(arr));
    }
}
