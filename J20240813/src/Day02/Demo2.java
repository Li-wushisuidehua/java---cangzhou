package Day02;

/**
 * 1. 创建dp表
 * 2. 初始化
 * 3. 填表
 * 4. 返回值
 */
public class Demo2 {
    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m=obstacleGrid.length;//求行数
        int n=obstacleGrid[0].length;//求列数
        //创建dp表
        int [][] dp=new int[m+1][n+1];

        //初始化dp表
        dp[0][1]=1;

        //最开始的代码
        /**
         *         for (int i = 1; i <=m ; i++) {
         *             for (int j = 1; j <=n; j++) {
         *                 if(obstacleGrid[i-1][j-1]!=1){
         *                     if(dp[i-1][j]==-1){
         *                         dp[i][j]+=dp[i][j-1];
         *                     }else if(dp[i][j-1]==-1){
         *                         dp[i][j]+=dp[i-1][j];
         *                     }else{
         *                         dp[i][j]=dp[i][j-1]+dp[i-1][j];
         *                     }
         *                 }else{
         *                     dp[i][j]=0;
         *                 }
         *             }
         *         }
         */

        //代码可以优化
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (obstacleGrid[i - 1][j - 1] != 1) {
                    dp[i][j] = dp[i][j - 1] + dp[i - 1][j];
                }
            }
        }
        //返回值
        return dp[m][n];

    }

    public static void main(String[] args) {
      int [][]arr=new int[][]{{0,0},{0,1}};
        System.out.println(uniquePathsWithObstacles(arr));
    }
}
