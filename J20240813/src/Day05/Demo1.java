package Day05;

/**
 * 1. 创建dp表
 * 2. 初始化
 * 3. 填表
 * 4. 返回值
 */
public class Demo1 {
    public static void main(String[] args) {
        int [][]c=new int[][]{{17,2,17}};
        System.out.println(minCost(c));
    }
    public static int minCost(int[][] costs) {
        int n=costs.length;//获取行数，列数一直是为3列

        //创建dp表+初始化
        int [][] dp=new int[n+1][4];

        for (int i = 1; i <n+1 ; i++) {
            for (int j = 1; j <4; j++) {
                dp[i][1]=Math.min(dp[i-1][2],dp[i-1][3])+costs[i-1][0];
                dp[i][2]=Math.min(dp[i-1][1],dp[i-1][3])+costs[i-1][1];
                dp[i][3]=Math.min(dp[i-1][2],dp[i-1][1])+costs[i-1][2];

            }
        }

        //返回值
        return Math.min(dp[n][1],Math.min(dp[n][2],dp[n][3]));
    }
}
