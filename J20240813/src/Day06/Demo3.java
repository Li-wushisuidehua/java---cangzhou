package Day06;

public class Demo3 {
    public static int maxProfit(int[] prices) {
        int n=prices.length;//获取列数

        //创建dp表
        int [][] dp=new int[n][3];

        //初始化
        dp[0][0]=-prices[0]; dp[0][1]=0; dp[0][2]=0;//可交易(选择买或者不买)

        //填表
        for (int i = 1; i <n; i++) {
            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][2]-prices[i]);//买入
            dp[i][1]=dp[i-1][0]+prices[i];//卖出=冷冻期的值
            dp[i][2]=Math.max(dp[i-1][2],dp[i-1][1]);//max(之前一直持续卖出的值大，还是新的冷冻期值大)

        }
        return Math.max(dp[n-1][1],dp[n-1][2]);

    }

    public static void main(String[] args) {

        int []arr=new int[]{1,3,7,5,10,3};
        System.out.println(maxProfit(arr));
    }
}
