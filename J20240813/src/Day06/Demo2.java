package Day06;

//买卖股票的最佳时机2
public class Demo2 {
    public static int maxProfit(int[] prices) {
        int n=prices.length;

        //创建dp表
        int [][]dp=new int[n][2];

        //初始化
        dp[0][0]=-prices[0];

        //填表
        for (int i = 1; i <n; i++) {
            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][1]-prices[i]);
            dp[i][1]=Math.max(dp[i-1][1],dp[i-1][0]+prices[i]);

        }
        return dp[n-1][1];

    }

    public static void main(String[] args) {
int []arr=new int[]{7,1,5,3,6,4};
        System.out.println(maxProfit(arr));
    }
}
