package Day06;

public class Demo4 {
    public static int maxProfit(int[] prices, int fee) {
        int n=prices.length;//获取列数

        //创建dp表
        int [][] dp=new int[n][3];

        //初始化
        dp[0][0]=-prices[0]; dp[0][1]=0; //可交易(选择买或者不买)

        //填表
        for (int i = 1; i <n; i++) {
            dp[i][1]=Math.max(dp[i-1][1],dp[i-1][0]+prices[i]-fee);//max(之前一直持续卖出的值大，还是新的冷冻期值大)

            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][1]-prices[i]);//买入
        }
        return dp[n-1][1];
    }

    public static void main(String[] args) {
        int []arr=new int[]{1,3,2,8,4,9};
        System.out.println(maxProfit(arr, 2));
    }
}
