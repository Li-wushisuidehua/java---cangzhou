package Day04;

/**
 * 1. 创建dp表
 * 2. 初始化
 * 3. 填表
 * 4. 返回值
 */
public class Demo1 {
    public static void main(String[] args) {
        int []arr=new int[]{1,2,3,1};
        System.out.println(massage(arr));
    }
    public static int massage(int[] nums) {
        int n=nums.length;
        if(n==0) return 0;//处理边界情况

        //创建dp表
        int []Fdp=new int[n];
        int []Gdp=new int[n];

        //初始化dp表
        Fdp[0]=nums[0];
      //  Gdp[0]=0;//这里其实可以不用写这个，因为原始创建一个表的时候就是这个样子

        //填表
        for (int i = 1; i < n; i++) {
            Fdp[i]=Gdp[i-1]+nums[i];
            Gdp[i]=Math.max(Fdp[i-1],Gdp[i-1]);
        }
        return Math.max(Fdp[n-1],Gdp[n-1]);
    }
}
