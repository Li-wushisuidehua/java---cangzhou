package Day04;
public class Demo3 {
    public static void main(String[] args) {
        int []arr=new int[]{2,2,3,3,3,4};
        System.out.println(deleteAndEarn(arr));
    }
    public static int deleteAndEarn(int[] nums) {
        int lenE=10001;
        int []arr=new int[lenE];
        for (int x:nums) {
            arr[x]+=x;
        }
        //创建dp表
        int []Fdp=new int[lenE];
        int []Gdp=new int[lenE];

        //初始化
        Fdp[0]=arr[0];
        for (int i = 1; i < lenE; i++) {
            Fdp[i]=Gdp[i-1]+arr[i];
            Gdp[i]=Math.max(Fdp[i-1],Gdp[i-1]);
        }
        return Math.max(Fdp[9999],Gdp[9999]);

    }
}
