package Day04;
//打家劫舍2

/**
 * 1. 创建dp表
 * 2. 初始化
 * 3. 填表
 * 4. 返回值
 */
public class Demo2 {
    public static void main(String[] args) {

    }
    //有两种情况把这两种情况封装为一个函数
    public int rob(int[] nums) {
        int n = nums.length;
        if (n == 1)
            return nums[0];
        return Math.max(nums[0] + rob1(nums, 2, n - 2), rob1(nums, 1, n - 1));
    }

    public int rob1(int[] nums, int x, int y) {
        if(x>y) return 0;
        int n = y - x + 1;

        //创建dp表
        int[] Fdp = new int[n];
        int[] Gdp = new int[n];

        //初始化
        Fdp[0] = nums[x];

        //填表
        for (int i = x+1; i <=y; i++) {
            Fdp[i-x] = Gdp[i - x-1] + nums[i];
            Gdp[i-x] = Math.max(Fdp[i - x-1], Gdp[i - x-1]);
        }

        //返回值
        return Math.max(Fdp[n - 1], Gdp[n - 1]);
    }
}
