//输出字符串中最长的数字字符串和它的长度，中间用逗号间隔。如果有相同长度的串，则要一块儿输出（中间不要输出空格）。

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//public class Demo4 {
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        while (in.hasNextLine()) {
//            String s = in.nextLine();
//            List<String> list = new ArrayList();
//            int max = 0;
//            int[] dp = new int[s.length() + 1];
//            for (int i = 1; i <= s.length(); i++) {
//                if (Character.isDigit(s.charAt(i - 1))) {
//                    dp[i] = dp[i - 1] + 1;
//                    if (dp[i] == max) {
//                        list.add(s.substring(i - max, i));
//                    } else if (dp[i] > max) {
//                        max = dp[i];
//                        list.clear();
//                        list.add(s.substring(i - max, i));
//                    }
//                }
//            }
//            list.forEach(k -> System.out.print(k));
//            System.out.println("," + max);
//        }
//    }
//}
import java.util.*;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Demo4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNext()) { // 注意 while 处理多个 case
            // 接收输入的字符串
            String str = in.nextLine();
            // 加上一个字母防止不统计最后的数字串
            char[] chs = (str+"a").toCharArray();
            // 最大长度
            int maxLength = 0;
            // 临时字符串中数字串的大小
            int tempSize = 0;
            // 创建map存放结果，key为数字串的长度， value是具体字符串
            Map<Integer, String> map = new HashMap<>();
            for(int i = 0; i< chs.length; i++) {
                if(Character.isDigit(chs[i])) {
                    // 如果是数字，直接tempSize+1即可
                    tempSize ++;
                } else {
                    // 不是数字，需要判断tempSize是否大于等于最大数字串长度
                    if(maxLength == tempSize) {
                        // 相等的话，需要将i-tempSize 到i的数字串添加进结果中
                        map.put(tempSize, map.getOrDefault(tempSize, "") + str.substring(i-tempSize, i));
                    } else if(maxLength < tempSize) {
                        // tempSize大于maxLength,清空map，同时把该数字串加入结果map中
                        map.clear();//这个清空的比自己删除的要方便的多
                        map.put(tempSize, str.substring(i-tempSize, i));
                        maxLength = tempSize;
                    }
                    // 不是字符串，需要将临时变量置为0
                    tempSize = 0;
                }
            }
            // 最后直接输出结果
            System.out.println(map.getOrDefault(maxLength, "") + "," + maxLength);
        }
    }
    //12ab34hhhh55m6
    //这里注意：String.substring(int beginIndex,int endIndex),返回字符串的一个字符串，从 beginIndex(下标包括) 到 endIndex(下标不包括)
   //文中Character.isDigit(char ch)此方法用于判断传入的字符参数是否为数字字符

    //程序中 map集合中方法的总结
    //map.put(……),将需要放入的值，放入map中
    //map.getOrDefault(Object key,V DefaultValue),这里的意思是调用getOrDefault方法，返回key对应的value值，key不存在，返回默认值，默认值是传入的第二个参数对应文中的“”
    //map.clear(),这个方法用于清空map中已经存有的值




}