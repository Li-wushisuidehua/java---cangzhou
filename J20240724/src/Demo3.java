import java.util.*;

public class Demo3 {
    //在字符串中找出连续最长的数字串
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
            String str = in.nextLine();
            int i = 0, j = 0, begin = 0, maxLen = 0;
            while (i < str.length()) {
                if (str.charAt(i) <= '9' && str.charAt(i) >= '0') {
                    j = i;
                    while (j<str.length() && str.charAt(j) <= '9' && str.charAt(j) >= '0') j++;
                    if (j - i > maxLen) {
                        maxLen=j-i;
                        begin = i;
                        i=j-1;//这个时候自己从i这里开始走便可以了
                    }
                }
                i++;

            }
        for (int k = begin; k <begin+maxLen ; k++) {
            System.out.print(str.charAt(k));
        }
    }

}
