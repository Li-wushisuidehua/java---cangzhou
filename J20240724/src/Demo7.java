import java.util.Scanner;

public class Demo7 {
    //解释 hasNext() 和 hasNextLine() 方法的区别

    //一、hasNext()
    //hasNext()方法用于检查输入流中是否还有下一个标记（token）。这里的“标记”是指以空格、制表符或换行符为分隔符的一个单词或数据项。
    //如果输入流中还有下一个标记，则返回true；否则返回false。
    /*
     public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            System.out.println(in.next());
        } //输入 aaa bdc ii
        // 输出 aaa(换行) bdc(换行) ii
    }
     */

    //二、hasNextLine()
    //hasNextLine()方法用于检查输入流中是否还有下一行。
    //如果输入流中还有下一行（包括空行），则返回true；否则返回false。
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        while(in.hasNext()){
            System.out.println(in.nextLine());
        }//输入 aaaaa    bbbbb（换行）bbbbb    vvv
        //输出 aaaaa    bbbbb（换行）bbbbb    vvv

    }



}
