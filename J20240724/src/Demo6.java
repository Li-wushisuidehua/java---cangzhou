import java.util.Arrays;

public class Demo6 {
    //笔试强训Day09-3 过啦！！超棒嘿嘿
    public static void main(String[] args) {
        int []arr=new int[]{0,0,0,0,1};
        System.out.println(IsContinuous(arr));
    }
    public static boolean IsContinuous (int[] numbers) {
        Arrays.sort(numbers);
        int n=0,sum=0;
        for (int i = 0; i < 5; i++) {
            if(numbers[i]==0){
                n++;
            }
            if(i!=4 && numbers[i]!=0){
                if(numbers[i]==numbers[i+1]) return false;
                sum+=(numbers[i+1]-numbers[i]-1);
            }
        }
       return sum<=n;
    }
}
