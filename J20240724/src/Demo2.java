import java.util.Arrays;
import java.util.Scanner;

//笔试强训Day07-3 拼三角
//构成三角形的充分必要条件是： 三角形任意之和大于第三边！！！！
public class Demo2 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        long[][] arr=new long[n][6];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 6; j++) {
                arr[i][j]=in.nextLong();
            }
            Arrays.sort(arr[i]);
        }
        for (int i = 0; i < n; i++) { //这里只需要考虑的是三角形任意两边之和大于第三边
                if(arr[i][0]+arr[i][1]>arr[i][2] && arr[i][3]+arr[i][4]>arr[i][5] ||
                        arr[i][0]+arr[i][2]>arr[i][3] && arr[i][1]+arr[i][4]>arr[i][5] ||
                        arr[i][0]+arr[i][3]>arr[i][4] && arr[i][1]+arr[i][2]>arr[i][5]||
                        arr[i][0]+arr[i][4]>arr[i][5] && arr[i][1]+arr[i][2]>arr[i][3]){
                    System.out.println("Yes");
                }
                else System.out.println("No");
        }

    }
}
