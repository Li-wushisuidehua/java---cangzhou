import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.io.*;

public class Demo8 {
    //快速输出
   public static PrintWriter in=new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
  public static   Read read=new Read();
    public static void main(String[] args) {

        in.println("hhh");
    }
}
 class Read{
    //裁剪
     StringTokenizer st=new StringTokenizer(" ");
     //创建缓冲的地方
     BufferedReader bf =new BufferedReader(new InputStreamReader(System.in));

     String next() throws IOException{
         while(!st.hasMoreTokens()){//看成in.hasNext()这类
             st=new StringTokenizer(bf.readLine());
         }
         return st.nextToken();
     }
     String nextLine() throws Exception{
         return bf.readLine();
     }
     int nextInt() throws Exception{
         return Integer.parseInt(next());
     }
     long nextLong() throws Exception{
         return Long.parseLong(next());
     }
     double nextDouble() throws Exception{
         return Double.parseDouble(next());
     }
 }
