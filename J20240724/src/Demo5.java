import java.util.Scanner;
public class Demo5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        String str = String.valueOf(n);
        StringBuffer sbf = new StringBuffer(str);
        sbf.reverse();
        String str2 = sbf.toString();
        sbf=new StringBuffer();
        int len = str2.length();
        int k = 0;
        for (int i = 0; i < len; i++) {
            sbf.append(str2.charAt(i));
            k++;
            if (k == 3 && i!=len-1) {
                sbf.append(",");
                k = 0;
            }
        }
        System.out.println(sbf.reverse().toString());
    }
}
