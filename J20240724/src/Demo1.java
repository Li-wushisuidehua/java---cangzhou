import javax.swing.*;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Demo1 {
    //判断两个字符串经过一系列转化，能否一样
    //如果说两个字符串的所有字符是相等的对应的单个字符也是相等的，那么在一个数组中进行加一减一的操作最终结果一定是等于0的。
    public static void main(String[] args) {
        String str="abc";
        String str1="bcc";
        System.out.println(CheckString2(str, str1));
    }
    public static boolean CheckString (String s,String t){
        int m=s.length();
        int n=t.length();
        int tot=0;
        if(m!=n) return false;
        int [] cnts=new int[256];
        for (int i = 0; i < n; i++) {
            if(++cnts[s.charAt(i)]==1) tot--;
            if(--cnts[t.charAt(i)]==0) tot++;
        }
        return tot==0;
    }


   //方法二：更加简单的容易操作的方法

    public static boolean CheckString2(String s1,String s2){
        return s1.chars().boxed().collect(Collectors.groupingBy(Function.identity(),Collectors.counting())).equals(s2.chars().boxed().
    collect(Collectors.groupingBy(Function.identity(),Collectors.counting())));
    }
    //chars()方法，会遍历字符串中的每个字符，并且生成一个IntStream，其中包含每个字符的Unicode码点。
    //chars().boxed()方法，是将上述生成的IntStream（原始类型int的流）转化为对象流，且Collectors.counting()和 Collectors.groupingBy()是为对象流设计的
    //他们期望的是Object类型的流，而不是原始类型。boexd()方法是将IntStream中的每个int值转换为一个Integer对象，从而生成Stream<Integer>。这里如果
    //没有写boxed()，但是在collectors的框架中会自动的将其隐式的转化。
    //collect(....)开始处理Stream<Integer>中的元素
    //Collectors.groupingBy(...) 对上述元素进行分组
    //Function.identity() 映射键值器
    //Collectors.counting() //计算分组的每个数量为多少


}
