class MysingleListNode {
    public static class ListNode {
        public int val;
        public ListNode next;

        //每次实例化时为val赋值;
        public ListNode(int val) {
            this.val = val;
        }
    }

    //链头
    public ListNode head;

    //链表的创建
    public void creatlist() {
        ListNode node1 = new ListNode(1);//其中未被实例化的next此时通通会被设定为0;
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        ListNode node6 = new ListNode(6);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;//此时虽然node6中的next没有被设定值，但是系统默认为其设定的初始值为null;
        head = node1;//链头为node1;
    }

    //链表的打印
    public void visitlist() {
        ListNode cur = head;
        System.out.println("链表的打印如下：");
        while (cur != null) {
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }

    //向链头插入元素,由于是向链头插入的元素，那么先插入的输出时在后面，后插入的输出时在前面
    //head在这里已经是被生成好了，后面需要用时直接用head就行。
    public void addFirst(int val) {
        ListNode node = new ListNode(val);
        if (head == null) {
            head = node;
            return;
        }
        node.next = head;
        head = node;
    }   //输出结果为:7 6 5 4 3

    //向链表末尾添加元素
    public void addLast(int val) {
        ListNode node = new ListNode(val);
        if(head==null){
            head=node;
            return ;
        }
        ListNode cur = head;
        while (cur.next != null) {
            cur = cur.next;
        }
        cur.next = node;
    }//输出结果应该为7 6 5 4 3 5 6 7

    //任意位置插入元素
    public void AddList(int index, int val) {
        try{
           checkIndex(index);
        }catch(NullPointerException e){
            e.printStackTrace();
        }
//        if (head == null) {
//            head = node;
//            return ;
//        }
        if(index==0){
            addFirst(val);
            return ;
        }
        if(index==size()){
            addLast(val);
            return ;
        }
        //找到要插入的前一个位置
        ListNode cur=findIndex(index);
        //开始插入
        ListNode node = new ListNode(val);
        node.next=cur.next;
        cur.next=node;
    }

    public ListNode findIndex(int index) {
        int count=0;
        ListNode cur = head;
        while (index - 1 !=count) {
            cur = cur.next;
            count++;
        }
        return cur;
    }

    public int size() {
        int count = 0;
        if (head == null) {
           return count;
        }
        ListNode cur = head;
        while (cur != null) {
            count++;
            cur = cur.next;
        }
        return count;
    }
    public void checkIndex(int index){
        if (index < 0 || index > size()) {
            throw new NullPointerException("位置异常");
        }
    }
    //查找关键字key是否包含在单链表中
    public boolean contians(int val){
        if(head==null){
            System.out.println("不存在！单链表为空！");
        }
        ListNode cur=new ListNode(val);
        while(cur!=null){
            if(cur.val==val){
                return true;
            }
            cur=cur.next;

        }
        return false;
    }
    //删除链表中的某个元素
    public void remove(int val){
        if(head==null){
            System.out.println("单链表为空！");
            return ;
        }
        if(head.val==val){
            head=head.next;
        }
        ListNode cur=head;
        while(cur.next!=null){
            if(cur.next.val==val){
                ListNode ret=cur.next;
                cur.next=ret.next;//这里ret.next和直接写cur.next.next是一样的;
            }
            cur=cur.next;
        }

    }
    //删除单链表中所有的keyword
    //方法一:
    public void removeallword(int val){
//        if(head==null){
//            System.out.println("单链表为空！");
//            return ;
//        }
//        if(head.val==val){
//            head=head.next;
//        }
//        ListNode cur=head;
//        while(cur.next!=null){
//            if(cur.next.val==val){
//                ListNode ret=cur.next;
//                cur.next=ret.next;//这里ret.next和直接写cur.next.next是一样的;
//            }
//            cur=cur.next;
//            if(cur==null){
//                return ;
//            }
//        }
//        ListNode cur=head;
//        while(cur.next!=null){
//            if(cur.next.val==val){
//                ListNode ret=cur.next;
//                cur.next=ret.next;//这里ret.next和直接写cur.next.next是一样的;
//            }
//            cur=cur.next;
//            if(cur==null){
//                break;
//            }
//        }
//        if(head.val==val){
//            head=head.next;
//        }


//        if (head == null) {
//            return ;
//        }
//        ListNode cur = head;
//        while (cur.next != null) {
//            if (cur.next.val == val) {
//                ListNode ret = cur.next;
//                cur.next = ret.next;// 这里ret.next和直接写cur.next.next是一样的;
//
//            }
//            cur = cur.next;
//        }
//        cur = cur.next;
//        if (head.val == val) {
//            head = head.next;
//        }



        if (head == null) {
            return ;
        }
        ListNode cur = head;
        //===================
        while (cur.next != null) {
            if (cur.next.val == val) {
                ListNode ret = cur.next;
                cur.next = ret.next;// 这里ret.next和直接写cur.next.next是一样的;

            }
            //当是连续要删除的时候,会一直删除了往后走,当其中不为想要删除的元素后,会直接跳过这个元素,去下一个元素;
        //=================
            else{
                cur=cur.next;
            }
        }
        cur = cur.next;
        if (head.val == val) {
            head = head.next;
        }

    }



    //方法二:
    //快慢指针法:
    public void Removeallword(int val){
        if(head==null){
            return ;
        }
        ListNode prev=head;
        ListNode cur=head.next;
        while(cur!=null){
            if(cur.val==val){
                prev.next=cur.next;
                cur=cur.next;
            }
            else {
                prev=cur;
                cur=cur.next;
            }
        }
        if(head.val==val){
            head=head.next;//此时后面所有已经为null了;
        }

    }
    //clear方法的实现
    //clear方法的主要内容是清空链表
    //方法一:
    public void clear(){
        head=null;//只需要使用这一个便可以了;
    }
    //方法二:
    public void cleartwo(){
        if(head==null){
            return ;
        }
        ListNode cur=head;
        while(cur!=null){
            ListNode ret=cur.next;
           cur.next=null;
           cur=ret;
        }
        head=head.next;
    }
   //逆序输出单链表
    public ListNode reverseList() {
        if(head==null){
            return head;
        }
        ListNode cur = head.next;//判断head是否为空；
        ListNode curN ;
        head.next = null;
        while (cur != null) {
            curN=cur.next;
            cur.next = head;
            head = cur;
            cur = curN;
        }
        return head;
    }
//获取链表的中间节点
    public ListNode middleNode(ListNode head) {
        if(head==null){
            return head;
        }
        ListNode fast=head;
        ListNode slow=head;
        while(fast!=null && fast.next!=null){
            slow=slow.next;
            fast=fast.next.next;
        }
        return slow;

       }
       //获取单链表倒数第K个节点
    public ListNode Getk(int k){
        ListNode cur=head;
        if(cur==null){
            return cur;
        }
        int count=0;
        while(cur!=null){
            count++;
            cur=cur.next;

        }
        cur=head;

        while(count-k>0){
            cur=cur.next;
            count--;
        }
        return cur;
    }
}


