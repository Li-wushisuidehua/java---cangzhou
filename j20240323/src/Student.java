public class Student implements Cloneable{
    public String name;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int age;

    @Override
    public String toString() {
        System.out.println("姓名："+this.name+"  年龄"+this.age);
        return null;
    }
//    @Override
//    public String toString() {
//        return "Student{" +
//                "name='" + name + '\'' +
//                ", age=" + age +
//                '}';
//    }
    //    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }

    public static void main(String[] args) throws CloneNotSupportedException{
        Student student=new Student("小美",18);
      Student student1=(Student)student.clone();
      System.out.println(student);
    }
}
//class Tets{
//    public static void main(String[] args) throws CloneNotSupportedException{
//        Student student=new Student("小美",18);
//        Student student1=(Student)student.clone();
//    }
//
//
//}