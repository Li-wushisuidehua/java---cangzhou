import java.util.Scanner;
//数组中两个字符串的最小距离
public class Dmeo10 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.nextLine();
        String str1 = in.next();
        String str2 = in.next();
        if (str1 == null && str2 == null) {
            System.out.println(-1);
        }
        int flag1 = -1;
        int flag2 = -1;
        int sub = 0;
        int min = -1;
        String[] strings = new String[n];
        for (int i = 0; i < n; i++) {
            strings[i] = in.nextLine();
            in.nextLine();
            if (strings[i].equals(str1)) {
                flag1 = i;//4
            }
            if (strings[i].equals(str2)) {
                flag2 = i;//3
            }
            sub = Math.abs(flag1 - flag2);
//            System.out.println(sub);
            if(min==(-1)){
                min=sub;
            }
           if (min > sub) {
                    min = sub;
                }
        }
        if (flag1 == -1 || flag2 == -1) {
            System.out.println(-1);
        } else {
            System.out.println(min);
        }

    }
}
