public class Demo4 {
    public static void main(String[] args) {
        Integer var1=new Integer(1);
        Integer var2=var1;
       doSomething(var2);
        System.out.println(var1.intValue());
        System.out.println(var1==var2);
    }
    public static void doSomething(Integer integer){//在这里创建了一个局部变量
        integer=new Integer(2);
        //这里integer是一个局部变量，它的变化不会影响到 main函数中的var2
    }
}
