public class Demo5 {
    public static int addDigits(int num) {
        int sum=0;
        if(num>=0 && num<10){
            return num;
        }else{
            while(num!=0){
                int n=num%10;
                sum+=n;
                num=num/10;
            }
          return  addDigits(sum);
        }
//时间复杂度为O(longN)
    }
    //这是一种不需要使用循环或者递归的做法
    public static int addDigits2(int num){
        if(num==0){
            return num;
        }
        if(num%9==0){
            return 9;
        }else{
            return num%9;
        }
    }

    public static void main(String[] args) {
        System.out.println(addDigits(0));
    }
}
