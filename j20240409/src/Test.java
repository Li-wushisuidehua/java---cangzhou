import java.util.*;

/**
 * 双端队列如何实现队列和栈
 */
class Test2{
    public static int j=20;
}
public class Test {
    public static int i;
    public static void play(){
        System.out.println("正在玩耍");
    }
    public static void main1(String[] args) {
        Test2 test2=new Test2();
       Test2.j=10;//不同类中的静态变量调用时需要通过类名.方法/变量进行调用
       test2.j=10;//可以使用实例的对象对其进行调用，但是被static修饰的变量最好用类名调用
        Test.i=10;
        i=12;//在同一个类中定义的静态变量可以直接使用，不需要通过类名调用
        System.out.println(i);
        play();
    }
    //双端队列
    public static void main3(String[] args) {
        Queue<Integer> stack=new ArrayDeque<>();//双端队列的线性实现
        Queue<Integer> queue=new LinkedList<>();//双端队列的链式实现
        stack.add(6);//进入队列
        stack.offer(7);
        System.out.println(stack.poll());
        System.out.println(stack.poll());
        //上面两个输出语句输出的是6，7正序输出，实现了队列
        queue.add(6);//add方法添加在末尾
        queue.add(7);
        System.out.println(queue.poll());
        System.out.println(queue.poll());
      //  System.out.println(queue.pop());
        //双向队列如何实现栈？？？？20240410

    }
    //队列
    public static void main(String[] args) {
        Deque<Integer> deque=new LinkedList<>();//队列的链式实现
     //   Deque<Integer> deque1=new ArrayDeque<>();//队列的线性实现
        deque.push(7);
        deque.offer(8);//因为dqpue实例化的是Linkedlist接口
//        deque1.push(9);
//        deque1.offer(10);
//        deque1.add(11);//因为Deque实现了Collection接口
        deque.add(12);
        deque.addFirst(13);
//        deque1.pop();
        System.out.println(deque);//输出的结果为[7,8,12]
        System.out.println(deque.pop());
        System.out.println(deque.pop());
        System.out.println(deque.pop());
//        System.out.println(deque1);//输出的结果为[9,10,11]
//        Stack<Integer> stack=new Stack<>();
//        stack.push(3);
//        System.out.println(stack.pop());
    }
}
