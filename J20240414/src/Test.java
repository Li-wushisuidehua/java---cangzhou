import java.util.Arrays;
import java.util.Random;

public class Test {
    //倒序
    public static void notOrder(int []array){
        for (int i = 0; i < array.length; i++) {
            array[i]= array.length-i;
        }
    }
    //正序
    public static void order(int []array){
        for (int i = 0; i < array.length; i++) {
            array[i]=i;
        }
    }
    //随机顺序
     public static void randomOrder(int []array){
         Random random=new Random();//生成随机数
         for (int i = 0; i < array.length; i++) {
             array[i]=random.nextInt(array.length);
         }

     }
    public  static void testinsertSort(int []array){
        array=Arrays.copyOf(array,array.length);
        long Starttime=System.currentTimeMillis();
        Sort.insertSort(array);//这里可以直接通过类名.方法来进行调用，则需要将方法默认为被static修饰
        long Endtime=System.currentTimeMillis();
        System.out.println("插入排序的时间为："+(Endtime-Starttime));

    }
    public static void testshellSort(int []array){
        array=Arrays.copyOf(array,array.length);//拷贝一份数组
        long Starttime=System.currentTimeMillis();//记录开始的时间
        Sort.shellSort(array);//进行排序
        long Endtime=System.currentTimeMillis();//记录排序完的时间
        System.out.println("希尔排序的时间为："+(Endtime-Starttime));
    }
    public static void testQuickSort(int []array){
        array=Arrays.copyOf(array,array.length);//拷贝一份数组
        long Starttime=System.currentTimeMillis();//记录开始的时间
        Sort.QuickSort(array);//进行排序
        long Endtime=System.currentTimeMillis();//记录排序完的时间
        System.out.println("优化后的快速排序的时间为："+(Endtime-Starttime));
    }
    public static void testQuickSort2(int []array){
        array=Arrays.copyOf(array,array.length);//拷贝一份数组
        long Starttime=System.currentTimeMillis();//记录开始的时间
        Sort.QuickSort2(array);//进行排序
        long Endtime=System.currentTimeMillis();//记录排序完的时间
        System.out.println("优化前的快速排序的时间为："+(Endtime-Starttime));
    }

    public static void main1(String[] args) {
        int []array={10,20,8,25,35,6,18,30,5,15,28};
//        Sort.selectSort(array);
        //排序前
        System.out.println(Arrays.toString(array));
//        Sort.bubbleSort2(array);
//        Sort.QuickSort(array);
//        Sort.NotRecursion(array);
//        Sort.mergeNor(array);
        Sort.shellSort(array);
        //排序后
        System.out.println(Arrays.toString(array));


    }

    public static void main5(String[] args) {
        int []array=new int[10_0000];
        randomOrder(array);
        testQuickSort2(array);
        testQuickSort(array);
        testshellSort(array);
        testinsertSort(array);
    }
    public static void main2(String[] args) {
        int []array=new int[10_0000];
        notOrder(array);//已经为array赋值了
      //  order(array);
    //randomOrder(array);
        testinsertSort(array);
         testshellSort(array);//由上面的结果可以知道，希尔排序的效率比纯插入排序的效率要高

    }
    public static void main(String[] args) {
        Sort sort=new Sort();
            int[] array = {5,1,1,2,0,0};
        //  int[] array1 = Arrays.copyOf(array, array.length);//数组的赋值，将数组copy了一份
         //   System.out.println(Arrays.toString(array1));//Arrays.toString(arrray1)是将数组以字符串的形式进行输出。
      //  sort.insertSort(array);
      //  System.out.println(Arrays.toString(array));
      //  sort.shellSort(array);
        sort.QuickSort(array);
        System.out.println(Arrays.toString(array));
    }
}
