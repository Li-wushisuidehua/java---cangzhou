import java.util.Arrays;
import java.util.Stack;

public class Sort {
    //插入排序

    /**
     * 时间复杂度：最坏的情况下5 4 3 2 1，复杂度为O(N的平方);
     * 空间复杂度：O(1),没有额外的开辟数组来占用额外的空间
     * 稳定性：稳定
     *
     * @param array
     */
    public static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int j = i - 1;
            int tmp = array[i];
            for (; j >= 0; j--) {
                if (array[j] > tmp) {//当前面的元素大于后面的元素时才交换，如果此时将条件改为array[j]>=tmp那么此时是不稳定的，因为相同的元素也进行了互换
                    array[j + 1] = array[j];
                } else {
                    // array[j+1]=tmp;
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }

    /**
     * 时间复杂度：N*log以gap为底n的对数
     * 空间复杂度：O(1)
     * 稳定性：不稳定
     */
    //希尔排序(缩小增量排序）
    public static void shellSort(int[] array) {
//        int gap=5;
       int gap = array.length;
      //将数组每次分count份
       while (gap > 1) {//希尔排序的前面的都算为预处理，只有当希尔排序分的gap最终等于1时才为最后的排序；
            gap = gap /2;//一般这里以谁为底，算时间复杂度为便是以谁为底的n的对数，这里是以2为底的n的对数
            TestshellSort(array, gap);
        }
    }

    public static void TestshellSort(int[] array, int gap) {
        for (int i = gap; i < array.length; i++) {
            int j = i - gap;
            int tmp = array[i];
            for (; j >= 0; j -= gap) {
                if (array[j] > tmp) {//当前面的元素大于后面的元素时才交换，如果此时将条件改为array[j]>=tmp那么此时是不稳定的，因为相同的元素也进行了互换
                    array[j + gap] = array[j];
                } else {
                    // array[j+1]=tmp;
                    break;
                }
            }
            array[j + gap] = tmp;
        }
    }

    /**
     * 时间复杂度：
     * 空间复杂度：
     * 稳定性：
     */
    //选择排序方法一
    public static void selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = i;//min表示的是下标
            int j = i + 1;
            for (; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            swap(array, min, i);
        }

    }

    public static void swap(int[] array, int i, int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }

    //选择排序方法二
    public static void selectSort2(int[] array) {
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            int minIndex = left;
            int maxIndex = left;
            for (int i = left + 1; i <= right; i++) {//right已经取的是length-1了，所以这里是<=
                if (array[i] > array[maxIndex]) {
                    maxIndex = i;
                }
                if (array[i] < array[minIndex]) {
                    minIndex = i;
                }
            }
            swap(array, minIndex, left);
            if (maxIndex == left) {
                maxIndex = minIndex;
            }
            swap(array, maxIndex, right);
            left++;
            right--;
        }
    }

    /**
     * 时间复杂度：O(N的平方)
     * 空间复杂度；O(N)
     * 稳定性：稳定
     */
    //冒泡排序优化版本一
    public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = tmp;
                }
            }

        }
    }


    /**
     * 时间复杂度：O(N)
     * 空间复杂度：O(1)
     * 稳定性：稳定
     *
     * @param array
     */
    //冒泡排序优化版本二
    public static void bubbleSort2(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            boolean flag = true;
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {//自身的前后进行比较，如果一次都没有变化，那么就说明本身已经是一个有序的数组了
                    swap(array, j, j + 1);
                    flag = false;
                }
            }
            if (flag) {
                return;
            }
        }
    }

    //快排第一种Hoare法
    public static void QuickSort(int[] array) {
        Sort(array, 0, array.length-1);
    }
    public static void QuickSort2(int[] array) {
        Sort2(array, 0, array.length-1);
    }
//测试优化后的快速排序
    public static void Sort(int[] array, int start, int end) {
        if (start >= end) {//当这里相等时，表明此时这里只有一个元素，一个元素不需要再进去找做或者右
            return;
        }
        int ret=centering(array, start,end);
        swap(array,start,ret);
        int par = testSort(array, start, end);
        testSort(array, start, par - 1);
        testSort(array, par + 1, end);
    }
    //测试优化快速排序前
    public static void Sort2(int[] array, int start, int end) {
        if (start >= end) {//当这里相等时，表明此时这里只有一个元素，一个元素不需要再进去找做或者右
            return;
        }
        //这时已经无限的趋近于有序，有序时使用插入排序时最快的
        if((end-start)+1<=10){
            insertSort(array);

        }
//        int ret=centering(array, start,end);
//        swap(array,start,ret);
        int par = testSort(array, start, end);
        testSort(array, start, par - 1);
        testSort(array, par + 1, end);
    }

    public  static int testSort(int[] array, int left, int right) {
        int i = left;
        int tmp = array[left];
        while (left < right) {
            while (left < right && array[right] >= tmp) {
                right--;
            }//当循环退出时array[right]已经拿到比tmp小的值了
            while (left < right && array[left] <= tmp) {
                left++;
            }//当循环退出的时候array[left]已经拿到比tmp大的值了，此时只需要将两个给元素进行交换
            swap(array, left, right);
        }
        //此时left与right已经相等了
        swap(array, left, i);
        return left;//此时的left指向的是left==right时的那个相等的值
    }
//快排第二种挖坑法
public static int testSort2(int[] array, int left, int right) {
    int i = left;
    int tmp = array[left];
    while (left < right) {
        while (left < right && array[right] >= tmp) {
            right--;
        }//当循环退出时array[right]已经拿到比tmp小的值了
        array[left]=array[right];
        while (left < right && array[left] <= tmp) {
            left++;
        }//当循环退出的时候array[left]已经拿到比tmp大的值了，此时只需要将两个给元素进行交换
       array[right]=array[left];
    }
    //此时left与right已经相等了
   array[left]=tmp;
    return left;//此时的left指向的是left==right时的那个相等的值
}
//快排第三种前后指针法
    public static int parttion(int []array,int left,int right){
        int prev=left;
        int cur=prev+1;
        while(cur<=right){
            if(array[cur]<array[left] && array[++prev]!=array[cur]){
                swap(array, cur, prev);
            }
            cur++;
        }
        swap(array,prev,left);
        return prev;
    }
    //快速排序优化方法
    public static int centering(int []array,int left,int right){
        int mid=(left+right)/2;
        if(array[left]>array[right]){
            if(array[mid]<array[right]){
                return right;
            }else if(array[mid]>array[left]){
                return left;
            }else{
                return mid;
            }
        }else{
            if(array[mid]<array[left]){
                return left;
            } else if (array[mid]>array[right]) {
                return right;
            }else{
                return mid;
            }
        }
    }
    //非递归实现快速排序
    public static void NotRecursion(int []array){
        Stack<Integer> stack=new Stack<>();
        int left=0;
        int right=array.length-1;
        int div=testSort2(array,left,right);
        if(div>left+1){
            stack.push(left);
            stack.push(div-1);
        }
        if(div<right-1){
            stack.push(div+1);
            stack.push(right);
        }
        while(!stack.empty()){
            right=stack.pop();
            left=stack.pop();
           div=testSort2(array,left,right);
            if(div>left+1){
                stack.push(left);
                stack.push(div-1);
            }
            if(div<right-1){
                stack.push(div+1);
                stack.push(right);
            }

        }
    }
    //归并排序的实现
    public static void mergeSort(int []array){
        mergeSortFun(array,0,array.length-1);
    }
    public static void mergeSortFun(int []array,int left,int right){
        if(left==right){
            return ;
        }
        int mid=(right+left)/2;
        mergeSortFun(array,left,mid);
        mergeSortFun(array,mid+1,right);
        merge(array,left,mid,right);

    }
    private static void merge(int []array,int left,int mid,int right){
        int []tmp=new int[right-left+1];//新规定的数组的长度
        int k=0;
        int s1=left;
        int e1=mid;
        int s2=mid+1;
        int e2=right;
        while(s1<=e1 && s2<=e2) {
            if (array[s1] <= array[s2]) {
                tmp[k++] = array[s1++];
            } else {
                tmp[k++] = array[s2++];
            }
        }
            while(s1<=e1){
                tmp[k++]=array[s1++];
            }
            while(s2<=e2){
                tmp[k++]=array[s2++];
            }
        for(int i=0;i<k;i++){
            array[i+left]=tmp[i];
        }
    }
    public static void mergeNor(int []array){
        int gap=1;
        while(gap<array.length){
            for (int i = 0; i <array.length ; i+=2*gap) {
               int left=i;
               int mid=left+gap-1;
               if(mid>=array.length){
                   mid=array.length-1;
               }
               int right=mid+gap;
               if(right>=array.length){
                   right=array.length-1;
               }
               merge(array,left,mid,right);
            }
            gap*=2;
        }

    }
//非于比较大小的方法，计数法排序
    public static void countSort(int []array){
        
    }

}