package Demo2;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        BuyCards buyCards=new BuyCards();

        List<Card> Cardlist=buyCards.buyCards();
        System.out.println("洗牌之前");
        System.out.println(Cardlist);
        System.out.println("洗牌之后");
        buyCards.shuffle(Cardlist);
        System.out.println(Cardlist);

    }
}
