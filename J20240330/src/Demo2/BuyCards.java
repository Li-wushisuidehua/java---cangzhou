package Demo2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BuyCards {
    public String [] cards=new String[]{"♥","♠","♣","♦"};
    public List<Card> buyCards(){
        List<Card> list1=new ArrayList<>();//将生成的扑克存储的地方
        for (int i=0;i<cards.length;i++){
            for (int j = 1; j <=13; j++) {
                Card card=new Card(cards[i],j);//实例化对象，为一张一张的牌
                list1.add(card);//在顺序表中添加元素
            }
        }
      return list1;//返回顺序表
    }
    public void shuffle(List<Card> list1){
        Random random=new Random();
        for (int i = list1.size()-1; i >0; i--) {
            int num=random.nextInt(i);
            swap(list1,i,num);//num为重新生成的随机数
        }



    }
    //对象内容的交换
      public void swap(List<Card> cardElemt,int i,int j){
        Card card=cardElemt.get(i);//get调用的函数用来取出对象
        cardElemt.set(i,cardElemt.get(j));//set用来交换元素
        cardElemt.set(j,card);

    }
    
 

}
