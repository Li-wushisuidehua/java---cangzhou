package Demo2;

//实现牌这个类
public class Card implements Comparable<Card>{
    public String suit;
    public int digit;

    public Card(String suit, int digit) {
        this.suit = suit;
        this.digit = digit;
    }

    @Override
    public String toString() {
        return "{" + suit +" "+ digit + "}";
    }

    @Override
    public int compareTo(Card o) {
        return this.digit-o.digit;
    }
    //♥，♠，♣，♦；
}
