package Demo2;

import java.util.ArrayList;
import java.util.List;

//抓牌
public class Drawcard implements Comparable<List<Card>>{
    //抓到的牌在手上应该按顺序排放
    public void OrderCard(List<Card> Cards){
        for (int j = 0; j < Cards.size()-1; j++) {
            for(int i = 0; i < Cards.size()-1-j; i++)
                if(Cards.get(i).compareTo(Cards.get(i+1))>0){
                Card card=Cards.get(i);
                Cards.set(i,Cards.get(i+1));
                Cards.set(i+1,card);
            }
        }
    }

    @Override
    public int compareTo(List<Card> o) {
        return 0;
    }

    public static void main(String[] args) {
        BuyCards buyCards=new BuyCards();
        List<Card> cards=buyCards.buyCards();
        buyCards.shuffle(cards);//洗牌
        List<Card> hand1=new ArrayList<>();
        List<Card> hand2=new ArrayList<>();
        List<Card> hand3=new ArrayList<>();
        List<List<Card>> hands=new ArrayList<>();//此时表示创建好了一个顺序表
        hands.add(hand1);
        hands.add(hand2);
        hands.add(hand3);
        //3个人抓5张牌
        for (int i = 0; i<5; i++) {
            for (int j = 0; j < 3; j++) {
               Card card=cards.remove(0);
                hands.get(j).add(card);
            }
        }
        System.out.println("第一个人的牌"+ hand1);
        System.out.println("第二个人的牌"+ hand2);
        System.out.println("第三个人的牌"+ hand3);

        Drawcard drawcard=new Drawcard();
        drawcard.OrderCard(hand1);
//        drawcard.OrderCard(hand2);
//        drawcard.OrderCard(hand3);
        System.out.println("第一个人被调整顺序后的牌"+ hand1);

    }


}
