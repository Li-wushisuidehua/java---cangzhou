import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
         Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }
        int min = arr[0],maxLen=0;
        for(int i=1;i<n;i++){
            min=Math.min(min,arr[i-1]);
            maxLen=Math.max(arr[i]-min,maxLen);
        }

        System.out.println(maxLen);

    }
}
