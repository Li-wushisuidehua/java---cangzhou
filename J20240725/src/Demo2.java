import java.util.Scanner;

public class Demo2 {
    //求字符串中最长回文子串
    public static void main(String[] args) {
        Scanner in =new Scanner(System.in);
        String str=in.nextLine();
        int i=0,len=str.length(),left=0,right=0,maxlen=0,flag=0;
        while(i<len){
            //当长度为奇数的时候
            left=i-1;
            right=i+1;
          while(left>=0 && right<len && str.charAt(left)==str.charAt(right)){
                  left--;
                  right++;
          }
            maxlen=Math.max(right-left-1,maxlen);

          //当长度为偶数的时候
            left=i; right=i+1;
          while(left>=0 && right<len && str.charAt(left) == str.charAt(right)){
                  left--;
                  right++;
          }
            maxlen=Math.max(right-left-1,maxlen);
            i++;
        }
        System.out.println(maxlen);

    }
}
