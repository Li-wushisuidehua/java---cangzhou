import java.util.Stack;

public class Test {

    public static void main(String[] args) {

    }
    public boolean isValid(String s) {
        Stack<Character> stack=new Stack<>();
        int i=0;
        for(i=0;i<s.length();i++){
            char ch=s.charAt(i);
            if(ch=='[' || ch=='{' || ch=='('){
                stack.push(ch);
            }else{
                if(stack.empty()){
                    return false;
                }else{
                    char chL=stack.peek();
                    if(chL=='(' && ch==')' || chL=='{' && ch=='}' || chL=='[' && ch==']'){
                        stack.pop();
                    }else {
                        return false;
                    }
                }
            }
        }
        if(stack.empty() && i==s.length()){
            return true;
        }
         return false;


    }
    public boolean IsPopOrder (int[] pushV, int[] popV) {
        Stack<Integer> stack=new Stack<>();
        int j=0;
        for (int i = 0; i < popV.length; i++) {
            stack.push(pushV[i]);
            while (!stack.empty() && j< popV.length && stack.peek()==popV[j]){
                stack.pop();
                j++;
            }
        }
        if(stack.empty()){
            return true;
        }
       return false;
    }
    //使用switch - case 代替if-else语句，会增大效率
    public int evalRPN(String[] tokens) {
        Stack<Integer> stack=new Stack<>();
        int p1;
        int p2;
        for(String s: tokens){
            switch (s){
                case "+":
                    p1=stack.pop();
                    p2=stack.pop();
                    stack.push(p1+p2);
                    break;
                case "-":
                    p1=stack.pop();
                    p2=stack.pop();
                    stack.push(p2-p1);
                    break;
                case "*":
                    p1=stack.pop();
                    p2=stack.pop();
                    stack.push(p2*p1);
                    break;
                case "/":
                    p1=stack.pop();
                    p2=stack.pop();
                    stack.push(p2/p1);
                    break;
                default:
                    //如果不是运算符时，将其压栈
                    stack.push(Integer.valueOf(s));//将其变为Integer类型

            }
        }
        return stack.pop();
    }

//            if(tokens[i].compareTo("+")==0){
//                int n=stack.pop();
//                int m=stack.pop();
//                stack.push(m+n);
//            }else if(tokens[i].compareTo("*")==0){
//                int n=stack.pop();
//                int m=stack.pop();
//                stack.push(m*n);
//            } else if (tokens[i].compareTo("/")==0) {
//                int n=stack.pop();
//                int m=stack.pop();
//                stack.push(m/n);
//            } else if (tokens[i].compareTo("-")==0) {
//                int n=stack.pop();
//                int m=stack.pop();
//                stack.push(m-n);
//            }
}
