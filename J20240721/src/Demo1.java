//笔试强训 Day06-1;
import java.io.PrintWriter;
public class Demo1 {
    public static void main(String[] args) {
        String s="999999999999999999999999999999999999999999999999999994";
        String t="";
        System.out.println(solve(s, t));
    }
    public  static String solve (String s, String t) {
        // write code here
        StringBuffer sb=new StringBuffer();//sb这里是一个字符串的类型
        int i=s.length()-1,j=t.length()-1;//从末尾下标开始
        int tmp=0;
        while(i>=0 || j>=0 || tmp!=0){
            if(i>=0) tmp+=s.charAt(i--)-'0';
            if(j>=0) tmp+=t.charAt(j--)-'0';
               sb.append(tmp%10);
               tmp/=10;
        }
        return sb.reverse().toString();
        //sb.append() 这里是一个StringBuffer的类型
        //sb.append().toString() 这里将StringBuffer类型转化为了字符串类型，防止return的类型不兼容

    }
}
