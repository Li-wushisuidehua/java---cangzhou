//大数相乘
public class Demo2 {
    public static void main(String[] args) {
        String s = "345";
        String t = "0";
        System.out.println(solve(s, t));
    }

    public static String solve(String ss, String tt) {
        // 将这两个字符串军转化为字符数组
        char[] s = new StringBuffer(ss).reverse().toString().toCharArray();//toCharArray()
        char[] t = new StringBuffer(tt).reverse().toString().toCharArray();
        int m = ss.length();
        int n = tt.length();
        int[] tmp = new int[m + n];

        //无进位相乘
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                tmp[i + j] += (s[i] - '0') * (t[j] - '0');//将数存入数组，因为这里原本的s[i]和t[j]为字符类型，-'0'可以是其变为int类型
            }
        }

        //进位相加
        StringBuffer ret=new StringBuffer();
        int c=0;
//        for(int i=0;i<tmp.length;i++){
//            ret.append((char)((tmp[i]+c)%10+'0'));
//            c=(tmp[i]+c)/10;
//        }
        //将上面的代码进行修改
        for (int x:tmp){
            c+=x;
            ret.append((char)(c%10+'0'));
            c/=10;
        }
        //由上面地for到这个的wihle注意，当最后一个元素%10和/10后，这里/10以后的值也是需要处理的，所以才有了下面的while
        while(c!=0){
            ret.append((char)(c%10+'0'));
            c/=10;
        }

        //去除末尾的0，但是如果光是0得留下一个
        while(ret.length()>1 && ret.charAt(ret.length()-1)=='0'){
            ret.deleteCharAt(ret.length()-1);//注意这里删除后，最初最后的元素的前一个元素成了最后的元素
        }
        return ret.reverse().toString();
    }
}
