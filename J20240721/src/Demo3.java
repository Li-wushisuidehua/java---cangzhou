public class Demo3 {
    public static void main(String[] args) {
        ListNode h1=new ListNode(9);
        ListNode h2=new ListNode(3);
        ListNode h3=new ListNode(7);
        ListNode hh1=new ListNode(6);
        ListNode hh2=new ListNode(3);
       h1.next=h2;
       h2.next=h3;
       hh1.next=hh2;
       h1= addInList(h1,hh1);
       while(h1!=null){
           System.out.print(h1.num);
           h1=h1.next;
       }


    }
    public static ListNode addInList (ListNode head1, ListNode head2) {
        // write code here
        ListNode cur=null;
        ListNode curN=null;
        //将链表用头插法
        if(head1.next!=null){
            cur=head1.next;
            head1.next=null;
            while(cur!=null){
                curN=cur.next;
                cur.next=head1;
                head1=cur;
                cur=curN;
            }
        }

        ListNode newHead=new ListNode(-1);
        int sum,n=0;

        //将链表转置

        if(head2.next!=null){
            cur=head2.next;
            head2.next=null;
            while(cur!=null){
                curN=cur.next;
                cur.next=head2;
                head2=cur;
                cur=curN;
            }
        }


        //对其进行相加
        ListNode cur1=head1;
        ListNode cur2=head2;
        cur=newHead;
        while(cur1 !=null && cur2!=null){
            sum=cur1.num+cur2.num+n;
            if(sum>=10){
             n=1;
            }else{
                n=0;
            }
            ListNode newNode=new ListNode(sum%10);
            cur.next=newNode;
            cur=newNode;
            cur1=cur1.next;
            cur2=cur2.next;
        }
        //对剩余的元素相加
        if(cur1==null){
            while(cur2!=null){
                sum=cur2.num+n;
                if(sum>=10){
                    n=1;
                }else{
                    n=0;
                }
                ListNode newNode=new ListNode(sum%10);
                cur.next=newNode;
                cur=newNode;
                cur2=cur2.next;
            }
            if(n==1){
                ListNode node=new ListNode(1);
                cur.next=node;
                cur=node;
            }
        }
        //对剩余的元素相加
        if(cur2==null){
            while(cur1!=null){
                sum=cur1.num+n;
                if(sum>=10){
                    n=1;
                }else{
                    n=0;
                }
                ListNode newNode=new ListNode(sum%10);
                cur.next=newNode;
                cur=newNode;
                cur1=cur1.next;
            }
            if(n==1){
                ListNode node=new ListNode(1);
                cur.next=node;
                cur=node;
            }
        }

        //你指结果链表
        newHead=newHead.next;
        cur=newHead.next;
        newHead.next=null;
        while(cur!=null){
            curN=cur.next;
            cur.next=newHead;
            newHead=cur;
            cur=curN;
        }

        return newHead;
    }
}
class ListNode{
    int num;
    ListNode next;

    public ListNode(int num) {
        this.num = num;
    }
}

