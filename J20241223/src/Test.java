import java.sql.*;
import java.time.*;
public class Test {
    public static void main1(String[] args) {
        System.out.println(LocalDateTime.now());
        System.out.println(ZonedDateTime.now().getYear());
        System.out.println(Instant.now());
    }
    static {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    public static Connection getConnection(){
        String id="sa";
        String password="123456";
        String url="jdbc:sqlserver://localhost;databaseName=Movie_Design";
        Connection conn;
        //开始连接数据库
        try {
            conn= DriverManager.getConnection(url,id,password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }

    public static void main(String[] args) throws SQLException {
        Connection conn=getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
       ps= conn.prepareStatement("select * from Accounts");
       rs=ps.executeQuery();
       while(rs.next()){
           System.out.println(rs.getString("Aid"));
           System.out.println(rs.getString("Apassword"));
       }
    }
}
