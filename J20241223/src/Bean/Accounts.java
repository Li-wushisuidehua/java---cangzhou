package Bean;

public class Accounts {
    private String Accountsname;
    private String AccountsSex;
    private int AccountsYear;


    public String getAccountsname() {
        return Accountsname;
    }

    public String getAccountsSex() {
        return AccountsSex;
    }

    public int getAccountsYear() {
        return AccountsYear;
    }

    public Accounts( String accountsname, String accountsSex, int accountsYear) {
        this.Accountsname = accountsname;
        this.AccountsSex = accountsSex;
        this.AccountsYear = accountsYear;
    }
}
