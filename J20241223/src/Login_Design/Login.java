package Login_Design;//需要用到的类
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
public class Login extends JFrame {
        //创建集合用来存放账户和密码信息。
        static ArrayList<String> users=new ArrayList<>();
        //下面的代码都在这个类里面
        // //构造setCode方法来创建一个随机的验证码
        String setCode(){
            Random r=new Random();
            char[] c=new char[52];    //在数组中存放52个英文字母
            String result="";
            for (int i = 0; i < c.length; i++) {
                if(i<26){
                    c[i]=(char)(65+i);//存放a~z
                } else {
                    c[i]=(char)(97+i-26);//存放A~Z
                }
            }
            for (int i = 0; i < 4; i++) {
                int number=r.nextInt(c.length);
                result=result+c[number];
            }

            int figure=r.nextInt(10);
            result=result+figure;
            //将字符串变为数组
            char[] crr=result.toCharArray();
            char t;
            //将数组打乱
            for (int i = 0; i < crr.length; i++) {
                int rom=r.nextInt(5);
                t=crr[i];
                crr[i]=crr[rom];
                crr[rom]=t;
            }
            //得到验证码
            return String.valueOf(crr);
        }
    //这里就是空参构造了，用来构建用户窗口
    public Login() {
        //创建开始界面
        JFrame jFrame = new JFrame();
        jFrame.setTitle("会员登录界面");
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(800, 480);//框大小
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLayout(null);
        //添加背景图片
        ImageIcon icon = new ImageIcon("D:\\壁纸\\small074417O9mVb1695253457.jpg");

        JLabel jLabel = new JLabel(icon);
        jLabel.setBounds(50, 50, 400, 440);

        //设置界面元素
        JLabel account = new JLabel("账号");
        JLabel password = new JLabel("密码");
        JLabel code = new JLabel("验证码");
        JButton renew = new JButton(setCode());
        //将验证码的值赋给renew按钮
        renew.setText(setCode());

        JButton register = new JButton("注册");
        JButton logIn = new JButton("登录");
        //这三个是输入框，下面图片中灰色的框就是
        JTextField account_ = new JTextField(80);
        JTextField password_ = new JTextField(80);
        JTextField code_ = new JTextField(70);
        //按钮的背景
        register.setIcon(new ImageIcon("C:\\Users\\21299\\Pictures\\Camera Roll\\1-图片\\imgs\\AE26B098-CB4C-46d5-A6D8-AAE4330AAFCB.png"));
        logIn.setIcon(new ImageIcon("C:\\Users\\21299\\Pictures\\Camera Roll\\1-图片\\imgs\\46B4E49A-7D7D-4dfb-916F-0881A4634E9A.png"));
        //设置位置
        register.setBounds(200, 380, 140, 45);//注册框
        logIn.setBounds(450, 380, 140, 45);//登录框
        renew.setBounds(410, 340, 90, 30);
        account.setBounds(250, 250, 80, 50);
        password.setBounds(250, 290, 80, 50);
        code.setBounds(235, 330, 80, 50);
        account_.setBounds(300, 260, 200, 30);
        password_.setBounds(300, 300, 200, 30);
        code_.setBounds(300, 340, 100, 30);
        //设置按钮字体的颜色和字样
        account.setForeground(Color.BLACK);
        password.setForeground(Color.BLACK);
        code.setForeground(Color.BLACK);
        account.setFont(new Font("楷体", Font.BOLD, 20));
        password.setFont(new Font("楷体", Font.BOLD, 20));
        code.setFont(new Font("楷体", Font.BOLD, 20));
        //输入框的颜色
        account_.setBackground(Color.lightGray);
        password_.setBackground(Color.lightGray);
        code_.setBackground(Color.lightGray);

        //将元素添加进窗口

        jFrame.add(account);
        jFrame.add(account_);
        jFrame.add(password);
        jFrame.add(password_);
        jFrame.add(code);
        jFrame.add(code_);
        jFrame.add(register);
        jFrame.add(logIn);
        jFrame.add(renew);
        jFrame.add(jLabel);
        jFrame.setVisible(true);
        //写到这里就可以呈现出下面的窗口画面（1）了
        //给注册，登录按钮添加鼠标监听器
        register.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (e.getButton() == 1) {
                    //如果左键点击注册，跳转进Register窗口
                    new Register();
                }
            }
        });
        logIn.addMouseListener(new MouseAdapter() {
            Connection conn=dao.BaseDao.getConnection();
            PreparedStatement ps=null;
            ResultSet rs=null;
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (e.getButton() == 1) {
                    if (account_.getText().isEmpty() || password_.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "请输入完整信息再登录");
                    } else if (!renew.getText().equals(code_.getText())) {
                        JOptionPane.showMessageDialog(null, "验证码输入错误");
                    } else {
                        try {
                            ps=conn.prepareStatement("select * from Pcounts");
                            rs=ps.executeQuery();
                            while(rs.next()){
                                Login.users.add(rs.getString("Aid"));
                                Login.users.add(rs.getString("Apassword"));
                            }
                        } catch (SQLException ex) {
                            throw new RuntimeException(ex);
                        }
                        //输入的账户，密码是否正确的依据
                        boolean flag = false;
                        for (int i = 0; i < users.size(); i++) {
                            if (account_.getText().equals(users.get(i)) && password_.getText().equals(users.get(i + 1))) {
                                flag = true;//如果无误，则true
                                break;
                            }
                        }
                        if (flag) {
                            JOptionPane.showMessageDialog(null, "登录成功！");

                        } else {
                            JOptionPane.showMessageDialog(null, "用户不存在");
                            JOptionPane.showMessageDialog(null, "请先注册");
                        }
                    }
                }
            }
        });
    }

}
