package Login_Design;

import Login_Design.Login;
import dao.BaseDao;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.time.*;


public class Register extends JFrame {

    //这里开始连接数据库



    boolean flag;//flag为账户是否已经存在的标准
    public Register(){
        //注册界面的窗口设置
        this.setTitle("会员注册界面");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(500,300);
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        this.setVisible(true);
        //设置窗口元素
        JLabel account=new JLabel("注册账户");
        account.setBounds(50,50,100,50);
        account.setFont(new Font("楷体",Font.BOLD,20));

        JLabel password=new JLabel("注册密码");
        password.setBounds(50,100,100,50);
        password.setFont(new Font("楷体",Font.BOLD,20));

        JLabel nextpassword=new JLabel("再次输入密码");
        nextpassword.setBounds(50,150,150,50);
        nextpassword.setFont(new Font("楷体",Font.BOLD,20));

        JTextField account_=new JTextField(30);//这里会进行账号密码啥的输入
        JTextField password_=new JTextField(30);
        JTextField nextpassword_=new JTextField(30);

        account_.setBounds(150,60,250,30);
        password_.setBounds(150,110,250,30);
        nextpassword_.setBounds(200,160,200,30);

        JButton jButton=new JButton("注册");
        jButton.setBounds(120,200,80,40);
        this.add(jButton);
        JButton jButton2=new JButton("退出");
        jButton2.setBounds(240,200,80,40);
        this.add(jButton2);
        //添加入窗口
        this.add(account);
        this.add(account_);
        this.add(password);
        this.add(password_);
        this.add(nextpassword);
        this.add(nextpassword_);

//代码敲到这里就可以实现这个窗口的基本布局了，如下图（2）



//给注册和退出添加鼠标监听器
        jButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Connection conn=dao.BaseDao.getConnection();
                PreparedStatement ps=null;
                ResultSet rs=null;
                try {
                    ps=conn.prepareStatement("select * from Pcounts");
                    rs=ps.executeQuery();
                    while(rs.next()){
                        Login.users.add(rs.getString("Aid"));
                        Login.users.add(rs.getString("Apassword"));
                    }
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
                super.mouseClicked(e);
                if(e.getButton()==1){
                    //for—each循环遍历，判断当前注册的账户是否已经存在
                    for(String str: Login.users){
                        String accountStr=account_.getText();
                        if(str.equals(accountStr)) {
                            flag = true;
                            break;
                        }
                    }
                    if(!flag){
                        //如果可以注册，接着判断是否为空
                        if(account_.getText().isEmpty()
                                ||password_.getText().isEmpty()){
                            JOptionPane.showMessageDialog(null,"账户或密码不能为空");
                        }else{
                            //判断两次密码是否一致
                            String passwordStr=password_.getText();
                            String nextpasswordStr=nextpassword_.getText();
                            if(passwordStr.equals(nextpasswordStr)){
                                //将账户和密码添加到集合中
                                //这里是添加的地方！！！！！！！！
                                String accountStr=account_.getText();
                                Login.users.add(accountStr);
                                Login.users.add(passwordStr);
                                try {
                                    ps=conn.prepareStatement("insert into Pcounts values(?,?,?)");//这个里面写sql语句
                                    ps.setString(1,accountStr);
                                    ps.setString(2,passwordStr);
                                    ps.setInt(3, ZonedDateTime.now().getYear());
                                    int i=ps.executeUpdate();
                                    if(i>0){
                                        System.out.println("插入成功！！");
                                    }
                                } catch (SQLException ex) {
                                    throw new RuntimeException(ex);
                                }

                                //将用户个人信息加入数据库加入数据库
                                JOptionPane.showMessageDialog(null,"注册成功");
                                flag=false;
                            }
                            else{
                                JOptionPane.showMessageDialog(null,"两次密码输入不一致");
                            }
                        }

                    }else{
                        JOptionPane.showMessageDialog(null, "该用户已存在！");
                    }

                }
            }
        });
        jButton2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if(e.getButton()==1){
                    //点击按钮“退出”后关闭此界面
                    dispose();
                }
            }
        });
    }
}
