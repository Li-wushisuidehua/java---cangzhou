package dao;


import java.sql.*;

public class BaseDao {
    static {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    public static Connection getConnection(){
        String id="sa";
        String password="123456";
        String url="jdbc:sqlserver://localhost;databaseName=电影在线网设计与开发";
        Connection conn;
        //开始连接数据库
        try {
            conn=DriverManager.getConnection(url,id,password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }

    public static void main(String[] args){
        Connection conn=getConnection();
        System.out.println(conn);
    }
}
