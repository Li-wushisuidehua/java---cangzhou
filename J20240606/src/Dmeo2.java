//多线程的实现方法  方法二
class MyRunnable implements Runnable{
    @Override
    public void run(){
        while(true){
            System.out.println("hello Thead");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
                e.printStackTrace();
            }
        }

    }

}
public class Dmeo2 {
    public static void main(String[] args) throws InterruptedException {
       Runnable Myrunnable=new MyRunnable() ;
        Thread t=new Thread(Myrunnable);
        //创建多线程时的万年不变函数
        t.start();

        //开始执行
        while(true){
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
//Runnable匿名内部类的实现
/*
//       Runnable Myrunnable=new Runnable(){
//            @Override
//           public void run(){
//                while(true){
//                    System.out.println("hello thread");
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        throw new RuntimeException(e);
//                    }
//                }
//            }
//        };
 */
/*
其中main是主线程
 */
