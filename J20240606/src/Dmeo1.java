//多线程的创建  方法一
class solution extends Thread{
    @Override
    public void run() {
        while(true){
            System.out.println("hello Thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
                e.printStackTrace();
            }
        }

    }
}
public class Dmeo1 {
    public static void main(String[] args) throws InterruptedException {
        solution solution=new solution();
        //创建线程
        solution.start();
        while(true){
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
/*
如上由此可见，sleep是Tread中的静态方法，被static修饰的方法可直接通过实例.方法名 。因为Thread是java.lang中函数，是系统自己导入的，每次
使用不需要自己进行对包的导入。上述中，其实并没有对run有执行，但是在整个程序执行的过程中会对run进行调用，这种用户自己定义，后被程序调用的方法
称之为回调函数。
 */