//使用lambada表达式书写多线程
public class Demo7 {
    public static void main(String[] args) throws InterruptedException {
        Thread thread=new Thread(()->{
            while(true){
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        //创建线程
        thread.start();

        while(true){
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
