public class Demo5 {
    public static void main(String[] args) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                System.out.println("hello thread");
            }
        };

        //创建线程
        thread.start();
    }
}
//上述将下述的代码用匿名内部类表示
//class Mythread extends Thread{
//    @Override
//    public void run(){
//        System.out.println("hello thread");
//    }
//}