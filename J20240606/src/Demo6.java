public class Demo6 {
    public static void main(String[] args) throws InterruptedException {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {//run为线程体
                while(true){
                    System.out.println("hello thread");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

            }
        });
        //创建线程
        thread.start();

        while(true){
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
