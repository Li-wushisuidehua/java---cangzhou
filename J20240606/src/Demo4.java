public class Demo4 {

    public static void main(String[] args) {
        Runner1 runner1 = new Runner1();
        Runner2 runner2 = new Runner2();
//		Thread(Runnable target) 分配新的 Thread 对象。
        Thread thread1 = new Thread(runner1);
        Thread thread2 = new Thread(runner2);
		thread1.start();
		thread2.start();
//        thread1.run();
//        thread2.run();
    }
}

class Runner1 implements Runnable { // 实现了Runnable接口，jdk就知道这个类是一个线程
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("进入Runner1运行状态——————————" + i);
        }
    }
}

class Runner2 implements Runnable { // 实现了Runnable接口，jdk就知道这个类是一个线程
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("进入Runner2运行状态==========" + i);
        }
    }
}
/*
start()函数使用来创建线程的，如果没有这个函数，就不会创建新的线程，当然也就不会执行多个线程
就像thread1和thread2.run()一样，此时是直接将run当作普通的函数进行调用，此时是当作普通函数，同时也就只有
主线程这一个线程在执行，此时在主线程中执行的都是需要按照执行顺序的。
 */
