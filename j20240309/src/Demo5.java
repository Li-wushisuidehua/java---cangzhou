//对象的打印！！！！好棒今天！！！！！越来越好！！！！！
class Student{
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
public class Demo5 {
    public static void main(String[] args) {
        Student student=new Student("小美",19);
        System.out.println(student);
    }
}
