package Demo3;

import Demo3.Animal;

public class Cat extends Animal implements IRunning {
    @Override
    public void run() {
        System.out.println(this.name+"正在走猫步！");
    }

    public Cat(String name, int age) {
        super(name, age);
    }

    @Override
    public void sleep() {
        System.out.println(this.name+"正在睡觉！");
    }
}
