package Demo3;

import java.util.concurrent.Callable;

public class Test {
    public static void fly(IFlying flying){//使用接口对其进行向上转型;
        flying.fly();//由于方法的重写会调用类中的方法;
    }
   public static void walk(IRunning running){
       running.run();
    }
    public static void sleep(Animal animal){
        animal.sleep();
    }
    public static void main(String[] args) {
        Drog drog=new Drog("青蛙",3);//类的实例化对象。
        Cat cat=new Cat("妹妹",10);
        Duck duck=new Duck("嘎嘎",5);
        walk(drog);
        walk(cat);
        fly(duck);
        sleep(drog);
        sleep(cat);
        sleep(duck);


    }
}
