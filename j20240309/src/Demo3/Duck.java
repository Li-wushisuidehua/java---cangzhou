package Demo3;

import Demo3.Animal;

public class Duck extends Animal implements IFlying, IRunning, ISwimming {
    @Override
    public void fly() {
        System.out.println(this.name+"正在飞！");
    }

    @Override
    public void run() {
        System.out.println(this.name+"正在奔跑！");
    }

    @Override
    public void siwm() {
        System.out.println(this.name+"正在游！");
    }

    @Override
    public void sleep() {
        System.out.println(this.name+"正在睡觉！");
    }

    public Duck(String name, int age){
        super("花花",18);
    }
}
