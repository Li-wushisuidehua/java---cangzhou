package Demo3;

import Demo3.Animal;

//有了继承、实现接口后,便有了一个新的类。这个类中包含了Animal和接口中的所有特性;
public class Drog extends Animal implements ISwimming, IRunning {//继承父类并且拥有implements后面的这些特性;
    @Override
    public void run() {//具有的特性
        System.out.println(this.name+"正在蹦蹦跳跳！");
    }

    @Override//方法重写的注解;
    public void siwm() {
        System.out.println(this.name+"正在开心游泳！");
    }

    public Drog(String name, int age) {//给了姓名
        super(name, age);
    }

    @Override
    public void sleep() {
        System.out.println(this.name+"正在睡觉！");
    }
}
