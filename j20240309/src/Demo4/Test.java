package Demo4;
class Student implements Comparable<Student>{
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(Student o) {
        if(this.name.compareTo(o.name)>0){
            return 1;
        }
        else if(this.name.compareTo(o.name)==0)
            return 0;
        else {
            return -1;
        }
    }
}
public class Test {
    public static void main(String[] args) {
        Student student1=new Student("wangzhaohan",18);//提问:为什么有一次作业中直接写了  new（）但是没有写前面的引用;
        Student student2=new Student("limingyue",19);
        if(student1.compareTo(student2)>0){
            System.out.println("student1>student2");
        }
        else if(student1.compareTo(student2)==0){
            System.out.println("student1=student2");
        }
        else {
            System.out.println("studen1<student2");
        }
    }


}
