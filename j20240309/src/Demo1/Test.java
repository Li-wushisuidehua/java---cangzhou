package Demo1;

public abstract class Test {
    public void eat(){
        System.out.println("hhhhhh");
    }
    public abstract void eatFood();

}
class Dog extends Test{
    @Override
    public void eatFood() {
        System.out.println("狗儿要吃狗粮！");
    }
    public  void show(){
        System.out.println("狗儿天天都很开心！");
    }
}
class show1{
    public static void eat(Test animal){//发生了向上转型，此时调用的本应该是父类的，如果父类中有重写了那么调用的就是重写的放方法。
        animal.eat();
        animal.eatFood();
        //现在要调用子类的自己的方法，此时需要向下转型；
        if(animal instanceof Dog){
            Dog dog=(Dog) animal;
           dog.show();
        }
    }

    public static void main(String[] args) {
        Dog dog=new Dog();
       eat(dog);//直接调用了eat这个函数，main函数时静态的，如果eat不加修饰符static那么eat就是非静态的。非静态需要对象来调用，而静态的不依赖于对象。
    }
}