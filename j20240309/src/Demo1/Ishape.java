package Demo1;

public interface Ishape {
     void draw();//接口当中的方法默认是public abstract修饰的，不需要写出来，接口当中的成员默认是public static final修饰的。定义的时候必须要初始化。
    void draw2();
    void draw3();
}
interface Ishaow{
    void show();


}
class c implements Ishaow,Ishape{
    @Override
    public void draw() {
        System.out.println("画一个三角形！");
    }

    @Override
    public void show() {
        System.out.println("小狗每天过的都很开心呢！");
    }

    @Override
    public void draw2() {

    }

    @Override
    public void draw3() {

    }
}
