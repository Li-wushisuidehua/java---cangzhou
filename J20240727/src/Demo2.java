import java.util.Scanner;

public class Demo2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n=in.nextInt(),sum=0;
        int []arr=new int[n+1];
        for (int i = 0; i < n; i++) {
            arr[i]=in.nextInt();
        }
        arr[n]=0;
        int min=arr[0];
        for (int i = 1; i < n+1; i++) {
            if(arr[i]<arr[i-1]){//现在i指向后面一位
                sum+=(arr[i-1]-min);
                min=arr[i];
            }
            if(arr[i-1]<min)  min=arr[i-1];
        }
        System.out.println(sum);
    }
}
