public class Dmeo1 {
    public static int lengthOfLongestSubstring(String ss) {
    //  char []s=ss.toCharArray();
      int []hash=new int[128];
      int left=0,right=0,n=ss.length();
      int ret=0;
      while(right<n){
          hash[ss.charAt(right)]++;
          while(hash[ss.charAt(right)]>1) {
              hash[ss.charAt(left++)]--;
          }
              ret=Math.max(ret,right-left+1);
          right++;
      }
      return ret;
    }

    public static void main(String[] args) {
        String str="deabcabca";
        System.out.println(lengthOfLongestSubstring(str));
    }
}
