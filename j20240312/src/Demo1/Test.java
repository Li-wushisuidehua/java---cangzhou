package Demo1;
//String类的定义，赋值
public class Test {
    public static void main(String[] args) {
        String str="";
        String str1=null;
        System.out.println(str);//有引用，但是对象的内容为空；所以打印0；
        System.out.println(str1);
        System.out.println(str.length());//str的对象没有，所以字符串长度为0；
        System.out.println(str1.length());//null是空指针，没有任何的指向，null表示不指向任何对象，对其求长度会出现空指针异常。
    }
    public static void main3(String[] args) {
        String str1="abcdef";
        String str2="Abcdef";
        System.out.println(str1.equals(str2));//打印false
        System.out.println(str1.equalsIgnoreCase(str2));//打印true

        //计算字符串的长度；
        String str="abcdef";
        System.out.println(str.length());
        System.out.println("abcdef".length());
    }
    public static void main2(String[] args) {
        String str1="abcdef";
        String str2="abcdef";
        System.out.println(str1==str2);//打印的结果为ture，此时涉及字符串常量池的知识点，以后介绍。
        System.out.println(str1.equals(str2));//判断str1和str2的内容，也就是字符串是否相等。
        String str3=new String("mmmgggg");
        String str4=new String("mmmgggg");
        System.out.println(str3==str4);//打印的结果为false，因为new的不同的对象，str3和str4所指向的对象的地址不一样，但是对象中均含有value
        //value中存储的是字符串所在的地址；
        System.out.println(str3.equals(str4));//解释同上；

    }
    public static void main1(String[] args) {
        //直接赋值
        String str="abcdef";
        System.out.println(str);
        //使用构造方法，对其进行赋值
        String str2 = new String("jqk");
        System.out.println(str2);
        //使用数组，把数组中的值放入字符串中
        //char[] chars=new char[]{'a','b','c'};
//          char[] chars;
//          chars=new char[]{'a','c','c'};

//        char [] chars={'a','b','c'};
        String str3=new String("");
        //" "表示有引用对象，但是对象为0，这种情况不等于null;
        System.out.println(str3);
    }
}
