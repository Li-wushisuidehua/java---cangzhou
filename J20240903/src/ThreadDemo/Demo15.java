package ThreadDemo;

public class Demo15 {
    private static volatile boolean isQuit=false;
    public static void main(String[] args) throws InterruptedException {
        Thread t1=new Thread(()->{
            while(!isQuit){
                System.out.println("李四面目狰狞，争分夺秒的转账……");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("完毒紫了，转了这么多……");
        });

        System.out.println("（李四接到老板电话后）：老板放心一定完成任务");
        t1.start();
        Thread.sleep(10*1000);
        System.out.println("老板给李四打电话说对方是骗子……");
        isQuit=true;
    }
}
