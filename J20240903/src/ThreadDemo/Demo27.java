package ThreadDemo;

import java.util.concurrent.*;

public class Demo27 {
   public static CountDownLatch countDownLatch=new CountDownLatch(110);
    public static void main(String[] args) throws InterruptedException {

        //TimeUnit.SECONDS ：当线程数目大于核心线程的数量时，多余的空闲线程存活时间最长为 60s
        ThreadPoolExecutor pool=new ThreadPoolExecutor(5,10,3,TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100),
                new ThreadPoolExecutor.DiscardPolicy());

        // 测试执行
        for (int i = 0; i < 2000; i++) {
            int id=i;
           pool.submit(()->{
               System.out.println(Thread.currentThread().getName()+" 已执行 "+id);
               countDownLatch.countDown();
           });
        }

        countDownLatch.await();
        pool.shutdown();
    }
}
