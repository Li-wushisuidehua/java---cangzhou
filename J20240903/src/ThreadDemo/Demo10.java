package ThreadDemo;

public class Demo10 {

    //死锁代码1
    public static Object locker=new Object();
    public static void main(String[] args) {
        Thread t1=new Thread(()->{
            synchronized (locker){
                synchronized (locker){
                    System.out.println("Hello world");
                }
            }
        });

        //开启线程
        t1.start();
    }
}
