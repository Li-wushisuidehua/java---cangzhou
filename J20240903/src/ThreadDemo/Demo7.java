package ThreadDemo;

public class Demo7 {
    public static void main(String[] args) {
        Thread t=new Thread(()->{
            while(true){
                System.out.println("hello.txt Thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        },"自定义线程");

        //查看线程在系统内核中是否存活
        System.out.println(t.isAlive());
        //开启线程
        t.start();

        //获取线程的ID
        System.out.println(t.getId());
        //获取线程的名称
        System.out.println(t.getName());
        //获取线程的当前状态
        System.out.println(t.getState());
        //查询该线程是否为后台线程
        System.out.println(t.isDaemon());
        //获取线程的优先级
        System.out.println(t.getPriority());
        //再次查看线程是否存活
        System.out.println(t.isAlive());
        //查看线程是否被中断
        System.out.println(t.isInterrupted());

    }
}
