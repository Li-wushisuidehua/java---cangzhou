package ThreadDemo;

public class Demo11 {
    //死锁代码2
    public static Object locker1=new Object();
    public static Object locker2=new Object();
    public static void main(String[] args) {
        Thread t1=new Thread(()->{
            synchronized (locker1) {
                System.out.println("t1 拿到锁1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (locker2) {
                    System.out.println("t1 拿到锁2");
                }
            }
        });
        Thread t2=new Thread(()->{
            synchronized (locker2) {
                System.out.println("t2 拿到锁2");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (locker1) {
                    System.out.println("t2 拿到锁1");
                }
            }
        });

        //开启线程
        t1.start();
        t2.start();
    }
}
