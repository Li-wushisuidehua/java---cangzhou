package ThreadDemo;

import java.util.concurrent.*;

//实现多线程中线程池的代码
public class Demo21 {
    public static void main(String[] args) {
        ExecutorService service=Executors.newFixedThreadPool(4);
        //其中Executors是一个类，这个类中有静态方法newFixedThreadPool()，这个静态方法的返回值为ExecutorService类型的变量
        //其中ExecutorService是一个接口，继承自Executor
      //  Executor executor=new ThreadPoolExecutor(2,4,60, TimeUnit.MINUTES,new ArrayBlockingQueue<>(3));
        for (int i = 1; i <=100 ; i++) {
            int id=i;

            //使用submit()来提交线程需要执行的任务
                service.submit(() -> {
                    System.out.println(Thread.currentThread().getName() + "生产了" + id);
                });
        }
        //使用这个方法来关闭前台线程
        service.shutdown();
    }
}
/**
 * 注意，在多线程中创建的线程均为前台线程，由于线程池创建的线程也为前台线程，所以说
 * 在上面的循环结束后，由于创建的前台线程，虽然main线程结束了，但是前台线程仍然存在。需要手动调用方法对前台线程进行关闭。
 */
