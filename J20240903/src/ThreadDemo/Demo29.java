package ThreadDemo;

import java.util.Timer;
import java.util.TimerTask;

public class Demo29 {
    public static void main(String[] args) {
        Timer timer=new Timer();

        //构造方法需要的是 TimerTask 类型的对象
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello.txt,Timer");
            }
        },1000);


    }
}
