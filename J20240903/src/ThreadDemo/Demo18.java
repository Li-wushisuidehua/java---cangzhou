package ThreadDemo;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Demo18 {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<String> queue=new ArrayBlockingQueue<>(3);
        queue.put("111111");
        System.out.println("加入成功");
        queue.put("111111");
        System.out.println("加入成功");
        queue.put("111111");
        System.out.println("加入成功");
        queue.put("111111");
        System.out.println("加入成功");
        queue.put("111111");
        System.out.println("加入成功");

         //由于上述设置了capacity为3个，所以这里即使加入很多也不会加入，会造程阻塞
        //当阻塞队列满的时候，此时在加入元素不会报错，而是会阻塞在哪里，取出元素也是一样，如果阻塞队列中现在已经没有值了，此时也不会
        //报错，而是会阻塞等待，等待有元素入队列
    }
}
