package ThreadDemo;

public class Demo5 {
    public static void main(String[] args) {
        Thread t=new Thread(()->{
            while(true){
                System.out.println("hello.txt Thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        //开启线程
        t.start();
    }
}
