package ThreadDemo;

public class MyBlockingQueue {
    public String[] data=null;
    //记录队列首的位置
    public volatile int head=0;
    //记录队列尾的位置
    public volatile int tail=0;
    //记录队列中元素的个数
    public volatile int size=0;

    public MyBlockingQueue(int capacity) {
        data=new String[capacity];//实现构造
    }

    //put方法
    public void put(String str){
        synchronized (this){
            //放入元素时需要判断队列的元素是否为满,如果满了需要阻塞，且由take一个元素之后可以继续put
            while(size==data.length){
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            data[tail]=str;
            tail++;
            size++;
            //阻塞队列是一个循环队列，当tail指向最后的时候需要将tail指向最初
            if(tail>=data.length) tail=0;
            this.notify();
        }


    }
    //take方法
    public String take(){
        String ret="";
        synchronized (this){
            while(size==0){
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
           ret=data[head];
            head++;
            size--;
            if(head==data.length) head=0;
            this.notify();

        }
        return ret;
    }
}
