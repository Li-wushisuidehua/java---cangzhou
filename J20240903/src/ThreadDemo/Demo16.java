package ThreadDemo;

public class Demo16 {
    public static void main(String[] args) throws InterruptedException {
        Thread t1=new Thread(()->{
            System.out.println(Thread.interrupted());//判断当前是否有设置标志位，并清除
        });

        System.out.println(t1.isInterrupted());//设置标志位,初始是false
        t1.interrupt();//此时会将标志为设置为true
        t1.start();//开启线程
        Thread.sleep(1000);//休眠，更好展示效果
        System.out.println(t1.isInterrupted());
    }
}
