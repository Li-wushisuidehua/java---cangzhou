package ThreadDemo;

public class Demo9 {
    public static Object locker1=new Object();
    public static Object locker2=new Object();
    public static void main(String[] args) {
        Thread t1=new Thread(()->{
            System.out.println("A");//注意这里的打印操作其实非常的耗时，防止还没有执行到wait()，notify()就先执行了

            //睡眠一下
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            synchronized (locker1){
                locker1.notify();
            }
        });
        Thread t2=new Thread(()->{
            synchronized (locker1){
                try {
                    locker1.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("B");
            synchronized (locker1){
                locker2.notify();
            }
        });
        Thread t3=new Thread(()->{
            synchronized (locker2){
                try {
                    locker2.wait(); //因为t2和t3一开头就是wait，所以不用担心会执行到
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("C");
            synchronized (locker2){
                locker2.notify();
            }
        });

        //开启线程
        t1.start();
        t2.start();
        t3.start();
    }
}
