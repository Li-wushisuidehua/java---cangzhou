package ThreadDemo;

public class Demo6 {
    public static void main(String[] args) throws InterruptedException {
        Thread t=new Thread(()->{

            // 获取当前线程的引用
           Thread tThread= Thread.currentThread();

           // tThread.isInterrupted()，最初调用时返回false
           while(!tThread.isInterrupted()){
               System.out.println("hello.txt Thread");
               try {
                   Thread.sleep(1000);//休眠
               } catch (InterruptedException e) {
                   throw new RuntimeException(e);
               }
           }
        });

        //开启线程
        t.start();

//        Thread.sleep(1);
        //调用interrupt()方法,此时 tThread.isInterrupted()标志位的值会变为true
        t.interrupt();
    }
}
