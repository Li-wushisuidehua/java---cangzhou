package ThreadDemo;

import java.util.concurrent.*;

public class Demo26_2 {

    //使用线程池累加从 1+2+3+……+1000
    public static void methods_02() {
            class MyCallable implements Callable<Integer> {
                // 每组数据的开始值
                private int start;
                // 每组数据的结束值
                private int end;

                // 构造方法，传入开始与结束值，明确累加区间
                public MyCallable(int start, int end) {
                    this.start = start;
                    this.end = end;
                }

                // 重写call方法
                @Override
                public Integer call() throws Exception {
                    // 完成累加操作并返回结果
                    int sum = 0;
                    for (int i = start; i <= end; i++) {
                        sum += i;
                    }
                    return sum;
                }
            }
            // 将0 ~ 1000拆成果10组数据分别用10个线程去执行，最后统计结果
            int count = 10;
            // 创建线程池
            ExecutorService pool = Executors.newFixedThreadPool(5);
            // 定义一个Future数组
            Future<Integer>[] futures = new Future[count];
            // 拆分大数，并创建任务，提交到线程池
            for (int i = 0; i < count; i++) {
                int start = i * 100 + 1;
                int end = (i + 1) * 100;
                // 提交到线程池，并保存返回的Futuer
                Future<Integer> future = pool.submit(new MyCallable(start, end));
                futures[i] = future;
            }

            // 先定义返回的结果，初始为0
            int result = 0;
            for (int i = 0; i < futures.length; i++) {
                // 获取Future
                Future<Integer> future = futures[i];
                try {
                    // 从Future中获取每条线程执行的结果
                    int sum = future.get();
                    // 累加
                    result += sum;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            // 停止线程池
            pool.shutdown();
            System.out.println("methods_02结果为：" + result);
    }
}
