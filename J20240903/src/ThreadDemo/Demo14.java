package ThreadDemo;

public class Demo14 {
    public static Object locker1=new Object();
    public static Object locker2=new Object();
    public static Object locker3=new Object();
    public static Object locker4=new Object();
    public static void main(String[] args) {
        Thread t1=new Thread(()->{
            synchronized (locker1){
                synchronized (locker2){
                    synchronized (locker3){
                        synchronized (locker4){

                        }
                    }
                }
            }
        });
        Thread t2=new Thread(()->{
            synchronized (locker2){
                synchronized (locker3){
                    synchronized (locker4){
                        synchronized (locker1){

                        }
                    }
                }
            }
        });
        Thread t3=new Thread(()->{
            synchronized (locker3){
                synchronized (locker4){
                    synchronized (locker1){
                        synchronized (locker2){

                        }
                    }
                }
            }
        });
        Thread t4=new Thread(()->{
            synchronized (locker4){
                synchronized (locker1){
                    synchronized (locker2){
                        synchronized (locker3){

                        }
                    }
                }
            }
        });

        //开启线程
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
