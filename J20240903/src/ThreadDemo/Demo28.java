package ThreadDemo;

import java.util.concurrent.atomic.AtomicInteger;

public class Demo28 {
    public static AtomicInteger count=new AtomicInteger(0);// 赋初始值为0
    public static void main(String[] args) throws InterruptedException {
        Thread t1=new Thread(()->{
            for (int i = 0; i < 5000; i++) {
                count.getAndIncrement();
            }
        });
        Thread t2=new Thread(()->{
            for (int i = 0; i < 5000; i++) {
                count.getAndIncrement();
            }
        });

        //开启线程
        t1.start();
        t2.start();
        t1.join();
        t2.join();

        //注意这里的两个 join 方法不可以省略，因为两个一旦省略会很快执行，下面这条打印语句
        //后面即使 count++ 了，但是没有打印语句仍然不会显现出来。
        System.out.println("最终的结果为："+count.get());
    }
}

