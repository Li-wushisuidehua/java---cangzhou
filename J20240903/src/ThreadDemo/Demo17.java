package ThreadDemo;

import java.util.ArrayDeque;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

public class Demo17 {
    public static void main(String[] args) {
        BlockingQueue<String> queue=new ArrayBlockingQueue<>(3);//利用Java的库函数创建阻塞队列
        queue.offer("1111");
        System.out.println("加入成功");
        queue.offer("1111");
        System.out.println("加入成功");
        queue.offer("1111");
        System.out.println("加入成功");
        queue.offer("1111");
        System.out.println("加入成功");

        //注意这里原本设置的capacity为三个，但是这里如果使用offer方法，代表这里不能构成阻塞，所以说四个都可以进入，但是如果使用put方法会不一样

    }
}
