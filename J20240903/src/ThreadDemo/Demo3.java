package ThreadDemo;

public class Demo3 {
    public static void main(String[] args) {

        Thread t=new Thread(){
            @Override
            public void run() {
                while(true){
                    System.out.println("hello.txt Thread");
                    try {
                        Thread.sleep(1000);//进行休眠
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };

        //开启线程
        t.start();
    }
}
