package ThreadDemo;

import java.util.Scanner;

public class Demo8 {
    public static volatile int n=0;//volatile可以用于解决内存的可见性问题，内存的可见性问题是由于JVM的优化操作产生的
    public static void main(String[] args) throws InterruptedException {
        Thread t=new Thread(()->{
            while(n==0){
                try {
                    Thread.sleep(10);//休眠需要的时间会比从内存中读取数据更长，更耗时
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }
        });

        Thread t2=new Thread(()->{
            Scanner scan=new Scanner(System.in);
            n=scan.nextInt();
        });

        t.start();
        t2.start();
        t2.join();
        t.join();
    }
}
