package ThreadDemo;

public class Demo19 {
 public  static MyBlockingQueue queue=new MyBlockingQueue(10);
    public static void main(String[] args) throws InterruptedException {

        Thread t1=new Thread(()->{
            for(int i=0;i<5;i++){
                try {
                queue.put(i+"");
                System.out.println("线程t1放入元素："+i);
                Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }
        });
        Thread t2=new Thread(()->{
            while(true){
                String s=queue.take();
                System.out.println("线程t2取出元素："+s);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        //开启线程
        t1.start();
        Thread.sleep(10);
        t2.start();
    }
}
