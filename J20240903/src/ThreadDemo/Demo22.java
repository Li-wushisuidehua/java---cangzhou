package ThreadDemo;

import java.util.concurrent.atomic.AtomicInteger;

//CAS的实现

public class Demo22 {
    public static AtomicInteger count=new AtomicInteger(0);//设置初始值
    public static void main(String[] args) throws InterruptedException {

        Thread t1=new Thread(()->{
            for (int i = 0; i < 50000; i++) {
                count.getAndIncrement();
            }
        });
        Thread t2=new Thread(()->{
            for (int i = 0; i < 50000; i++) {
                count.getAndIncrement();

            }
        });

        //启动线程
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("count = "+count);

    }
}
