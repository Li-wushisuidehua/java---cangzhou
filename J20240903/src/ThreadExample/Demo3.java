package ThreadExample;

/*
刚刚的饿汉和懒汉模式，在多线程的情况下，其中会有线程安全问题的出现
其中饿汉模式没有线程安全问题，但是懒汉模式有线程安全
 */
//修改懒汉模式的线程安全问题----ok
public class Demo3 {
    public static void main(String[] args) {
        Thread t1=new Thread(()->{
            singletonLazy2 st1=singletonLazy2.getInstance();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            singletonLazy2 st2=singletonLazy2.getInstance();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(st1==st2);

        });
        Thread t2=new Thread(()->{
            singletonLazy2 st1=singletonLazy2.getInstance();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            singletonLazy2 st2=singletonLazy2.getInstance();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(st1==st2);
        });

        //开启线程
        t1.start();
        t2.start();
    }
}
class singletonLazy2{
    public static volatile singletonLazy2 instance=null;
    //构造方法私有化
    private singletonLazy2(){

    }
    //获取实例
    public static singletonLazy2 getInstance(){
        if(instance==null){
            synchronized (singletonLazy2.class){
                if(instance==null){
                    instance=new singletonLazy2();
                }
            }
        }

        //其实这里可以看成是一个读操作
        return instance;
    }
    /*
    出现if判断并修改的代码，是典型的线程不安全问题，这样的时候需要加锁处理
     */

}
