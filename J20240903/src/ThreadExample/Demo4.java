package ThreadExample;

public class Demo4 {
    public static void main(String[] args) throws InterruptedException {
        Thread t1=new Thread(()->{
            System.out.println("买醋的路上");
            for (int i = 0; i < 1000; i++) {
                try {
                    Thread.sleep(1000);//sleep 会在下文中讲解到
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("我在买醋……");
            }
            System.out.println("买完醋ing~~，回家！！");

        });
        Thread t2=new Thread(()->{
            System.out.println("煮面条，打调料碗……");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("面条出锅……");

        },"t2线程");

        Thread.currentThread().setName("main线程");
        //开启线程
        System.out.println("先去买醋");
        t1.start();
        t1.join();

        System.out.println("买完醋回来了");
        t2.start();
        t2.join();

    }

}
