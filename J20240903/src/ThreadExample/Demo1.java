package ThreadExample;

//多线程的案例---饿汉模式(只允许一个实例的出现)
public class Demo1 {
    public static void main(String[] args) {
        singleton st=singleton.getSingleton();
        singleton st2=singleton.getSingleton();
        System.out.println(st==st2);

    }
}
//饿汉模式在类被加载的时候就被创建实例
class singleton{
   public static singleton st=new singleton();

    //获取已经创建好的实例,每次获取的实例为同一个，这样确保了只有一个实例
    public static singleton getSingleton(){
        return st;
    }
    //private修饰构造方法,确保外界不能创建实例
    private singleton(){

    }
}