package ThreadExample;


import java.sql.Time;
import java.util.PriorityQueue;
import java.util.Timer;
import java.util.TimerTask;

public class MyTimer {
    //传递的任务都是要带时间的
   private PriorityQueue<MyTimerTask> queue=new PriorityQueue<>();
  private Object locker = new Object();
    public void schedule(Runnable run,long delay) throws InterruptedException {
        synchronized (locker){
            // 这里实例化了一个 task，且唯一对应
            MyTimerTask task=new MyTimerTask(run,delay);
            queue.offer(task);
            locker.notify();
        }

    }
    public MyTimer() throws Exception {
        //此时在这里已经创建出了其他线程
        Thread t=new Thread(()->{
            while(true){
                synchronized (locker){

                    //这里使用 while 来判定是否 wait 操作，防止 wait 被其他可唤醒机制唤醒
                    while(queue.size()==0){
                        try {
                            locker.wait();
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    MyTimerTask current=queue.peek();
                    if(System.currentTimeMillis()>=current.getTime()){
                        current.run();
                        queue.poll();
                    }else{
                        try {
                            //使用 wait 带超时时间的方法，既让出 CPU 的执行权，又可以等待，直到等待时间结束，因为如果最少一个是时间都没有执行完，那么长时间的也不会执行到
                            locker.wait(current.getTime()-System.currentTimeMillis());
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            }
        });
      t.start();
    }
}


//不止一个任务，每个任务开始执行的时间不一样，根据时间的先后顺序，依次执行相应的程序
class MyTimerTask implements Comparable<MyTimerTask>{
    private Runnable runnable=null;
    private long delay=0;

    public MyTimerTask(Runnable runnable, long delay) {
        this.runnable = runnable;

        //注意这里时间的定义，有助于后面的修改
        this.delay = System.currentTimeMillis()+delay;
    }

    public void run(){
        runnable.run();
    }
    public long getTime(){
        return delay;
    }
    @Override
    public int compareTo(MyTimerTask o) {
        return (int)(this.delay-o.delay);
    }
}



class Test{
    public static void main(String[] args) throws Exception {
        MyTimer timer=new MyTimer();
        timer.schedule(()->{
            System.out.println("hello.txt 3000");
        },3000);
        timer.schedule(()->{
            System.out.println("hello.txt 2000");
        },2000);
        timer.schedule(()->{
            System.out.println("hello.txt 1000");
        },1000);
        System.out.println("线程开始执行");
    }
}
