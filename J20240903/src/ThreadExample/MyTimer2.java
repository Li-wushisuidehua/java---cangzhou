package ThreadExample;

import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;

public class MyTimer2 {
   private PriorityQueue<MyTimerTask2> queue=new PriorityQueue<>();
    private Object locker=new Object();

    public void schedule(Runnable runnable,long delay){
              MyTimerTask2 task2=new MyTimerTask2(runnable,delay);
              synchronized (locker){
                  queue.add(task2);
                  //唤醒正在因为没有元素而阻塞的堆
                  locker.notify();
              }
    }
    public MyTimer2(){
        Thread t=new Thread(()->{
           while(true){
               synchronized (locker){
                   //当堆中没有元素时，此时应该处于阻塞状态，使用 while 防止是其他操作唤醒了 wait
                   while(queue.size()==0){
                       try {
                           locker.wait();
                       } catch (InterruptedException e) {
                           throw new RuntimeException(e);
                       }
                   }

                   MyTimerTask2 current=queue.peek();
                   //判定时间是否达到执行的时间
                   if(current.getTime()<=System.currentTimeMillis()){
                       //达到条件执行方法
                       current.run();
                       //方法执行完毕，将此元素踢出堆
                       queue.poll();
                   }else{
                       //既可以休眠又可以让出 CPU 执行权的方法， wait 带超时时间的方法，这样还不会影响到其他线程
                       //这里不建议使用 Thread.sleep() 方法，因为此方法会抱着锁一起睡，这样其他线程的行动受阻
                       try {
                           locker.wait(current.getTime()-System.currentTimeMillis());
                       } catch (InterruptedException e) {
                           throw new RuntimeException(e);
                       }
                   }
               }
           }
        });
        t.start();
    }
}
class MyTimerTask2 implements Comparable<MyTimerTask2>{
    private Runnable runnable=null;
    private long time=0;

    public MyTimerTask2(Runnable runnable, long time) {
        this.runnable = runnable;
        this.time = System.currentTimeMillis()+time;
    }

    public void run() {
        runnable.run();
    }

    public long getTime() {
        return time;
    }

    @Override
    public int compareTo(MyTimerTask2 o) {
        //原方法的返回类型为 int 类型
        return (int) (this.time-o.time);
    }
}
class Test2{
    public static void main(String[] args) {
        MyTimer2 myTimer2=new MyTimer2();
        myTimer2.schedule(()->{
            System.out.println("hello.txt 1000");
        },1000);
        myTimer2.schedule(()->{
            System.out.println("hello.txt 2000");
        },2000);
        myTimer2.schedule(()->{
            System.out.println("hello.txt 3000");
        },3000);
        System.out.println("线程开始执行~");
    }
}