package ThreadExample;

//多线程案例---懒汉模式(在被需要的时候建立)
public class Demo2 {
    public static void main(String[] args) {
        singletonLazy st1=singletonLazy.getInstance();//获取实例
        singletonLazy st2=singletonLazy.getInstance();

        //判断两个引用是否指向同一个对象
        System.out.println(st1==st2);
    }
}
class singletonLazy{
    public static singletonLazy instance=null;
    //构造方法私有化
   private singletonLazy(){

    }
    //获取实例
    public static singletonLazy getInstance(){
      if(instance==null){
          instance=new singletonLazy();
      }
      return instance;
    }

}
