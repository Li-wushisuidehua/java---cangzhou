package ThreadExample;
/*
public class MyThreadPoolExecutor2 {
    threads=new Thread[capacity];
    countDownLatch=new CountDownLatch(1000);
    //根据capacity的值来创建线程的个数
        for (int i = 0; i < capacity; i++) {
        threads[i]=new Thread(()->{
            //每个线程不停的从队列中获取任务
            while(!isQuit){
                try {
                    Runnable run = queue.take();
                    run.run();
                    countDownLatch.countDown();//over!!
                } catch (InterruptedException e) {
                    // 线程被中断，退出循环
                    break;
                }
            }
        });
        threads[i].start();
    }
}

    //递交方法
    public void submit(Runnable run){
        try {
            queue.put(run);//任务在这里被接受
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    // 停止所有的 worker线程 这个只在线程池要关闭的时候才会调用
    private void stopAllThread() {
        for (Thread worker : threads) {
            worker.stop(); // 调用 worker 的 stop 方法 让正在执行 worker 当中 run 方法的线程停止执行
        }
    }

    public void shutDown() throws InterruptedException {
        countDownLatch.await();//只是判断所有的任务都执行完毕了
        stopAllThread();
    }
   }
 */
// countDownLatch 用于特定的场景下判断多个线程执行多个任务有没有结束，就像线程池这种不好用于判断程序是否运行完毕，如上述代码


