package Network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

public class UdpechoClient {
   private DatagramSocket socket=null;
    public String add=null;
    public int port=0;

    public UdpechoClient(String add,int port) throws SocketException {
        this.socket = new DatagramSocket();
        this.add=add;
        this.port=port;

    }
    public void start() throws IOException {
        System.out.println("启动客服端");
        Scanner scanner=new Scanner(System.in);

        while(true){ // 客服端可以一直向服务器发送请求
            System.out.print("->");
            // 输入请求
            String s=scanner.next();

            // 将数据传入数据包,需要服务器的 ip+端口号
            DatagramPacket datagramPacket=new DatagramPacket(s.getBytes(),s.getBytes().length, InetAddress.getByName(this.add),this.port);

            // 发送数据包
            socket.send(datagramPacket);

            //接收响应
            DatagramPacket datagramPacket1=new DatagramPacket(new byte[4096],4096);
            socket.receive(datagramPacket1);
            String s1=new String(datagramPacket1.getData(),0,datagramPacket1.getLength());

            //打印响应
            System.out.println(s1);
        }

    }

    public static void main(String[] args) throws IOException {
        UdpechoClient client=new UdpechoClient("127.0.0.1",9090);
        client.start();
    }
}
