package Network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

//udp服务器
public class UdpechoServer {
    private DatagramSocket socket = null;

    public UdpechoServer(int port) throws SocketException {
        this.socket = new DatagramSocket(port);//此时服务器需要有一个端口号，来标识

    }

    public void start() throws IOException {
        System.out.println("启动服务器!");
        while (true) {
            // 创建一个数据报用于接受数据
            DatagramPacket datagramPacket = new DatagramPacket(new byte[4096], 4096);
            //这里的 datagramPacket 是一个输出型参数
            socket.receive(datagramPacket);// 如果没有请求的时候这里会阻塞

            // 接收到的数据报需要转化为一个字符串有利于接下来的响应操作
            String request = new String(datagramPacket.getData(), 0, datagramPacket.getLength());// 第一个参数的作用  Returns the data buffer

            // 执行响应操作
            String response = process(request);

            //返回响应
            DatagramPacket datagramPacket1 = new DatagramPacket(response.getBytes(),response.getBytes().length, datagramPacket.getSocketAddress());
            socket.send(datagramPacket1);

            //打印响应日志
            System.out.printf("[%s:%d],req= %s,res= %s", datagramPacket.getAddress(), datagramPacket.getPort(), request, response);

        }

    }

    //此处为回显服务器，创建服务器中的 hello.txt world
    public String process(String s) {
        return s;
    }

    public static void main(String[] args) throws IOException {
        UdpechoServer server = new UdpechoServer(9090);
        server.start();

    }
}
