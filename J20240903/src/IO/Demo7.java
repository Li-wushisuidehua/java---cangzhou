package IO;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Demo7 {
    public static void main(String[] args) throws IOException {
        try(OutputStream outputStream=new FileOutputStream("./hello",true)){
            outputStream.write(0x68);
            outputStream.write(0x65);
            outputStream.write(0x6c);
            outputStream.write(0x6c);
            outputStream.write(0x35);//每一次写操作都是写后面
            outputStream.flush();

        }
    }
}
