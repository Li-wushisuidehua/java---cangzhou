package IO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Demo9 {
    public static void main(String[] args) throws IOException {
        try(InputStream in=new FileInputStream("./hello")){
            Scanner scanner=new Scanner(in);
            System.out.printf("%d",scanner.nextInt());
        }
    }
}
