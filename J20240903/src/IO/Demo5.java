package IO;

import java.io.*;

public class Demo5 {
    public static void main1(String[] args) throws IOException{
            try (OutputStream os = new FileOutputStream("output.txt")) {
                os.write('H');
                os.write('e');
                os.write('l');
                os.write('l');
                os.write('o');
                // 不要忘记 flush
                os.flush();
            }
    }

    public static void main(String[] args) throws IOException {
        try(OutputStream os=new FileOutputStream("./hello",true)){
//            os.write('h');
//            os.write(98);
//            os.write(97);
            os.write('h');
            os.write('e');
            os.write('l');
            os.write('l');
            os.write('o');
        }
    }
}
