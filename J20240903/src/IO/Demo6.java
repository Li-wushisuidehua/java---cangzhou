package IO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Demo6 {
    public static void main(String[] args) throws IOException {
        try (InputStream in=new FileInputStream("./hello")){
            int len=0;
            byte[] bytes=new byte[1024];
            while(true){
                len=in.read(bytes);
                if(len==-1){
                    break;
                }
                for (int i = 0; i < len; i++) {
                    System.out.printf("0x%x\n",bytes[i]);// 十六进行形式打印
                }
            }

        }

    }
}
