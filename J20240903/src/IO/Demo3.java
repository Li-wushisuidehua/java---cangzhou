package IO;

import java.io.*;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Demo3 {
    public static void main(String[] args) throws IOException {
        File file=new File("./hello");
       try(InputStream inputStream=new FileInputStream(file)){
           //从这里读取数据但是可能数据量大，一次读不完
           byte[] bytes=new byte[1024];
           int len=0;
           while(true){
               len= inputStream.read(bytes);
               if(len==-1){
                   break;
               }
           /*
           1. 输出的第一种方法
           String s=new String(bytes,0,len); //将字节数组转化为字符串
            System.out.print(s);
            */

               // 2. 输出的第二种方法
               for (int i = 0; i < len; i++) {
                   System.out.printf("%c",bytes[i]);
                }
          }
       }

    }
}
