package IO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

//利用Scanner对字符进行读取
public class Demo4 {
    public static void main(String[] args) throws IOException {
        try(InputStream inputStream=new FileInputStream("./hello")){
            Scanner scan=new Scanner(inputStream,"UTF-8");
            while(scan.hasNext()){
                String s=scan.next();
                System.out.print(s);
            }
        }
    }
}
