package IO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Demo2 {
    public static void main(String[] args) throws IOException {
        try (InputStream is = new FileInputStream("./hello")) {
            while (true) {
                int b = is.read();
                if (b == -1) {
                    // 代表⽂件已经全部读完
                    break;
                }

                System.out.printf("%c", b);
            }
        }
    }
}
