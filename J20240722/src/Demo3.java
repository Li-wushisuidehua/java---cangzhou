import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Demo3 {
   private static Read1 in=new Read1();
    public static void main(String[] args) throws Exception {
         int n=in.nextInt();
         int [][] arr=new int[n][6];
         for(int i=0;i<n;i++){
             for (int j = 0; j < 6; j++) {
                 arr[i][j]=in.nextInt();
             }
             Arrays.sort(arr[i]);
         }
         int left=0;
         int right=0;
         int max=0;
        for (int i = 0; i < n; i++) {
            left = 0;
            right = 4;
            max = arr[i][5];
            while (left < right) {
                if (arr[i][left] + arr[i][right] > max && arr[i][left] > (max - arr[i][right])) {
                    if (left == 0) {
                        if (arr[i][left + 1] + arr[i][left + 2] > arr[i][left + 3] &&
                                arr[i][left + 1] > (arr[i][left + 3] - arr[i][left + 2])) {
                            System.out.println("Yes");
                           break;
                        }else{
                            System.out.println("No");
                            break;
                        }
                    } else {
                        if (right - left - 1 == 0) {
                            if (arr[i][0] + arr[i][1] > arr[i][2] &&
                                    arr[i][0] > (arr[i][2] - arr[i][1])) {
                                System.out.println("Yes");
                                break;
                            }else{
                                System.out.println("No");
                                break;
                            }
                        } else if ((right - left - 1) > 1) {
                            if (arr[i][0] + arr[i][left + 1] > arr[i][left + 2] &&
                                    arr[i][0] > (arr[i][left + 2] - arr[i][left + 1])) {
                                System.out.println("Yes");
                                break;
                            }else{
                                System.out.println("No");
                                break;
                            }
                        } else if((right-left-1)==1){
                            if (arr[i][0] + arr[i][1] > arr[i][left + 1] &&
                                    arr[i][0] > (arr[i][left + 1] - arr[i][1])) {
                                System.out.println("Yes");
                              break;
                            }else{
                                System.out.println("No");
                                break;
                            }
                        }
                    }
                } else {
                    left++;
                }
            }

        }

    }
}
//快速写入的函数
class Read1{
    StringTokenizer st=new StringTokenizer(" ");//裁剪字符串
    BufferedReader bf=new BufferedReader(new InputStreamReader(System.in));//
     String next() throws IOException{
         while(!st.hasMoreTokens()){
             st=new StringTokenizer(bf.readLine());
         }
         return st.nextToken();
     }
     String nextLine() throws Exception{
         return bf.readLine();
     }
     int nextInt() throws Exception{
         return Integer.parseInt(next());
     }
     long nextLong() throws Exception{
         return Long.parseLong(next());
     }
     double nextDouble() throws Exception{
         return Double.parseDouble(next());
     }
}
