import java.util.*;
import java.io.*;
public class Demo1 {
    //快速输出的书写
    public static PrintWriter out=new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
    public static Read in=new Read();

    public static void main(String[] args) throws Exception {
//         int m=in.nextInt();
//         out.println(m);
        int m= Integer.parseInt(in.next());//in.next()输入后为字符串，Integer.parsInt(xxx)是将字符串转化为有效的数字，如果字符串转化出来后不为
        //有效的整数会报错。

        String str=in.next();
        String str1=in.nextLine();
        //注意上面的next()和nextLine()方法，前面的next()如果在输入的字符串中出现了空格、tab键或者换行符视为分隔符，会截取空格前的内容，
        // 如果为nextLine()会遇到Enter键后才会停止
        //eg:输入 "aaa   bbb" (Enter)如果是前者的方法输出的结果为 aaa ，而后者输出的结果为 aaa   bbb

        out.println(str1);
         out.close();//在输入、输出之后一定要加上 close 代码检测好像只有这样才会正确的输出
    }
}
//快速输入的大框架的生成
class Read{
    //字符串裁剪
    StringTokenizer st=new StringTokenizer(" ");
    //缓冲区的建立
    BufferedReader bf=new BufferedReader(new InputStreamReader(System.in));

    //token：标记
    String next() throws IOException{
        while(!st.hasMoreTokens()){//接下来是否还有元素需要裁剪
            st= new StringTokenizer(bf.readLine());//读取一行，StringTokenizer用于将字符串裁剪为更多的标记
        }
        return st.nextToken();//返回根据空格裁剪后的内容
    }
    String nextLine() throws Exception{
        return bf.readLine();//读一行
    }
    int nextInt() throws Exception{
        return Integer.parseInt(next());//如果解析出来的字符串不是有效的整数表示，则会抛出异常NumberFormatException的异常。
    }
    long nextLong() throws Exception{
        return Long.parseLong(next());
    }
    double nextDouble() throws Exception{
        return Double.parseDouble(next());
    }
}

