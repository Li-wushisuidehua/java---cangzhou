package Demo1;

public class Test {
    static class Student{//内部类前面写static;
        public String name="美丽";
        public int age=19;
        {
             String color="黄色";//实例代码块有助于代码的共享;
            System.out.println("正在执行实例代码块");

        }
        public void eat(){
            System.out.println(this.name+"正在吃饭");
        }
        public void play(String name){
            System.out.println(this.name+"正在和"+name+"一起玩耍");//体现了this的重要性
        }

    }
}
class Test1{
    public static void main(String[] args) {
        Test.Student student=new Test.Student();
        String name="小明";
        student.play(name);
    }
}
