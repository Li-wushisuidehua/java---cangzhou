package Demo2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

//实现栈的方法
public class Test {
    public static void main2(String[] args) {
        List<Integer> stack = new LinkedList<>();//用链表实现栈,且LinkedList为双向链表
        stack.add(1);
        stack.add(5);//add()是使用的尾插法
        stack.add(9);
        stack.add(7);
        System.out.println(stack);//输出的结果为[1,5,9,7]
        stack.set(2, 6);//是根据下标重新给之前下标从新赋值
        System.out.println(stack);//输出的结果为[1.5.6.7]
        stack.remove(2);
        System.out.println(stack);//输出的结果为[1,5,7],removw是根据下标来删除的元素

    }

    /*
    Deque继承了Queue，双端队列继承了队列
     */public static void main4(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.push(4);//实现了Deque接口，Deque为双端队列
        linkedList.add(5);//实现了list接口
        System.out.println(linkedList);//输出的结果为[4,5]
        linkedList.offer(7);//实现了Qeque接口，Queue为队列
        System.out.println(linkedList);//输出的结果为[4,5,7]
    }

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(3);
        arrayList.add(5);
        arrayList.add(7);
        arrayList.add(9);
        System.out.println(arrayList.isEmpty());//判断顺序表是否为空
        System.out.println(arrayList);//输出的结果为[3,5,7,9]说明此时一定重写了toString方法
        arrayList.remove(3);
        System.out.println(arrayList);//输出的结果为[3,5,7]
    }

    //直接使用类，但是Stack这个类已经过时了，不推荐使用
    public static void main1(String[] args) {
        Stack<Integer> stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(3);//压栈
        stack.add(9);
        System.out.println(stack.peek());//查看栈顶元素
        System.out.println(stack.pop());//弹出栈顶元素；
        System.out.println(stack.peek());//此时输出的结果为 3 3 2代表peek只是查看一下元素的内容，但是不会对元素的内容进行修改
        System.out.println(stack.empty());//判断栈是否为空，输出结果为false因为很明显这里可以看出栈中元素结果不为空

    }
}
