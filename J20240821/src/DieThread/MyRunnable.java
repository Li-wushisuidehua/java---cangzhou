package DieThread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyRunnable implements Runnable{
    Lock lock=new ReentrantLock();
    int count=0;
    @Override
    public void run() {
        while(true){
            lock.lock();
            if(count==10){
                break;
            } else{
                System.out.println(count);
                count++;
            }
            lock.unlock();
        }
      //  System.out.println("hh");
    }
}
