import java.util.concurrent.TimeUnit;

public class Demo1 {
    public static void main(String[] args) {
        MyRunnale mr=new MyRunnale();
        Thread t1=new Thread(mr);
        Thread t2=new Thread(mr);

        t1.setName("线程1");
        t2.setName("线程2");

        //启动线程
        t1.start();
        t2.start();

    }
}
