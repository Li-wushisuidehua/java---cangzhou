public class MyRunnale implements Runnable{
    long count=0;//count为共享数据，但是此时只会实例化一个变量
    @Override
    public void run() {
        //注意此时的i为局部变量，不存在数据竞争的问题
            for (int i = 0; i <10000; i++) {
                synchronized (MyRunnale.class){
                System.out.println(Thread.currentThread().getName()+"----"+count);
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    count++;
            }
        }
    }
}
