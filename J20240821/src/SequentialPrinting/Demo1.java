package SequentialPrinting;

public class Demo1 {
    public static void main(String[] args) {
        Object locker1=new Object();
        Object locker2=new Object();
        Object locker3=new Object();
        Thread t1=new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.print("C");
                try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }

                synchronized (locker2){
                    locker2.notifyAll();
                }
                synchronized (locker1) {
                    try {
                        locker1.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

        });
        Thread t2=new Thread(()->{
            for (int i = 0; i < 10; i++) {
                synchronized (locker2){
                    try {
                        locker2.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.print("B");
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                synchronized (locker3){
                    locker3.notifyAll();
                }
            }
        });
        Thread t3=new Thread(()->{
            for (int i = 0; i < 10; i++) {
                synchronized (locker3){
                    try {
                        locker3.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("A");

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                synchronized (locker1){
                    locker1.notifyAll();
                }
            }

        });

        t1.setName("a");
        t2.setName("b");
        t3.setName("c");

        t1.start();
        t2.start();
        t3.start();
    }
}
