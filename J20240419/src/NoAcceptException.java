public class NoAcceptException extends RuntimeException{
    public NoAcceptException() {

    }

    public NoAcceptException(String message) {
        super(message);
    }
}
