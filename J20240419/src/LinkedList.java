// 2、无头双向链表实现
public class LinkedList {
   static class ListNode{
        int val;
        ListNode pre;
        ListNode post;

        public ListNode(int val) {
            this.val = val;
        }
    }
    //双向链表可以设置头节点和尾节点
   public ListNode head;
   public ListNode list;

    //头插法,没有使用尾巴节点的
    public void addFirst(int data){
       if(head==null){
           //这里如果使用data.val，此时的head为空，head.val会报
           //head.val=data;
           head=new ListNode(data);
           head.pre=head.post=null;
           return ;
       }
       ListNode cur=new ListNode(data);
       cur.post=head;
       head.pre=cur;
       head=cur;
    }
    //头插法的第二种优化方法，使用了头节点和尾巴节点
    public void addFirst2(int data){
        ListNode node=new ListNode(data);
        if(head==null){
       list=head=node;
        }else{
            node.post=head;
            head.pre=node;
            head=node;
        }

    }
    //尾插法，没有使用尾巴节点
    public void addLast(int data){
        if(head==null){
            head=new ListNode(data);
            head.pre=head.post=null;
            return ;
        }
        ListNode cur=head;
        ListNode curN=new ListNode(data);
        while(cur.post!=null){
            cur=cur.post;
        }
        cur.post=curN;
        curN.pre=cur;

    }
    //尾插法的第二种优化后的方法，使用了头节点和尾巴节点
    public void addLast2(int data){
        ListNode node=new ListNode(data);
        if(head==null){
         head=list=node;
        }else{
            list.post=node;
            node.pre=list;
            list=list.post;
        }
    }
    //任意位置插入,第一个数据节点为0号下标
    public boolean addIndex(int index,int data){
        ListNode cur=head;
        for (int i = 0; i < size(); i++) {
            if(i==index){//寻找到需要更改的下标
                ListNode now=new ListNode(data);
                cur.pre.post=now;
                now.pre=cur.pre;
                now.post=cur;
                cur.pre=now;
                return true;
            }
            cur=cur.post;
        }
        return false;
    }
    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){
        if(head==null){
          throw new NoAcceptException("该单链表为空因此该元素不存在！");
        }
        ListNode cur=head;
        while(cur!=null){
            if(cur.val==key){
                return true;
            }
            cur=cur.post;
        }

        return false;
    }
    //删除第一次出现关键字为key的节点
    public void remove(int key){
        if(head==null){
            return ;
        }
        if(head.val==key){
            head=head.post;
            return;
        }
        if(list.val==key){
            list.pre.post=null;
            return;
        }
        ListNode cur=head;
        while(cur!=null){
            if(cur.val==key){
                cur.pre.post=cur.post;
                cur.post.pre=cur.pre;
                return ;
            }
            cur=cur.post;
        }

    }

    //删除所有值为key的节点
    public void removeAllKey(int key){
//        if(head==null){
//            return ;
//        }
//        ListNode cur=head;
//        while(cur!=null){
//            if(cur.val==key){
//                cur.pre.post=cur.post;
//                cur.post.pre=cur.pre;
//            }
//            cur=cur.post;
//        }
//        if(head.val==key){
//            head=head.post;
//        }

            if(head==null){
                return ;
            }
            ListNode cur=head;
            ListNode curN=head.post;
            while(curN!=null){
                if(curN.val==key){
                cur.post=curN.post;
                cur=cur.post;
                }else{
                    cur=cur.post;
                }
                curN=curN.post;
                if(head.val==key){
                    head=head.post;
                }
            }


    }
    //得到单链表的长度
    public int size(){
        int count=0;
        if(head==null){
            return count;
        }
        ListNode cur=head;
        while(cur!=null){
            count++;
            cur=cur.post;
        }
        return count;
    }
    public void display(){
        ListNode cur=head;
        while(cur!=null){
            System.out.print(cur.val+" ");
            cur=cur.post;
        }
        System.out.println();
    }
    //清空双向链表
    public void clear(){
        //head=null;
        ListNode cur=head;
        ListNode curN;
        while(cur!=null){
            curN=cur.post;
            cur.post=null;
            cur=curN;

        }
        head=null;

    }
}
