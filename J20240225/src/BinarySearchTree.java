public class BinarySearchTree {
    //实现结点
    static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    //定义根节点
    public TreeNode root;

    //查找元素是否在搜索二叉树当中
    public TreeNode Search(int val) {
        if (root == null) {
            return root;
        }
        TreeNode cur = root;
        while (cur != null) {
            if (cur.val < val) {
                cur = cur.right;
            } else if (cur.val > val) {
                cur = cur.left;
            } else {
                return cur;//此时代表
            }
        }
        return null;
    }

    //在搜索二叉树中插入元素
    public void Insert(int val) {
        TreeNode node = new TreeNode(val);
        if (root == null) {
            root = node;
        }
        TreeNode cur = root;
        TreeNode parent = null;
        while (cur != null) {
            parent = cur;
            if (cur.val < val) {
                cur = cur.right;
            } else if (cur.val > val) {
                cur = cur.left;
            } else {
                return;
            }
        }
        if (parent.val > val) {
            parent.left = node;
        } else {
            parent.right = node;
        }
    }

    //在二叉搜索树中删除元素
    public TreeNode Delete(int val) {
        //先找到元素存储在二叉搜素树中的位置
        if (root == null) {
            return null;
        }
        TreeNode parent = null;//如果parent没有进去的话，那么此时传回去的parent是没有值的，是不可以的
        TreeNode cur = root;
        while (cur != null) {
            if (cur.val < val) {
                parent = cur;
                cur = cur.right;
            } else if (cur.val > val) {
                parent = cur;
                cur = cur.left;
            } else {
                //代表此时找到了想要删除的元素
                RemoveNode(parent, cur);
                break;
            }
        }
        return cur;//如果这里返回的时parent是他的父母的值
    }

    public void RemoveNode(TreeNode parent, TreeNode cur) {
        //删除元素时，分为左边为空，右边为空，左右两边均不为空
        if (cur.left == null) {
            if (cur == parent) {
                parent = cur.right;
            } else if (cur == parent.left) {
                parent.left = cur.right;
            } else {
                parent.right = cur.right;
            }
        } else if (cur.right == null) {
            if (cur == parent) {
                parent = cur.left;
            } else if (cur == parent.left) {
                parent.left = cur.left;
            } else {
                parent.right = cur.left;
            }
        } else if (cur.left != null && cur.right != null) {
            //找到右边子树的最小值或者左边子树的最大值
            TreeNode targetP = cur;
            TreeNode target = cur.right;
            while (target.left != null) {
                targetP = target;
                target = target.left;
            }
            cur.val = target.val;
            if (target == targetP.left) {
                targetP.left = target.right;
            } else {
                targetP.right = target.right;
            }
        }

    }

    public TreeNode Convert() {
        InOrderTraversal(root);
        return root;
    }

  
    public TreeNode InOrderTraversal(TreeNode root) {
        if(root==null){
            return root;
        }
        TreeNode cur=root;
        InOrderTraversal(cur.left);
        if(cur.left==null){

        }

    }

}
