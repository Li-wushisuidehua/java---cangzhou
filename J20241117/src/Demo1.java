import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        int m=scan.nextInt();
        int []dp=new int[10000];
        dp[0]=1;dp[1]=2;
        for (int i = 2; i < m; i++) {
            dp[i]=dp[i-1]+dp[i-2];
        }
        System.out.println(dp[m-1]);
    }
}
