package Demo1;

class  Money implements Cloneable{
        public double m = 99.99;

        public static void main(String[] args)throws CloneNotSupportedException{
            Money m2=new Money();
            double m1=(double)m2.clone();
        }

//        @Override
//        protected Object clone() throws CloneNotSupportedException {
//            return super.clone();
//        }
    }
    class Person implements Cloneable{
        public Money money = new Money();
        @Override
        protected Object clone() throws CloneNotSupportedException {
            Person tmp=(Person) super.clone();
            return tmp;
        }
    }
    public class TestDemo3 {
        public static void main(String[] args) throws CloneNotSupportedException {
            //浅拷贝
            Person person1 = new Person();
            Person person2 = (Person) person1.clone();
            System.out.println("通过person2修改前的结果");
            System.out.println(person1.money.m);
            System.out.println(person2.money.m);
            person2.money.m = 88.88;
            System.out.println("============");
            System.out.println("通过person2修改后的结果");
            System.out.println(person1.money.m);
            System.out.println(person2.money.m);
            //深拷贝
            Person person3=new Person();//实现克隆
            System.out.println(person1.money.m);
            System.out.println(person3.money.m);
            System.out.println("=============");
            person3.money.m=28;
            System.out.println(person1.money.m);
            System.out.println(person3.money.m);



        }
    }

