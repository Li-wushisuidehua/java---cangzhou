package Demo4;

//简单拆箱，装箱处理
public class Test {
    //装箱
    public static void main1(String[] args) {
        int a = 10;
        // Integer i=new Integer(a);//出现画红线的原因是因为这个方法已经过时了;
        Integer i = Integer.valueOf(a);//将a转变成为了字符串;   显示装箱   valueof是类里面的方法
        Integer ii = a;//自动装箱，自动装箱底层调用该的也是valuof

    }

    //拆箱
    public static void main2(String[] args) {
        Integer a = 4;
        int a1 = a;//自动拆箱，其实自动拆箱底层也是调用的intValue();
        int a2 = a.intValue();//显示拆箱
        System.out.println("a1:"+a1);
        System.out.println("a2:"+a2);


    }

    public static void main(String[] args) {
        Integer a=100;
        Integer b=100;
        System.out.println(a==b);
        System.out.println("===========");
        Integer c=200;
        Integer d=200;
        System.out.println(c==d);
    }

}
