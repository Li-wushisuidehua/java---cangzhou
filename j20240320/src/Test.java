import java.util.Objects;


public class Test{
//    @Override
//    public String toString() {//重写了这个方法，重写的方法是需要调用的，才会实现。
//        return "haha";
//    }
  public int age;

//    @Override
//    public String toString() {
//        return "Test{" +
//                "age=" + age +
//                ", name='" + name + '\'' +
//                '}';
//    }
    public String toString(){
        return this.name+"  是他的名字"+this.age+"  是他的年龄";
    }

    public String name;

    public Test(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public static void main(String[] args) {
        Test test=new Test(18,"meili");
        System.out.println(test);
    }
}
