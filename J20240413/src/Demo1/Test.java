package Demo1;

public class Test {
    public static void main(String[] args) {
        MyLinkedList linkedList = new MyLinkedList();
        linkedList.add(2);
        linkedList.add(1);
        linkedList.add(4);
        linkedList.add(3);
        linkedList.add(9);
        linkedList.add(7);
        linkedList.displayList();
       // linkedList.splitList(5);
       MyLinkedList.Node newnode=linkedList.splitList(5);
        System.out.println("===========");
        linkedList.displayList(newnode);

    }


}

class MyLinkedList {
    //定义节点
    static class Node {
        public int val;
        public Node next;

        public Node(int val) {
            this.val = val;
        }
    }

    public Node head;

    public void add(int x) {
        Node cur = new Node(x);//这里创建新节点时只为val赋值了，next没有赋值，默认next为null;
        if (head == null) {
            head = cur;
        } else {//这里还可以优化，必须想出可优化的代码
            Node now = head;
            while (now.next != null) {
                now = now.next;
            }
            now.next = cur;
        }

    }

    public void displayList() {
        Node cur = head;
        while (cur != null) {
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }

    //分割链表，将大于x的放左边，小于x的放右边
    public Node  splitList(int x) {
        if(head==null){
            return head;
        }
        Node cur=head;
        Node left=null;
        Node right=null;
        Node endleft=null;
        Node endright=null;
        while(cur!=null){
            if(cur.val<x){
                if (left == null) {
                    left=endleft=cur;
                } else {//这里还可以优化，必须想出可优化的代码
                   endleft.next = cur;
                   endleft=endleft.next;
                }

            }else {
                if (right == null) {
                    right = endright=cur;
                } else {//这里还可以优化，必须想出可优化的代码
                   endright.next = cur;
                   endright=endright.next;
                }
            }
            cur=cur.next;
        }
        if(left==null){//说明只有右边有
           return right;
        }
        if(right==null) {//说明只有左边有
            return left;
        }
        endleft.next=right;
        endright.next=null;
        return left;


      //  Node left=new Node();
      //  Node right=new Node();

    }
    public void displayList(Node node) {
        Node cur = node;
        while (cur != null) {
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }
}
