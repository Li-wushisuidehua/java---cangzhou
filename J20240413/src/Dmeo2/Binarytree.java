package Dmeo2;

import com.sun.source.tree.Tree;

import java.util.*;

//类中 方法外 的为 成员变量 或者 方法
public class Binarytree {
    //普通方法创建一颗二叉树
    static class Treenode {
        public int val;
        public Treenode left;
        public Treenode right;

        public Treenode(int val) {
            this.val = val;
        }
    }

    public Treenode creatTree() {
        //Treenode node=new Treenode(1);
        Treenode node1 = new Treenode(2);
        Treenode node3 = new Treenode(3);
        Treenode node4 = new Treenode(4);
        Treenode node5 = new Treenode(5);
        Treenode node6 = new Treenode(6);
        Treenode node7 = new Treenode(7);
        node1.left = node3;
        node1.right = node4;
        node3.left = node5;
        node3.right = node6;
        node4.left = node7;
        return node1;
    }


/*
    //从前序遍历和中序遍历构建一棵二叉树
    //前序遍历和中序遍历分别为 preorder = [3,9,20,15,7],  inorder = [9,3,15,20,7]
    public Treenode buildTree(int[] preorder, int[] inorder) {
        //因为这里需要有的参数为5个，同时又需要使用递归来进行，所以说只能重写一个方法来实现所需要的功能，以便实现函数的调用
        return buildTreeChild(preorder,inorder,0,inorder.length-1);

    }
    public int preIndex;
    public Treenode buildTreeChild(int []preorder,int []inorder,int Inbegin,int Inend){
        if(Inbegin>Inend){
            return null;
        }
        Treenode root=new Treenode(preorder[preIndex]);
        int rootIndex=findIndex(inorder,Inbegin,Inend,preorder[preIndex]);
        preIndex++;
        root.left=buildTreeChild(preorder,inorder,Inbegin,rootIndex-1);
        root.right=buildTreeChild(preorder,inorder,rootIndex+1,Inend);
        return root;

    }
   //查找每棵树根节点在中序遍历中的位置
    public int findIndex(int []inorder,int begin,int end,int key){
        for (int i = begin; i <=end; i++) {
            if(inorder[i]==key){
                return i;
            }
        }
        return -1;//如果没有相等的
   }
 */
/*
  public int postIndex;
    //从中序遍历和后序遍历创建一棵二叉树，中序:badce,后序:bdeca
    //inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
    public Treenode buildTree(int[] inorder, int[] postorder) {
        postIndex=postorder.length-1;
        return buildTreeChild(inorder,postorder,0,inorder.length-1);

    }

    public Treenode buildTreeChild(int []inorder,int []postorder,int Inbegin,int Inend){
        if(Inbegin>Inend){
            return null;
        }
        Treenode root=new Treenode(postorder[postIndex]);
        int rootIndex=findIndex(inorder,Inbegin,Inend,postorder[postIndex]);
        postIndex--;
        root.right=buildTreeChild(inorder,postorder,rootIndex+1,Inend);
        root.left=buildTreeChild(inorder,postorder,Inbegin,rootIndex-1);
        return root;
    }
    //查找每棵树根节点在中序遍历中的位置
    public int findIndex(int []inorder,int begin,int end,int key){
        for (int i = begin; i <=end; i++) {
            if(inorder[i]==key){
                return i;
            }
        }
        return -1;//如果没有相等的
    }
 */


    /*
         根据二叉树写出其对应的字符串形式
        public String tree2str(Treenode root) {
            StringBuilder stringBuilder=new StringBuilder();//stringBuilder.append()会自动创建一个字符串，保存在stringBuilder中。
            tree2strChild(root,stringBuilder);
            return stringBuilder.toString();

        }
        private void tree2strChild(Treenode root,StringBuilder stringBuilder){
            if(root==null){
                return ;
            }
            stringBuilder.append(root.val);
            if(root.left!=null){
                stringBuilder.append("(");
                tree2strChild(root.left,stringBuilder);
                stringBuilder.append(")");
            }else{
                if(root.right==null){
                    return ;//这里回退到的是第29行
                }else{
                    stringBuilder.append("()");//右边不为0
                }
            }
            if(root.right==null){
                return ;
            }else
            {
                stringBuilder.append("(");
                tree2strChild(root.right,stringBuilder);
                stringBuilder.append(")");
            }

        }
     */
//不使用递归实现前序遍历
    public void preOrder(Treenode root) {
        if (root == null) {
            return;//本身是想要实现前序遍历，如果此时root为空，那么没有必要对其进行排序，直接返回root即可
        }
        Stack<Treenode> stack = new Stack<>();
        Treenode cur = root;
        while (cur != null || !stack.empty()) {
            while (cur != null) {
                stack.push(cur);
                System.out.print(cur.val + " ");//这里对其进行了打印；
                cur = cur.left;
            }
            Treenode top = stack.pop();
            cur = top.right;
        }
    }

    //以数组的形式返回已经遍历好的前序遍历并且已经将元素存储在了ArrayList所在的数组中
    /*
        public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> list=new ArrayList<Integer>();
        if(root==null){
            return list;
        }
        Stack<TreeNode> stack=new Stack<>();
        TreeNode cur=root;
        while(cur!=null || !stack.empty()){
            while(cur!=null){
                stack.push(cur);
                list.add(cur.val);
                cur=cur.left;
            }
            cur=stack.pop();
            cur=cur.right;

        }
        return list;

    }
     */
    //使用无递归的中序遍历遍历二叉树，并将其以数组的形式返回，
    //不以数组的形式返回便是将其add()换成打印
    /*
        public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list=new ArrayList<>();//List只是一个接口
        if(root==null){
            return list;
        }
        TreeNode cur=root;
        Stack<TreeNode> stack=new Stack<>();
        while(cur!=null || !stack.empty()){
            while(cur!=null){
                stack.push(cur);
                cur=cur.left;
            }
            cur=stack.pop();
            list.add(cur.val);
            cur=cur.right;
        }
return list;
    }
     */
    //不以递归的方式后序遍历二叉树，并将其打印
    //后序遍历，左、右、根
    /*
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list=new ArrayList<>();//List只是一个接口
        if(root==null){
            return list;
        }
        TreeNode cur=root;
        TreeNode pre=null;
        Stack<TreeNode> stack=new Stack<>();
        while(cur!=null || !stack.empty()){
            while(cur!=null){
                stack.push(cur);
                cur=cur.left;
            }
          TreeNode top=stack.peek();
            if(top.right==null || top.right==pre){
            stack.pop();//将栈中的元素弹出
            list.add(top.val);
            pre=top;
            }else{
            cur=top.right;//如果当top.right!=null时，会将top.right赋值给cur
            }
        }
        return list;
    }

     */
    //判断一棵树是否是平衡二叉树
    //平衡二叉树：是指该树所有节点的左右子树的深度相差不超过 1。
    public boolean isBalanced(Treenode root) {
        if (root == null) {
            return true;
        }
        int leftH = isBalancedTest(root.left);
        int rightH = isBalancedTest(root.right);
        return Math.abs(rightH - leftH) <= 1
                && isBalanced(root.left)
                && isBalanced(root.right);

    }

    public static int isBalancedTest(Treenode root) {
        if (root == null) {
            return 0;
        }
        Treenode cur = root;
        int lefthigh = isBalancedTest(cur.left);
        int righthight = isBalancedTest(cur.right);
        return lefthigh > righthight ? lefthigh + 1 : righthight + 1;
    }

    //求一颗二叉树的深度,高度
    public int getHeight(Treenode root) {
        if (root == null) {
            return 0;
        }
        Treenode cur = root;
        int lefthigh = getHeight(cur.left);
        int righthigh = getHeight(cur.right);
        return lefthigh > righthigh ? lefthigh + 1 : righthigh + 1;

    }

    //判断一棵二叉树是否为对称二叉树
    public boolean isSymmetric(Treenode root) {
        if (root == null) {
            return true;
        }
        return isSymmetricTest(root.left, root.right);
    }

    public boolean isSymmetricTest(Treenode rootleft, Treenode rootright) {
        if (rootleft == null && rootright != null || rootleft != null && rootright == null) {
            return false;
        }
        if (rootleft == null && rootright == null) {
            return true;
        }
        if (rootleft.val != rootright.val) {
            return false;
        }
        return isSymmetricTest(rootleft.right, rootright.left) &&
                isSymmetricTest(rootleft.left, rootright.right);

    }

    //寻找另一棵树的子树
    public boolean isSubtree(Treenode root, Treenode subRoot) {
        if (root == null) {//最初的时候如果root等于null
            return false;
        }
        if (IssameTree(root, subRoot)) {
            return true;
        }
        if (isSubtree(root.left, subRoot)) {
            return true;
        }
        if (isSubtree(root.right, subRoot)) {
            return true;
        }
        return false;
    }

    public boolean IssameTree(Treenode root, Treenode subRoot) {
        if (root == null && subRoot != null || root != null && subRoot == null) {
            return false;
        }
        if (root == null && subRoot == null) {
            return true;
        }
        if (root.val != subRoot.val) {
            return false;
        }
        return IssameTree(root.left, subRoot.left) &&
                IssameTree(root.right, subRoot.right);
    }

    //前序遍历二叉树
    public void preOrder2(Treenode root) {
        if (root == null) {
            return;
        }
        Treenode cur = root;
        System.out.print(cur.val + " ");
        preOrder2(cur.left);
        preOrder2(cur.right);
    }

    //中序遍历二叉树
    public void inOrder(Treenode root) {
        if (root == null) {
            return;
        }
        Treenode cur = root;
        inOrder(cur.left);
        System.out.print(cur.val + " ");
        inOrder(cur.right);
    }

    //后序遍历二叉树
    public void postOder(Treenode root) {
        if (root == null) {
            return;
        }
        Treenode cur = root;
        postOder(cur.left);
        postOder(cur.right);
        System.out.print(cur.val + " ");
    }

    public int count;

    //获取二叉树中节点的个数，可以通过遍历全部的二叉数，求个数
    public int catchNode(Treenode root) {
        //根据前序遍历对其进行计数
        if (root == null) {
            return 0;//表示节点个数一个没有，说明节点为0个
        }

        return catchNode(root.left) + catchNode(root.right) + 1;
    }

    public int catchNode2(Treenode root) {
        if (root == null) {
            return 0;
        }
        count++;
        catchNode(root.left);
        catchNode(root.right);
        return count;
    }

    public int leafSzie;

    //获取二叉树中叶子节点的个数
    public int getLeafNodeCount(Treenode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
//        Treenode cur=root;
        return getLeafNodeCount(root.left) + getLeafNodeCount(root.right);
    }

    public int getLeafNodeCount2(Treenode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            leafSzie++;
        }
//       Treenode cur=root;
        getLeafNodeCount2(root.left);
        getLeafNodeCount2(root.right);
        return leafSzie;
    }

    //获取二叉树第k层节点的个数
    public int getKLevelNodeCount(Treenode root, int k) {
        if (root == null) {
            return 0;
        }
        if (k == 1) {
            return 1;
        }
        return getKLevelNodeCount(root.left, k - 1) + getKLevelNodeCount(root.right, k - 1);

    }

    //查找某个元素是否存储在当前的二叉树当中
    public Treenode find(Treenode root, int key) {
        if (root == null) {
            return null;
        }
        if (root.val == key) {
            return root;
        }
        Treenode ret = null;
        ret = find(root.left, key);//关键字有可能是在左子树找到的，也有可能是在右子树找到的
        if (ret != null) {
            return ret;
        }
        ret = find(root.right, key);
        if (ret != null) {
            return ret;
        }
        return ret;//如果在前面都没有返回值，那么说明最后也不会有存在的节点了，刚好ret默认的初始值也就是null
    }
    //层序遍历二叉树
    public void levelOrder(Treenode root){
        if(root==null){
            return ;
        }
        Queue<Treenode> queue=new ArrayDeque<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            Treenode cur=queue.poll();
            System.out.print(cur.val+" ");
            if(cur.left!=null){
                queue.offer(cur.left);
            }
            if(cur.right!=null){
                queue.offer(cur.right);
            }
        }

    }
    //在层序遍历的基础上进行分层遍历
    public static List<List<Integer>> levelOrder2(Treenode root){
        List<List<Integer>> lists=new ArrayList<>();//最初没有赋值时为 0
        if(root==null){
            return lists;
        }
        Queue<Treenode> queue=new ArrayDeque<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            Treenode cur=null;
            int size=queue.size();
            List<Integer> list=new ArrayList<>();
            while (size>0){
                cur=queue.poll();
                list.add(cur.val);
                if(cur.left!=null){
                    queue.offer(cur.left);
                }
                if(cur.right!=null){
                    queue.offer(cur.right);
                }
                size--;
            }
            lists.add(list);

        }
        return lists;
    }
// 二叉树的层序遍历 II
public static List<List<Integer>> levelOrderBottom(Treenode root){
    List<List<Integer>> lists=new ArrayList<>();//最初没有赋值时为 0
    if(root==null){
        return lists;
    }
    Queue<Treenode> queue=new ArrayDeque<>();
    queue.offer(root);
    while(!queue.isEmpty()){
        Treenode cur=null;
        int size=queue.size();
        List<Integer> list=new ArrayList<>();
        while (size>0){
            cur=queue.poll();
            list.add(cur.val);
            if(cur.left!=null){
                queue.offer(cur.left);
            }
            if(cur.right!=null){
                queue.offer(cur.right);
            }
            size--;
        }
        lists.add(0,list);

    }
    return lists;
}


    //判断一棵二叉树是否为完全二叉树
    public boolean isCompleteTree(Treenode root){
        if(root==null){
            return true;
        }
        Queue<Treenode> queue=new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            Treenode cur=queue.poll();
            if(cur==null){
                break;
            }
            queue.offer(cur.left);
            queue.offer(cur.right);
        }
        while (!queue.isEmpty()){
            Treenode cur=queue.peek();
            if(cur!=null){
                return false;
            }else{
                queue.poll();
            }
        }
        return true;

    }
    public boolean isCompleteTree2(Treenode root) {
        Queue<Treenode> queue = new LinkedList<>();
        if(root == null)
            return true;
        queue.offer(root);
        while (!queue.isEmpty()) {
            Treenode cur = queue.poll();
            if(cur == null) {
                break;
            }
            queue.offer(cur.left);
            queue.offer(cur.right);
        }
        while (!queue.isEmpty()) {
            Treenode node = queue.peek();
            if(node != null) {
                return false;
            }else {
                queue.poll();
            }
        }
        return true;
    }
    /*
        //二叉树的遍历创建一棵二叉树
//    abc##de#g##f###
   class TreeNode{
        public char val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(char val) {//构造方法；
            this.val = val;
        }
    }

    public class Main {
     public static int i=0;//定义为在类中方法外
        public static TreeNode createTree(String str){
             TreeNode node=null;
           if(str.charAt(i)!='#'){
                node=new TreeNode(str.charAt(i));
                i++;
                node.left=createTree(str);
                node.right=createTree(str);//虽然说这里传的是str，但是i的值一直在改变，每次传过去时，取的字符会不一样
            }else{
               i++;
           }
             return node;
        }
        public static void inOrder(TreeNode root){
            if(root==null){
                return ;
            }
            TreeNode cur=root;
            inOrder(cur.left);
            System.out.print(cur.val+" ");
            inOrder(cur.right);

        }

        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            // 注意 hasNext 和 hasNextLine 的区别
            while (in.hasNextLine()) { //hasNextLine是什么意思；
                String str=in.nextLine();
                TreeNode node=createTree(str);
                inOrder(node);

            }
        }
    }
     */


    //二叉树的最近的公共祖先
    public Treenode lowestCommonAncestor(Treenode root, Treenode p, Treenode q) {
        if(root==null){
            return root;
        }
        /*
        这里还可以写成
             if(root== p || root== q){
            return root;
        }
        1.首先这里的p和q本身就是依据这个二叉树原本有的节点所设置的
        2.因为节点是唯一的，所以说比较时使用root==p || root==q是可以的，这里使用的比较方法由于左右两边都是类类型所以比较的是地址
         */
        if(root.val== p.val && root.val!= q.val){
            return p;
        }
        if(root.val==q.val && root.val!=p.val){
            return q;
        }
        Treenode cur=root;
        Treenode left=lowestCommonAncestor(cur.left,p,q);
        Treenode right=lowestCommonAncestor(cur.right,p,q);
        if(left!=null && right!=null){
            return root;
        }
        if(left!=null ){
            return left;
        }else {
            return right;
        }
    }


}

class Test {
    public static void main(String[] args) {
        Binarytree binarytree = new Binarytree();
        Binarytree.Treenode root = binarytree.creatTree();
//        System.out.println(binarytree.catchNode(root));//节点数
//        binarytree.preOrder(root);
//       boolean ret=binarytree.isBalanced(root);
//        System.out.println(ret);
//        System.out.println(binarytree.getHeight(root));
//        binarytree.preOrder2(root);
//        binarytree.inOrder(root);
//        binarytree.postOder(root);
//        System.out.println();
//        System.out.println("=============");
//      System.out.println(binarytree.getLeafNodeCount(root));
//        System.out.println(binarytree.getLeafNodeCount2(root));
//        System.out.println(binarytree.getKLevelNodeCount(root, 3));
//        System.out.println(binarytree.getKLevelNodeCount(root, 2));
 //       System.out.println(binarytree.find(root, 12));
  //      binarytree.levelOrder(root);
        System.out.println(binarytree.isCompleteTree2(root));

    }
}
