public class  HashBucket{
    //创建节点
    class Node{
        public int key;
        public int value;
        public Node next;

        public Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }
     public   Node []array;
    public HashBucket() {
      array=new Node[10];
    }

    //创建节点数组,其实创建数组可以写入HashBucket的构造方法
//   public Node[] array=new Node[5];
    //实际元素的个数
    public int UsedSize;
    //负载因子的最大限
    public static final double LOAD_FACTOR=0.75;
    //添加元素
    public int put(int key,int value){
        int index=key%array.length;
        for(Node cur=array[index];cur!=null;cur=cur.next){
            if(key==cur.key){
                int oldValue=cur.value;
                cur.value=value;
                return oldValue;//旧的应该被替换的元素
            }
        }
        Node node=new Node(key,value);
        node.next=array[index];
        array[index]=node;
        UsedSize++;
        double load_factor=UsedSize*1.0/array.length;//问题：怎么将其转化为double小数
        if(load_factor>=LOAD_FACTOR){
            resize();
        }
        return -1;
    }
    //扩容
    public void resize(){
        Node []newArray=new Node[array.length*2];//创建一个新的数组并将其长度扩展为原数组的两倍
        for(int i=0;i<array.length;i++){
            Node node;
            for(Node cur=array[i];cur!=null;cur=cur.next){
              int index=cur.key% newArray.length;
              node=new Node(cur.key, cur.value);
              node.next=newArray[index];
              newArray[index]=node;
            }
        }
        array=newArray;
    }
    //使用这种方法需要将所有可能的结果都走一遍
    //优化如get2
    public int get(int key){
        for(int i=0;i<array.length;i++){
            Node node;
            for(Node cur=array[i];cur!=null;cur=cur.next){
                if(cur.key==key){
                    return cur.value;
                }
            }
        }
          return -1;
    }
public int get2(int key){
        int index=key%array.length;
        Node node=array[index];
        while(node!=null){
            if(node.key==key){
                return node.value;
            }
            node=node.next;
        }
        return -1;
}
}
class Test2 {
    public static void main(String[] args) {
        HashBucket hashBucket = new HashBucket();
        hashBucket.put(1, 11);
        hashBucket.put(4, 44);
        hashBucket.put(14, 1414);
        hashBucket.put(24, 2424);
        hashBucket.put(3, 33);
        hashBucket.put(6,33);
        hashBucket.put(7,33);
        hashBucket.put(8,33);
        System.out.println(hashBucket.get(14));

    }

}
