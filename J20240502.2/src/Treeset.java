import java.util.*;

public class Treeset {
    public static void main(String[] args) {
        Set<String> set=new TreeSet<>();
        set.add("apple");
        set.add("apple");
   //     System.out.println(set);
        set.add("banana");
        set.add("orange");
        set.add("watermelen");
        set.add("stray");//草莓
        //使用迭代器对元素进行输出
        Iterator<String> iterator=set.iterator();
        while(iterator.hasNext()){
            System.out.print(iterator.next()+" ");//打印当前的元素并且往下走
        }
        System.out.println();
        //将集合c中的元素添加到set中，可以达到去重的目的
        List<String> list=new ArrayList<>();
        list.add("hahha");
        list.add("xixiix");
        list.add("ohhhhh");
        list.add("guguug");
        set.addAll(list);
        //验证集合c中的元素是否全部存储在set中
       boolean falg= set.containsAll(list);
        System.out.println(falg);//这里返回的结果为：true
        //将set转变为一个数组
        //set.toArray()使得原set变为数组
        System.out.println(Arrays.toString(set.toArray()));
        //打印的结果为：[apple, banana, guguug, hahha, ohhhhh, orange, stray, watermelen, xixiix]

    }


}
