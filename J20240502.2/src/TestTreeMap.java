import java.util.*;

public class TestTreeMap {
    //put函数，放入值入集合中
    public static void Put(Map<String,String> map) {
        map.put("黑旋风","李逵");
        map.put("及时雨","宋江");
        map.put("齐天大圣","孙悟空");
        map.put("行者","武松");
        map.put("大聪明","武松");

    }
    //get函数获取map对象中已有的值
    public static void Get( Map<String,String> map) {
        String str=map.get("及时雨");
        System.out.println(str);
        String str1=map.get("无名");
        System.out.println(str1);
        /*
        结：注意如果key存在返回key对应的value值，如果key不存在，返回一个默认值(null)
         */
    }
    //如果key相同时，会替换对应的value值
    public  static void Instead( Map<String,String> map){
       String str= map.put("及时雨","大聪明");
        System.out.println(str);
        /*
        结：由于前面Put函数已经为“及时雨”-“宋江”，但是现在key值相同，所以此时会变成”及时雨“-”大聪明“，并且会返回key对应的以前旧的value值，也就是”宋江“
         */
    }
    //寻找key是否包含在Map中，时间复杂度为long(N)
    public static void ContainsKey( Map<String,String> map){
        System.out.println(map.containsKey("及时雨"));
        /*
        结：查找对应的key是否在Map中，如果存在会返回true，如果不存在会返回false
         */
    }
    //寻找value是否包含在Map中，时间复杂度为O(N)
    public static void ContainsValue( Map<String,String> map){
        System.out.println(map.containsValue("宋江"));
        /*
        结：查找对应的value是否在Map中，如果存在会返回true，如果不存在会返回false
         */
    }
    //Returns a Set view of the keys contained in this map.
    //打印所有的不可重复的key的值
    public static void KeySet(Map<String,String> map){
        for (String key:map.keySet()) {
            System.out.print(key+" ");
        }
        System.out.println();
    }
    //打印所有的可重复的value值
    public static void ValueSet(Map<String,String> map){
        for (String str:map.values()) {
            System.out.print(str+" ");
        }
        System.out.println();
    }
    //打印所有的键值对
    public static void Key_Value(Map<String,String> map){

     //   写法一：
         Set<Map.Entry<String, String>> set=new HashSet<>(map.entrySet());
        for (Map.Entry<String, String> s:set) {
            System.out.println("key: "+s.getKey()+"    value: "+ s.getValue());
        }

        /*
        写法二:
        for (Map.Entry<String, String> m:map.entrySet()) {
            System.out.println("key: "+m.getKey()+"    value: "+ m.getValue());

        }
         */

    }
    //删除对应的键值对，返回的是其对应的value值
    public static void Remove(Map<String,String> map){
        String str=map.remove("及时雨");
        System.out.println(str);
    }
    //返回key对应的value值
    public static void Ret(Map<String,String> map){
        String str=map.getOrDefault("及时雨","宋江");
       // String str2=map.getOrDefault(null,"宋江");不能为null会报错
        System.out.println(str);
      //  System.out.println(str2);
    }

    public static void main(String[] args) {
        Map<String,String> map=new TreeMap<>();
        Put(map);
//        Get(map);
  //      Instead(map);
 //       ContainsKey(map);
 //       ContainsValue(map);
 //       KeySet(map);
//        ValueSet(map);
 //       Key_Value(map);
 //       Remove(map);
//        Ret(map);
     System.out.println(map);


    }

}
