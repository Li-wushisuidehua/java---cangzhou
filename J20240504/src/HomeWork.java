import java.util.*;

public class HomeWork {
    public static void main(String[] args) {


    }

    public int singleNumber(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int x : nums) {
            if (set.isEmpty()) {
                set.add(x);
            } else {
                if (set.contains(x)) {
                    set.remove(x);
                } else {
                    set.add(x);
                }
            }
        }
        for (Integer x : set) {
            if (x != null) {
                return x;
            }
        }
        return -1;
    }

    //宝石与石头
    public int numJewelsInStones(String jewels, String stones) {
        int count = 0;
        char ch = 0;
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < jewels.length(); i++) {
            ch = jewels.charAt(i);
            set.add(ch);
        }
        for (int i = 0; i < stones.length(); i++) {
            if (set.contains(stones.charAt(i))) {
                count++;
            }
        }
        return count;

    }


}

class Test {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str1 = in.nextLine();
            String str2 = in.nextLine();
            fun(str1, str2);
        }
    }

    public static void fun(String str1, String str2) {
//      HashSet<Character> set=new HashSet<>();
//      HashSet<Character> set1=new HashSet<>();
//        //首先使用toUpperCase()将字符串转成大写，然后使用toCharArray()将字符串转成字符数组
//       for (char ch:str1.toUpperCase().toCharArray()) {
//           set.add(ch);
//       }
//       for (char ch:str2.toUpperCase().toCharArray()) {
//           if(!set.contains(ch) && !set1.contains(ch)){
//               System.out.print(ch);
//               set1.add(ch);
//           }
//       }
        HashSet<Character> set1 = new HashSet<>();
        HashSet<Character> setBroken = new HashSet<>();

        for (char ch : str2.toUpperCase().toCharArray()) {
            set1.add(ch);
        }
        for (char ch : str1.toUpperCase().toCharArray()) {
            if (!set1.contains(ch) && !setBroken.contains(ch)) {
                System.out.print(ch);
                setBroken.add(ch);
            }
        }
    }

    /*
         Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str1 = in.nextLine();
            String str2 = in.nextLine();
            fun(str1,str2);
        }
     */
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int x : nums) {
            if (set.isEmpty()) {
                set.add(x);
            } else {
                if (set.contains(x)) {
                    return true;
                } else {
                    set.add(x);
                }
            }

        }
        return false;

    }

    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsValue(nums[i])) {
                if ((Math.abs(i - map.get(nums[i]))) <= k) {
                    return true;
                }
            }
            map.put(nums[i], i);
        }
        return false;
    }

    //寻找前k个高频词
    public List<String> topKFrequent(String[] words, int k) {
        Map<String, Integer> map = new HashMap<>();
        //找到各个单词出现的频率
        for (String s : words) {
            if (!map.containsKey(s)) {
                map.put(s, 1);
            } else {
                int val = map.get(s);
                map.put(s, val + 1);
            }
        }
        //建立小根堆，将前k个元素放入进去
        PriorityQueue<Map.Entry<String, Integer>> minHeap = new PriorityQueue<>(new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                if(o1.getValue().compareTo(o2.getValue())==0){
                    return o2.getKey().compareTo(o1.getKey());
                }
                return o1.getValue() - o2.getValue();
            }
        });
        //将其他元素放入并对其进行比较
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (minHeap.size() < k) {
                minHeap.offer(entry);
            } else {
                Map.Entry<String, Integer> top = minHeap.peek();
                if (top.getValue().compareTo(entry.getValue()) < 0) {
                    minHeap.poll();
                    minHeap.offer(entry);
                } else {
                    if (top.getValue().compareTo(entry.getValue()) == 0) {
                        if (top.getKey().compareTo(entry.getKey()) > 0) {
                            minHeap.poll();
                            minHeap.offer(entry);
                        }
                    }
                }

            }
        }
            List<String> ret = new ArrayList<>();
            for (int i = 0; i < k; i++) {
                Map.Entry<String, Integer> tmp = minHeap.poll();
                ret.add(tmp.getKey());
            }
            Collections.reverse(ret);
            return ret;
    }

    //复制链表
    /*

     */

}
