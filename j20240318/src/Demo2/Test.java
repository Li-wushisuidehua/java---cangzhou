package Demo2;

import java.util.Arrays;

public class Test {
    public static void func(int[] array) throws CloneNotSupportedException {//throws用于声明异常的;
//        if (array == null) {
//            throw new CloneNotSupportedException();//抛出异常
//        }
        System.out.println(Arrays.toString(array));
    }

    public String name = "hahahahh";
    public int age = 18;

    @Override
    protected Object clone() throws CloneNotSupportedException {//throws抛出的就是异常，也就是catch要捕捉的内容
        return super.clone();
    }

    public static void fun(int []array) throws CloneNotSupportedException{//只是用于申明这儿有这么个异常;
        //写了throws代表谁调用了这个函数，谁就需要
        if(array==null){
            throw new CloneNotSupportedException();//抛出异常;
        }
    }
    public static void main(String[] args) throws CloneNotSupportedException{
        int[] array = null;
        fun(array);
//        try {
//            int[] array = null;
//            fun(array);
//        }catch (NullPointerException e){
//            e.printStackTrace();
//            System.out.println("成功捕获到了异常，正在尝试解决");
//        }
        System.out.println("hahhaha");
    }
    public static void main4(String[] args) throws CloneNotSupportedException{
        int []array=new int[]{1,2,3,4,5};
        func(array);
    }
    public static void main3(String[] args) {
        Test student=new Test();
        try{
        Test student1=(Test) student.clone();
        }catch (CloneNotSupportedException e){
            e.printStackTrace();
            System.out.println("已经获取到了问题，正在尝试解决问题！！");
        }
        int []array=new int[]{1,2,3,4,5};
       // System.out.println(student1.toString());//Student1本身是一个引用;
        //System.out.println(array.toString());//array是数组名本身就是一个引用;引用引用的是地址;
    }

    public static void main2(String[] args) {

        try {
            int[] array = null;
            func(array);
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("正在处理这个问题！！");
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            System.out.println("已捕捉到了异常，正在尝试解决异常");

        }

    }
}
