/*
通配符：？
通配符上界：通配符上界不能进行输入，可以用来get
通配符下界：通配符下届不能进行输出，可以用来set
 */
public class Plate<T> {
    private T massage;

    public T getMassage() {
        return massage;
    }

    public void setMassage(T massage) {
        this.massage = massage;
    }
}
class Food{

}
class Fruit extends Food{

}
class Apple extends Fruit{

}
class Banana extends Fruit{

}
class Test{

    public static void main(String[] args) {
        Plate<Food> plate=new Plate<>();
        fun4(plate);
    }
    //通配符的下界,表示传入的参数类型只能是Fruit或其父类
    public  static void fun4(Plate <? super Food> temp){//main函数方法中传过来使的temp参数类型是Food但是这里通配符super后面写的是Fruit这便验证了下界
                                                         //通配符是只能传入其或其父类
        temp.setMassage(new Fruit());
       temp.setMassage(new Food());
       temp.setMassage(new Apple());
        Food food=(Food) temp.getMassage();
        Fruit fruit=(Fruit) temp.getMassage();//这里如果不用强制类型转换会报错，因为无法确定类型，比如如果原本是Food类型，但是最终拿Fruit来接收，需要强转
    }

    public static void main4(String[] args) {
        Plate<Fruit> plate=new Plate<>();
       plate.setMassage(new Apple());
       plate.setMassage(new Banana());
        fun3(plate);

    }
    //通配符的上界,其中传入的类型可以是Fruit类型或者其子类型
public  static void fun3(Plate<? extends Fruit> temp){
        Fruit fruit= temp.getMassage();
    System.out.println(fruit);


}
    //通配符
    public static void main3(String[] args) {
        Plate<String> plate=new Plate<>();
        plate.setMassage("今天是个好日子");
        fun(plate);
        Plate<Integer> plate1=new Plate<>();
        plate1.setMassage(100);
        fun2(plate1);
    }
    public static void main2(String[] args) {
        Plate<String> plate=new Plate<>();
        plate.setMassage("今天是个好日子");
        fun(plate);
        Plate<Integer> plate1=new Plate<>();
        plate1.setMassage(100);
     //   fun(plate1);//这里会报错因为fun()中的参数类型可以看作是String类型，那么通过什么方法可以解决这个问题呢？
        //答案：通配符，看fun2(),main()
    }
    public static  void fun2(Plate<?>temp){
        System.out.println(temp.getMassage());

    }

   public static void fun(Plate<String> temp){
       System.out.println(temp.getMassage());

    }
}
